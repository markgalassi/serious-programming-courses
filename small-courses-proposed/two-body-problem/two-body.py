#! /usr/bin/env python3

import math

au = 1.496e11
G = 6.67408e-11
Ms = 1.989e30
Me = 4.8685e24
one_year = 365.25 * 24 * 60 * 60 # seconds in a year
dt = 10.0                       # the time interval for our solution

def main():
    pos, vel = planet_at_t0()
    print('#', pos)
    print('#', vel)
    t = 0
    # while t < 31557600:
    while t < one_year:
        pos, vel = take_step(t, pos, vel)
        t += dt

def planet_at_t0():
    # return ([-1*au, 0], [0, 29783])
    return ([-1*au, 0], [4000, 29783])

def take_step(t, x, v):
    r = math.sqrt(x[0]**2 + x[1]**2)
    F = [-x[0]*G*Ms*Me/r**3, -x[1]*G*Ms*Me/r**3]
    acc = (F[0]/Me, F[1]/Me)
    # print(t, r, F)
    v[0] = v[0] + acc[0]*dt
    v[1] = v[1] + acc[1]*dt
    x[0] = x[0] + v[0]*dt
    x[1] = x[1] + v[1]*dt
    theta = math.atan(x[1]/x[0])
    print('%g    %g   %g   %g   %g   %g' 
          % (t, x[0], x[1], v[0], v[1], theta))
    return x, v

main()
