#! /bin/sh

dot -Tsvg -O kennedys.dot
dot -Tpng -O kennedys.dot
dot -Tpdf -O kennedys.dot

echo "gtp_graph.dot: gtp.py ;	python3 gtp.py" | make -f-
echo "Complex_system.dot: gtp.py ;	python3 gtp.py Complex_system" | make -f-
echo "Asterix.dot: gtp.py ;	python3 gtp.py Asterix" | make -f-
