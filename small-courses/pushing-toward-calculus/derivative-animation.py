#! /usr/bin/env python3

import math
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

draw_scale = 4.0

def main():
    # the limits and the function that we plot
    a = -1.3
    b = 3.2
    func = curvy_cubic

    # matplotlib graphics setup - boiler-plate stuff
    plt.rcParams.update({'font.size': 10*draw_scale})
    fig = plt.figure(figsize=(6.4*draw_scale, 4.8*draw_scale))

    # first generate the x for the entire domain from a to be
    x = np.arange(a, b, 1.0/1000)
    # pick the point at which we calculate the derivative
    # x0 = a + 2.0*(b-a)/3
    x0 = 1.8

    ## now a loop in which h gets smaller and smaller
    for i in range(2,300):
        # pick a value of h that keeps decreasing toward 0
        h = 2/(i + 1e-2*i*i)
        # calculate the slope of the secant line
        y0 = func(x0)
        y1 = func(x0+h)
        m = (y1 - y0) / h
        # now get the intercept
        b = y0 - m * x0
        print(x0, h, x0+h, y0, m, b)

        # to animate we clear and then redraw everything
        fig.clear()
        # draw the entire curve
        plt.plot(x, func(x), color='g', linewidth=1.0*draw_scale)
        plt.grid(True)
        # draw the two secant points
        plt.plot([x0], [y0], marker='o', markersize=7*draw_scale, color='red')
        plt.plot([x0+h], [y1], marker='o', markersize=5*draw_scale, color='black')
        # draw the line through those two points, using the linear
        # equation y = m*x + b
        line_x_pts = np.arange(x0 - 1.2, x0 + 1.2, 1.0/100)
        line_y_pts = m * line_x_pts + b
        plt.plot(line_x_pts, line_y_pts, color='b', linewidth=1.0*draw_scale)
        # annotate the current value of h and slop
        info_para = """func: %s\nh: %g\nslope: %g""" % (func.__name__, h, m)
        plt.annotate(
            info_para,
            xy=(0, -2), xytext=(0, -2))
        # now annotate where (x0, y0) and (x0+h, y1) are
        plt.annotate('(x0, y0)', xy=(x0, y0), xytext=(x0-1, y0-3),
                     arrowprops=dict(facecolor='black', shrink=0.05))
        plt.annotate('(x0+h, y1)', xy=(x0+h, y1), xytext=(x0+h+1, y1+2),
                     arrowprops=dict(facecolor='black', shrink=0.05))
        # to animate we do the draw_idle() and pause()
        fig.canvas.draw_idle()
        plt.pause(1.45+h)

    plt.waitforbuttonpress()

def curvy_cubic(x):
    """a simple cubic function mixed with a sin - it shows some
    interesting curves"""
    return (x-1)**3 - 3*(x-1) + np.sin(x)

main()
