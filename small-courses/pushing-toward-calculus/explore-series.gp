set grid
set title '1/k and 1/k^2 series'
set xlabel 'number of terms in the series'
set ylabel 'sum of the series'
plot 'explore-series.dat' using 1:2 with lines, \
     'explore-series.dat' using 1:3 with lines, \
     pi*pi/6 with lines lw 2
