#! /bin/sh

echo "running: $0"
echo "args: $*"

## generate the damped oscillator columns
echo "explore-series.dat: explore-series.py ;	python3 \$< > \$@" | make -f-

## convert the native .svg files to .pdf for latex output
echo "%.pdf: %.svg ;	rsvg-convert -f pdf -o \$@ \$<" | make -f- Secant-calculus.pdf
echo "%.pdf: %.svg ;	rsvg-convert -f pdf -o \$@ \$<" | make -f- Tangent-calculus.pdf
