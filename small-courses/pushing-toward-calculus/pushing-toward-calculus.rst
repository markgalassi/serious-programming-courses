.. _chap-pushing-toward-calculus:

=========================
 Pushing toward calculus
=========================

[status: somewhat written but incomplete]

Motivation and plan
===================

When I was in school calculus is where math became a pure and
never-ending thrill.

Knowing calculus will get us on a path toward science the way it is
really done: scientific laws describe how biological or chemical or
physical systems change in time and position, and calculus is the
mathematics of those changes.

So introducing calculus opens up a fantastic world to us, but how do
we teach it to middle and high school kids who have not yet learned
the prerequisites?

Our advantage over a pure math class is that we can write programs to
illustrate calculus topics.  We might not yet know trigonometric and
logarithmic identities, but we can write programs to convince
ourselves of the properties we have not yet proven.  The same goes for
limits.  This means that we can jump ahead for a while, knowing that
in our pure math classes we will eventually prove all the material
that we now understand intuitively.


Prerequisites
=============

* The 10 hour programming workshop.

* The mini-courses on elementary and intermediate plotting in
  :numref:`chap-data-files-and-first-plots` and
  :numref:`chap-intermediate-plotting`

* The "tour of functions" mini-course in
  :numref:`chap-a-tour-of-functions`.


Limits, the infinitely big, and the infinitesimally small
=========================================================

Let us plot a function: :math:`f(x) = 1/x`:

::

   gnuplot> set grid
   gnuplot> plot 1/x

We see that it spikes both down and up when x is close to zero, and
that it gets closer and closer to zero as x gets very big in the
positive direction, and also when x gets very big in the negative
direction.

This brings up some questions:

#. Does :math:`f(x)` ever actually reach zero when x gets big?

#. What is :math:`f(0)`?  We cannot divide by zero, so the answer
   cannot be :math:`f(0) = 1/0`.

There is no clear answer to these questions, but as we begin to grope
for answers we might want to say something like:

#. :math:`f(x)` will reach zero when x is infinity, or minus infinity.
   In formulae: :math:`f(\infty) = 1/\infty = 0` and :math:`f(-\infty)
   = 1/(-\infty) = 0`.

#. :math:`f(0)` is infinite.  So: :math:`f(0) = 1/0 = \infty`.

But there are several problems with writing down these expressions.

First of all infinity is not an actual number, so we do not have a
proper arithmetic for it.  We have to define what all of that means.

Then there is the obvious worry that from the plot it looks like
:math:`f(0) = \infty` (when you look from the right) *and* :math:`f(0)
= -\infty` (when you look from the left).  This is not OK becuase a
function can only have one value at a given point.

The way mathematicians tackle this is with the idea of a *limit*.  Let
us start by using this kind of terminology for our :math:`1/x`
example.  For the case of x getting big:

"The limit of :math:`1/x` as x goes to infinity is zero." (And "The
limit of :math:`1/x` as x goes to minus infinity is zero.")

And the way we define that is (taking some liberty):

"You can make :math:`1/x` arbitrarily close to zero by making
:math:`x` big enough."

A more formal way we can say it is:

"For any number (no matter how small) :math:`\epsilon`, I can find a
value :math:`x_0` such that, for all :math:`x > x_0`, :math:`f(x) <
\epsilon`."

So that allows us to talk about the behavior of :math:`1/x` as x gets
really big (or really big in the negative direction): we do not say
that :math:`1/\infty = 0`, but rather we say that the limit of
:math:`1/x` is 0 as :math:`x` gets really big.  The math notation for
this is:

.. math::

   \lim_{x\to\infty} \frac{1}{x} = 0

although one can also say:

.. math::

   \frac{1}{x} \to 0 \; {\rm as } \; x \to \infty

And how do mathematicians deal with :math:`f(0)`?  That's the one
where we have different behavior to the left and to the right of
:math:`x = 0`.

For this mathematicians define a *one-sided limit*.  The way we can
talk about :math:`1/x` around 0 is:

"The limit of :math:`1/x` as x goes to 0 *from above* is infinity."

.. math::

   \lim_{x\to 0^+} \frac{1}{x} = \infty

and

"The limit of :math:`1/x` as x goes to 0 *from below* is minus
infinity."

.. math::

   \lim_{x\to 0^-} \frac{1}{x} = -\infty


.. note::

   Limits also apply to ordinary functions at ordinary points, not
   just to unusual situations.  For example, :math:`\lim_{x\to 3}
   x^2` is simply :math:`3^2 = 9`.


Continuous functions
====================




Convergence and divergence
==========================

We saw that :math:`f(x) = 1/x` approaches zero as x goes to infinity.
We say that :math:`f(x)` *converges* to zero as x goes to infinity.

We saw that :math:`f(x) = 1/x` goes wild at zero.  We say that
:math:`f(x)` *diverges* as x goes to zero.

Another example clarifies what mathematicians mean by divergence.
Take this limit:

.. math::

   \lim_{x\to\infty} \sin(x)

The :math:`\sin(x)` function will bounce around forever, bounded
between -1 and +1 forever - it will never settle close to a single
value.  We say that this function diverges, even though it's not
blowing up to infinity or minus infinity: the fact that it does not
converge to a single value means that the limit diverges.


Weird mixes
===========

Our examples of :math:`1/0` and :math:`1/\infty` are straightforward,
but what if you have a more complex limit, like:

.. math::

   \lim_{x\to 0} \sin(1/x)

Let us plot this one, and then zoom in to see if it converges:

::

   gnuplot> set samples 1000
   gnuplot> plot sin(1/x)
   gnuplot> plot [-3:3] sin(1/x)
   gnuplot> plot [-1:1] sin(1/x)
   gnuplot> plot [-0.5:0.5] sin(1/x)
   gnuplot> plot [-0.1:0.1] sin(1/x)
   gnuplot> plot [-0.05:0.05] sin(1/x)
   gnuplot> plot [-0.01:0.01] sin(1/x)
   gnuplot> plot [-0.005:0.005] sin(1/x)
   gnuplot> plot [-0.001:0.001] sin(1/x)

We see that there is no convergence, and in fact as you get closer to
zero the function looks like it's *less* likely to converge on a
single value.

Now let us look at:

.. math::

   \lim_{x\to 0} x\sin(1/x)

and let us plot this slighty different function in the same way:

::

   gnuplot> set samples 1000
   gnuplot> plot x*sin(1/x)
   gnuplot> plot [-3:3] x*sin(1/x)
   gnuplot> plot [-1:1] x*sin(1/x)
   gnuplot> plot [-0.5:0.5] *sin(1/x)
   gnuplot> plot [-0.1:0.1] x*sin(1/x)
   gnuplot> plot [-0.05:0.05] x*sin(1/x)
   gnuplot> plot [-0.01:0.01] x*sin(1/x)
   gnuplot> plot [-0.005:0.005] x*sin(1/x)
   gnuplot> plot [-0.001:0.001] x*sin(1/x)

Or let us automate that:

::

   reset
   set samples 5000
   set yrange [-1:1]

   do for [ii=1:100:1] {
       max = 100.0/(ii*ii)
       print(max)
       plot [-max:max] x*sin(1/x)
       pause 0.03
   }


This zooming in shows us that the function :math:`f(x) = x \sin(1/x)`
does not diverge at :math:`x = 0`, even though it has that :math:`1/x`
bit inside.  Although we cannot take :math:`f(0)`, we do have a limit
that converges:

.. math::

   \lim_{x\to 0}x \sin(1/x) = 0


Limits of some functions
========================

.. literalinclude:: explore-limit.py
                    :caption: explore-limit.py -- exploring the
                              limiting behavior of some functions.


..
   Limits of sequences
   ===================


The limit of a series
=====================

A *summation* is the addition of a sequence of numbers.  We use the
cool gree capital sigma letter :math:`\Sigma` as a notation for this.
For example, if we want to write Gauss's formula for the sum of the
first 100 numbers:

.. math::

   \sum_{k=1}^{100} k = (100 * 101) / 2 = 5050

More generally:

.. math::

   \sum_{k=1}^{n} k = \frac{n (n+1)}{2}

Or the sum of the first 50 values of the *harmonic series*
:math:`1/k`:

.. math::

   \sum_{k=1}^{50} \frac{1}{k}

Or the sum of the first :math:`n` values of :math:`1/k^2`:

.. math::

   \sum_{k=1}^{n} \frac{1}{k^2}

We read this as "The sum of one over k squared with k going from one
to n ..."

Sometimes we have something called an *infinite series*, or just
*series*.  This is a sum where you keep adding up the elements
forever.  An example is:

.. math::

   \sum_{k=1}^{\infty} \frac{1}{k^2}

which looks like the previous example of a finite sum, but where we
sum forever.

One question you might ask is "wait, if you add infinitely many
things, won't that diverge?"  My first answer is to say that I'm
delighted at how you used the word "diverge" so comfortably.  The
answer to the question is that we are summing an infinite number of
terms, but as we add more terms and k gets bigger, :math:`1/k^2` gets
infinitesimally small!  That means that it's *possible* that the sum
will converge.

Let us write a simple program which tests this:

.. literalinclude:: explore-series.py
   :caption: explore-series.py - explore whether the infinite sums of
             :math:`1/k` and :math:`1/k^2` diverge or converge.

.. _fig-explore-series:

.. figure:: explore-series.*
   :width: 60%

   The sum of the sums of :math:`1/k` and :math:`1/k^2`.  We see that
   the sum of :math:`1/k` gets smaller but eventually does surpass any
   point you might pick, while the sum of :math:`1/k^2` never grows
   past :math:`\frac{\pi^2}{6}`.

A cute fact to note is that:

.. math::

   \sum_{k=1}^{\infty} \frac{1}{k^2} = \frac{\pi^2}{6}

so you should take a moment to take the result of the third column in
the output of ``explore-series.py``, multiply by 6, take the square
root, and see if you get :math:`\pi`.

We have seen two examples of infinite series, one of which converges
and the other does not.  Note that mathematicians have dozens of
interesting ways of proving *rigorously* that the harmonic series
diverges.  A proof dating from the middle ages is shown in the
`Wikipedia article on the harmonic series
<https://en.wikipedia.org/wiki/Harmonic_series_(mathematics)#Divergence>`_



The most important application: derivatives
===========================================

The most important use we have for limits is to formulate differential
and integral calculus.

Differential calculus is the study of the *rate of change* of
functions, while integral calculus is the study of the areas defined
by the curves of functions.

We will discuss integrals in :numref:`chap-numerical-integration`,
while we will talk briefly about derivatives here.  [FIXME: eventually
derivatives might also have their own chapter.]

There are many introductions to calculus, including some written as
free documentation, so here I will simply show a couple of figures
from the `Wikipedia article on derivatives
<https://en.wikipedia.org/wiki/Derivative>`_ so that an instructor can
make reference to those pictures and explain it on a blackboard or
whiteboard.

In :numref:`fig-derivative-tangent` we see that a reasonably smooth
curve has a tangent line at each point, and the slope of that tangent
line (which we know from analytical geometry) is what we call the
slope of the curve.

.. _fig-derivative-tangent:

.. figure:: Tangent-calculus.*
   :width: 60%

   The derivative is slope of the the line tangent to the curve at the
   given point.

But how do we find the slope of this tangent line?  In
:numref:`fig-derivative-secant` we see how we might do this:

.. _fig-derivative-secant:

.. figure:: Secant-calculus.*
   :width: 60%

   To find the tangent line we start with an *intersecting* (or
   *secant*) line that goes through the point :math:`x` and a point a
   tiny bit ahead :math:`x + h`, where :math:`h` is small.


If we take the points :math:`(x, f(x)), (x+h, f(x+h))` we have a line
segment and we can calculate its slope with the usual :math:`\Delta
y/\Delta x`:

.. math::

   {\rm derivative\; of\; f\; at\; x} = \frac{f(x+h) - f(x)}{(x + h) - x} = \frac{f(x+h) - f(x)}{h}

We use the notation

.. math::

   \frac{d f(x)}{dx}

for the slope (derivative) of a function f at point x.

The place where limits come in is that if we make h really small then
this intersecting line will become the tangent line, and its slope
will be the derivative at that point.

.. note::

   The notation :math:`\frac{df(x)}{dx}` is called the "Leibniz
   notation".  Other common ways of writing the derivative are the
   "Lagrange notation:" :math:`f'(x)`, and Newton's "dot" notation which
   is usually used when we have functions of *time*:

   .. math::

      \dot x = \frac{dx}{dt}


Visualizing derivatives with an animation
=========================================

Drawing the pictures for the derivative on the board might help some,
but an animation really drives home the idea of the derivative as a
*limit*.

The program in :numref:`listing-derivative-animation` shows an
animation of the types of plots from a *secant* line to *tangent*
line.  See :numref:`fig-derivative-secant` and
:numref:`fig-derivative-tangent`.

.. _listing-derivative-animation:

.. literalinclude:: derivative-animation.py
   :caption: derivative-animation.py - look at the limit as h
             approaches zero: the secant line becomes a tangent line at the given point.

