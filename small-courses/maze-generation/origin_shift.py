import numpy as np
import networkx as nx
import random
import matplotlib.pyplot as plt

def main():
    dims = [25, 25]
    iterations = 5000
    maze, node_arr = gen_blank_maze(dims)
    maze = origin_shift(maze, node_arr, iterations)
    save_wall_maze(maze, dims)

class node(object):
    """Serves as a node in the NetworkX network, because arrays cannot"""
    def __init__(self, x, y):
        self.x = x
        self.y = y

def gen_blank_maze(dims):
    """returns a blank grid and an array containing the nodes"""
    new_maze = nx.Graph()
    node_arr = []
    for x in range(dims[0]):
        node_arr.append([])
        for y in range(dims[1]):
            new_node = node(x, y)
            new_maze.add_node(new_node)
            node_arr[-1].append(new_node)
    return new_maze, np.array(node_arr)

def simple_tree(maze, node_arr): # assumes maze is empty
    """returns the simplest possible tree given the starting maze"""
    maze = nx.DiGraph(maze)
    dims = [len(node_arr), len(node_arr[0])]
    for x in range(dims[0]):
        if x != dims[0] - 1:
            maze.add_edge(node_arr[x + 1][0], node_arr[x][0])
        for y in range(dims[1] - 1):
            maze.add_edge(node_arr[x][y + 1], node_arr[x][y])
    return maze

def origin_shift(maze, node_arr, n_iterations): # note: edges must already be pointed to root.
    """uses the origin shift algorithm to generate a maze"""
    maze = simple_tree(maze, node_arr)
    root = node_arr[0][0]
    dims = [len(node_arr), len(node_arr[0])]
    for i in range(n_iterations):
        root_neighbors = unvisited_neighbors(root, [], node_arr, dims)
        random.shuffle(root_neighbors)
        old_root = root
        root = root_neighbors[0]
        maze.add_edge(old_root, root)
        # removes the outgoing edge from the new root
        maze.remove_edge(list(maze.out_edges(root))[0][0],list(maze.out_edges(root))[0][1])
        if i % 10 == 0:
            # save the maze every 10 iterations
            save_wall_maze(maze, dims, show_fig=False)
    return maze

def unvisited_neighbors(node, nodes_visited, node_arr, dims):
    """find all the neighbors of a given node not yet visited"""
    x, y = node.x, node.y
    neighbors = []
    # makes sure no nodes that aren't in the maze are added to the neighbors
    if x != 0:
        neighbors.append(node_arr[x - 1][y])
    if x != dims[0] - 1:
        neighbors.append(node_arr[x + 1][y])
    if y != 0:
        neighbors.append(node_arr[x][y - 1])
    if y != dims[1] - 1:
        neighbors.append(node_arr[x][y + 1])
    visited_neighbors = []
    for neighbor in neighbors:
        if neighbor in nodes_visited:
            visited_neighbors.append(neighbor)
    # remove visited neighbors
    neighbors = [i for i in neighbors if i not in visited_neighbors]
    return neighbors

def save_wall_maze(maze, dims, show_fig=True):
    """show the maze in a format recognizable to most people"""
    dims = [dims[0] + 1, dims[1] + 1]
    wall_maze, wall_node_arr = gen_blank_maze(dims)
    for x in range(dims[0]):
        for y in range(dims[1]):
            if (x + y) % 2 == 0: # avoids doubling edges
                current_node = wall_node_arr[x][y]
                neighbors = unvisited_neighbors(current_node, [], wall_node_arr, dims)
                for neighbor in neighbors:
                    wall_maze.add_edge(current_node, neighbor)

    for edge in maze.edges:
        # this code shows the relationship between the paths in the maze and the
        # edges of the maze
        if edge[0].x == edge[1].x:
            if edge[0].y > edge[1].y: # make sure the edge is in the right order for the formula
                edge = list(reversed(edge))
            edge_to_remove = wall_node_arr[edge[0].x][edge[0].y + 1], wall_node_arr[edge[1].x + 1][edge[1].y]
        if edge[0].y == edge[1].y:
            if edge[0].x > edge[1].x:
                edge = list(reversed(edge))
            edge_to_remove = wall_node_arr[edge[0].x + 1][edge[0].y], wall_node_arr[edge[1].x][edge[1].y + 1]
        wall_maze.remove_edge(edge_to_remove[0], edge_to_remove[1])

    # remove edges for the start and end
    start_edge = wall_node_arr[0][0], wall_node_arr[0][1]
    end_edge = wall_node_arr[dims[0] - 1][dims[1] - 1], wall_node_arr[dims[0] - 1][dims[1] - 2]
    wall_maze.remove_edges_from([start_edge, end_edge])

    # use networkx layout functions to arrange nodes in a grid
    layer_dict = {}
    for i in range(len(wall_node_arr)):
        layer_dict[i] = wall_node_arr[i]
    pos = nx.multipartite_layout(wall_maze, subset_key=layer_dict)
    nx.draw(wall_maze, pos=pos, arrows=False, node_size=0, width=2)
    plt.savefig('origin_shift.png')
    print('Maze saved to origin_shift.png')
    if show_fig:
        plt.show()
    else:
        plt.clf()

main()
