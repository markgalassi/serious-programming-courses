#! /bin/sh

echo "running: $0"
echo "args: $*"

## generate the simple sin wave
echo "simplewave.txt: simplewave.py ;	python3 simplewave.py > simplewave.txt" | make -f-
## and the one that we plot
echo "simplewave-write.dat: simplewave-write.py ;	python3 simplewave-write.py" | make -f-

## fetch world population data
echo "world-pop-mac-format.csv: ;	wget --continue https://ourworldindata.org/wp-content/uploads/2013/05/WorldPopulationAnnual12000years_interpolated_HYDEandUNto2015.csv -O world-pop-mac-format.csv" | make -f-
## convert mac carriage returns to newlines, since gnuplot gets confused
echo "world-pop.csv: world-pop-mac-format.csv  ;	cat \$< | tr '\r' '\\\n' > \$@" | make -f- world-pop.csv
