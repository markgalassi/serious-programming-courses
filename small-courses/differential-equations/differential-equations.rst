.. _chap-differential-equations:

========================
 Differential Equations
========================

.. sectionauthor:: Sophia Mulholland <smulholland505gmail.com>

Motivation, Prerequisites, Plan
===============================

The goal of this chapter is to learn about differential equations and
how they are used to solve scientific problems.  We will also learn to
write programs in C to solve differential equations.

Differential equations are used to answer scientific questions about
how systems *change*.
We will look in to these examples:

A very loose definition of the uses of differential equations is to
describe how things change.

* how springs move
* how populations grow
* how radioactive material decays
* how bridges collapse

The plan is to start out with a simple explanation of derivatives, and
see examples of derivatives in action. We'll look into the derivation
of a falling object's equation and then Euler's method. Then we'll end
up modeling a falling body and an oscillating spring.

Derivatives
===========

If you google the word 'derivative' you'll find the definition:

An expression representing the rate of change of a function with
respect to an independent variable.

So the rate of change of a function can change right? It could be
going up then down and then up again. So how can we represent the rate
of change if it's not just a number? We use an expression. Like
:math:`2x` or :math:`5x^3`. We can use a function to model the rate of
change of another function.

Here's an example of taking a derivative:

.. math::
   f(x) & = 5x^3-3x^2+10x-5\\
   f'(x) &= 15x^2-6x+10

There are many different techniques to solve these equations like
the Chain rule, the Quotient rule, and the Product rule. 

You can graph the functions in gnuplot like this

::
   
   gnuplot
   # and at the gnuplot> prompt:
   set grid
   plot 5*x*x*x-3*x*x+10*x-5
   plot 15*x*x-6*x+10

.. _fig-example-derivative:

.. figure:: example-derivative.*
            :width: 90%
	    
Why?
~~~~

So why do we care about the rate of change of a function? It helps us
predict what's going to come next. We can predict the future
population of earth by analyzing its rate of change right now and
making an estimate. We can predict how radioactive material is going
to decay.

You can think of a derivative as an instantaneous rate of
change.

In the image below, as the two blue points get closer together, we can
estimate the slope as the change in :math:`x` as it approaches :math:`0`.

.. _fig-derivative-png:

.. figure:: derivative.png
   :width: 70%

   This photo from the Wikipedia article on derivatives,
   https://en.wikibooks.org/wiki/Waves/Derivatives shows the concept
   of taking the derivative


There are real world examples of derivatives all throughout physics,
computer science, chemistry, biology, and economics. 

Derivatives are an instantaneous rate of
change or a slope at one point in time. 

Definitions
===========

Differential equation
   An equation involving a function and one or more of it's derivatives

Derivative
   An expression representing the rate of change of a function with
   respect to an independent variable.

Different notations:
   .. math::

      y'' & = 3x  \;\; \textrm{Lagrange notation} \\
      f''(x) & = 3x  \;\; \textrm{Lagrange notation} \\
      \frac{d^2y} {dx^2} & = 3x  \;\; \textrm{Leibniz notation} \\
      \ddot{y}(t) & = 3t  \;\; \textrm{Newton notation (only for functions of time)}

We have heard lots of talk of equations, and it was probably a
reference to *algebraic* equations, which might look like:

.. math::

   x^2 + 2x - 3 & = 0 \\

We work out the solutions, possibly by factoring it:

.. math::

   (x+3)(x-1) & = 0 \\
   \dots \implies & x = \{1, -3\}

The solution to a system of algebraic equations is a number or set of
numbers.

But now we talk about *differential* equations.  An example might look
like (in the various notations):

.. math::

   y'' + 2y' & = 3y \\
   f''(x) + 2f'(x) & = 3f(x) \\
   \frac{d^2y} {dx^2} + 2\frac{dy} {dx} & = 3y \\
   \dots \implies & y = e^{-3x}

The solution to a differential equation is a function or a family of functions.

An Example
==========

We know from Newton's law that

.. math::

   F = ma

If the force is the force gravity for a falling body, :math:`F = -mg`,
then:

.. math::

   ma = -mg

and that

.. math::

   a = -g

Now

.. math::

   ma & = -mg \\
   m\frac{d^2y} {dt^2} & = -mg \\
   \frac{d^2y} {dt^2} + g & = 0

From this, we have proved that the constant :math:`-g` is equal to the
second derivative of position.

We also know that acceleration is the second derivative of position. How?
Well if an object's position is x, then it's velocity is the change in
x over time.

The acceleration is then the change in velocity over time.
:math:`\frac{dv} {dt}` or :math:`\frac{d^2x} {dt^2}`

.. _sec-diffeq-population-growth:

Population Growth
=================

The differential equation describing population growth is:

.. math::

   \frac{dP(t)}{dt} = r P(t)

One way to read this is to think that the more rabbits you have, the
more babies you will make in a generation.

How do you solve this?  Do you remember which function had a
derivative that was equal to itself?

.. math::

   f(x) & = e^x \\
   \frac{df(x)}{dx} & = e^x = f(x)

with a constant multiplier in the exponent:

.. math::

   f(x) & = e^{\alpha x} \\
   \frac{df(x)}{dx} & = \alpha f(x)

With all that in hand, we can *guess* that the solution is:
:math:`P(t) = e^{rt}`: this will solve the differential equation.

But wait, now note that if you multiply :math:`P(t)` by a constant
:math:`A`, the equation is still satisfied, since you can simplify
away any constant multiple of :math:`P(t)` in the equation.  This
means that the most general solution is:

.. math::

   P(t) = A \times e^{rt}

While a pure mathematician might be happy to simply say that you can
have an arbitrary constant, a scientist will naturaly ask what the
constant means in the system we are studying.  And usually there is a
good answer.  Take a look at what happens at time :math:`t = 0`:

.. math::

   P(0) = A e^{r \times 0} = A e^0 = A

So :math:`A` is the starting value of the population at time 0.  We
can call it :math:`P_0` and write:

.. math::

   P(t) = P_0 e^{r t}

To summarize our notation in this problem:

* :math:`P(t)` is the population at time t
* :math:`P_0` is the initial population
* and :math:`r` is the growth rate

Let's try to graph a rabbit population with an initial population of
2 rabbits and a growth rate of 0.5:

:math:`P = 2e^{0.5t}` is the function

:math:`\frac{dP} {dt} = e^{0.5t}` is the function's derivative
      
::

   gnuplot
   # and at the gnuplot> prompt:
   plot 2*exp(0.5*x)
   replot exp(0.5*x)

.. _fig-rabbit-population:

.. figure:: rabbit-population.*
   :width: 60%

   The growth of the population is in purple and the derivative of
   that line is in green. Note that as the slope of the rabbit
   population gets steeper, the derivative also increases.
   
But what does the derivative tell us?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Since the derivative is positive, the function is increasing.

* Since the derivative is increasing, the slope of the function is
  increasing.

Euler's Method
==============

Euler's Method is a way of approximating the solution of a first-order
differential equation. Remember that the solution of a differential
equation is always a function or set of functions. To use this method,
we need to know the first point on the section we want to look at.

When we don't know the solution to a particular equation, we can use
Euler's Method. By taking little steps along the graph, it shows us a
prediction of what the exact solution looks like.


.. _fig-euler-method-from-wikipedia.svg:

.. figure:: Euler-method-from-wikipedia.*
   :width: 50%

   From Wikipedia, https://en.wikipedia.org/wiki/Euler_method A
   picture of the Euler method. The unknown curve is blue, and the
   approximation is in red.

The idea is that if you have the differential equation and one
starting point, you can approximate the solution of the equation.  In
the figure above, the red line is obviously not curvy like the blue,
but why?

At the first point, we have the derivative at a point :math:`(x_1,
y_1)`. That means we have the slope at that exact point. We use the
slope to graph a tangent line across a time step, which is why they
appear to be segments. Then, we have the next two points (hopefully
they are close to the actual solution). We calculate
the derivative for :math:`(x_2, y_2)` and repeat the process.

Let's graph :math:`y' = 2x - 2` and use euler's method to graph its
solution :math:`y = x^2 - 2x`.

It's much easier to know if we're right, if we already know the
solution. For most differential equations, it is really hard or
impossible to solve it analytically.

.. _euler-method-c:

.. literalinclude:: euler-method.c
   :language: c
   :caption: ``euler-method.c`` demonstrates Euler's method.

Compile and run and plot this with:

::

   gcc -o euler-method euler-method.c -lm
   ./euler-method > euler-method.dat
   gnuplot
   # then at the gnuplot> prompt type:
   set grid
   plot "euler-method.dat" using 1:2 with lines
   replot  "euler-method.dat" using 1:3 with lines

.. _fig-euler:

.. figure:: euler-method.*
   :width: 60%

   Euler's approximation with 1000 steps.

At first glance, It looks like the two lines are right on top of each
other right? Well if you zoom in on your own graph you can probably
see that the lines are not exactly the same. The green line is our
exact solution. Using Euler's method can give us a good approximation
for the solution, but never exact.
	   
So now, we can compare that good approximation to a poor
approximation using Euler's method.  If you run:

::

   ./euler-method 10 > euler-method-bad-approximation.dat
   gnuplot
   # then at the gnuplot> prompt type:
   set grid
   plot "euler-method-bad-approximation.dat" using 1:2 with lines title "approximate"
   replot  "euler-method-bad-approximation.dat" using 1:3 with linespoints title "exact"

.. _fig-euler-bad-approximation:

.. figure:: euler-bad-approximation.*
   :width: 60%

   Euler's approximation with just 10 steps.

On the purple line there are only ten steps and so it differs a lot
from an exact solution of the equation :math:`\frac{dy} {dx} = 2x - 2`.


Second Order Differential equations
===================================

A second order differential equation is one that involves not just the
first derivative of y, but the second one too.

.. math::

   y'' + P(x)y' + Q(x)y = 0

where :math:`P(x)` and :math:`Q(x)` are continuous functions or
constants.

The solutions to second order differential equations will have two
arbitrary constants, instead of having just one for first order
equations.  Yes: this is not a coincidence.

Incorporating a second order differential into a real world problem
can be really useful. For instance, a model of a ball dropped off a
tall building is going to have more than one factor right? To model
it's position, you have to incorporate it's initial position AND its
initial velocity AND it's acceleration due to gravity.


Falling Body
============

Imagine taking a basketball and dropping it off a building. What force
is acting on that ball?

Gravity will cause the ball to accelerate towards the ground. So what
would we need to model it? The mass of the basketball and the
acceleration of gravity.

The way this is described in physics is with *Newton's second law:*

.. math::

   F = m a = m \frac{d^2y}{dt^2}

We know that for a smallish fall in an ordinary part of the world the
force of gravity is :math:`F = -m g` (the negative sign is because it
is going down), so we end up with this equation:

.. math::

   m \frac{d^2y}{dt^2} & = - m g \\

you can cancel the mass terms to get:

.. math::

   \frac{d^2y}{dt^2} = -g

The solution to this equation is one of the simplest cases of
indefinite integral applied twice:

.. math::

   v(t) & = - g t + C_1 \\
   y(t) & = - \frac{1}{2} g t^2 + C_1 t + C_2

where :math:`C1` and :math:`C_2` are the two arbitrary constants we
expect from a second order differential equation.  Note that at time
:math:`t = 0` we can see that

.. math::

   v(0) & = C_1 \\
   y(0) & = C_2

so we can pick better names for the constants: :math:`v_0` and
:math:`y_0` (initial velocity and initial height):

.. math::

   v(t) & = g t + v_0 \\
   y(t) & = - \frac{1}{2} g t^2 + v_0 t + y_0

where the physical meaning of the arbitrary constants stands out
nicely.


Type in the program below called ``falling-body.c``

.. _falling-body-c:

.. literalinclude:: falling-body.c
   :language: c
   :caption: ``falling-body.c`` shows exact solution and approximation
      
Compile and run it with:

::

   gcc -o falling-body falling-body.c -lm
   ./falling-body > falling-body.txt

Now use gnuplot to graph the falling body as a function of time

::

   gnuplot
   # then at the gnuplot> prompt:
   set grid
   plot "falling-body.txt" using 1:2 with lines
   replot "falling-body.txt" using 1:4 with lines  

.. _fig-falling-body:

.. figure:: falling-body.*
   :width: 60%

   Height of a falling body versus time.

Adding Air Resistance to Falling Body
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you consider air drag then the falling body behaves differently.
Air drag can often be modeled with an opposing force

.. math::

   F_{\rm drag} = b v^2 = b \dot{y}^2

where :math:`b` is a constant coefficient which depends on the
properties of the fluid (in our case air) and the body moving in it.

This drag force gives us this differential equation:

.. math::

   m \ddot{y} = b \dot{y}^2 - m g \\
   m \ddot{y} - b \dot{y}^2 + m g = 0

The full solution to this differential equation is possible, though
quite gnarly - we discuss the full solution below.

Type or paste in the program below called ``falling-body-drag.c``

.. _falling-body-drag-c:

.. literalinclude:: falling-body-drag.c
   :language: c
   :caption: ``falling-body-drag.c`` shows a falling
	      body's path as a function of time.

Compile and run it with
::

   gcc -o falling-body-drag falling-body-drag.c -lm
   ./falling-body-drag 1.7 1000 0.4 > falling-body-drag.dat

Now use gnuplot to graph the falling body as a function of time,
including drag:
::

   set grid
   plot "falling-body-drag.dat" using 1:2 with lines title "with drag"
   replot "falling-body-drag.dat" using 1:4 with lines title "without drag"

.. _fig-falling-body-drag:

.. figure:: falling-body-drag.*
	    :width: 60%

You will notice that the two plots start out together, but then the
plot with drag diverges from the free fall and eventually straightens
out.

This straightening out of the slope of :math:`y(t)` means that the
velocity has become a constant due to air drag balancing out the
effects of gravity.  This is called the *terminal velocity*.

To get more insight into what happened you can look at the plots of
column 3 (velocity) and column 5 (acceleration).  You will see that
the velocity starts close to 0, gets very negative very quickly, and
then tapers off to about :math:`-14 m/s`, which is the terminal
velocity.

The acceleration, instead, starts at :math:`-9.8 m/s^2` and taperes
off to zero once the body is moving fast enough that the :math:`b v^2`
drag force matches the force of gravity.

Now for a discussion of the full solution.  There is an article at:
https://philosophicalmath.wordpress.com/2017/10/21/terminal-velocity-derivation/
in which they focus on the question of calculating the velocity.

This they find to be:

.. math::

   v(t) = \sqrt{\frac{mg}{b}} \tanh\left(t \sqrt{\frac{b g}{m}} +
   \arctanh\left(v_0 \sqrt{\frac{b}{m g}}\right)\right)

The :math:`\tanh()` function is integrable, so one can also find
:math:`y(t)` exactly from this equation, but their main focus is on
how to find the terminal velocity:

.. math::

   \lim_{t\to\infty} v(t) = \lim_{t\to\infty} \left( \dots \right) =
   \sqrt{\frac{m g}{b}}

So the terminal velocity depends on the mass, the acceleration of
gravity, and (inversely) on the coefficient :math:`b`.  This matches
our intuition, and if we look at the values in our program (at the
time of writing this paragraph):

.. math::

   m & = 1 \; kg \\
   g & = 9.8 \; m/s^2 \\
   b & = 0.05 \; {\rm kg} / m \\
   \dots & \implies v_{\rm terminal} = 14 \; m/s

which is what our numerical solution found.

One more thing to say about drag: when certain conditions are
satisfied for an object falling in a fluid (or having the fluid move
around it), the coefficient :math:`b` can be given by:

.. math::

   b = \frac{1}{2} \rho C_d A

where :math:`\rho` is the fluid density, :math:`C_d` is the drag
coefficient, and :math:`A` is the cross-sectional area of our falling
body.

	       
The harmonic oscillator
=======================

The motion of a mass attached to a spring is described by Newton's
law, where the force produced by the spring is given (once you ignore
all friction effects) by Hooke's law:

.. math::

   F = - k x

Here is how to read that equation: :math:`x` is a small displacement
of the mass when you pull it from its rest position.  :math:`k` is
the *sprint constant* - a bigger value of :math:`k` means that we have
a stiffer spring which will return to its resting point more
vigorously.

If you *push* the spring instead of pulling you get (for small
displacements) the same force in the opposite direction.

The resulgint system is called a "harmonic oscillator".  The Wikipedia
page on the harmonic oscillator points out its importance in physics
with (approximately) this wording:

   The harmonic oscillator model is very important in physics, because
   any mass subject to a force in stable equilibrium acts as a
   harmonic oscillator for small vibrations. Harmonic oscillators
   occur widely in nature and are exploited in many human-made
   devices, such as clocks and radio circuits. They are the source of
   virtually all sinusoidal vibrations and waves.


The simple harmonic oscillator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Applying Newton's law, as we did for the falling body but this time
with the spring force, we get:

.. math::

   m \frac{d^2x}{dt^2} & = -k x \\
   \implies m \frac{d^2x}{dt^2} + k x & = 0

Let us look at this differential equation in its simplest form.  The
question that comes to mind is: "what function, when you take its
derivative twice, gives you itself back again with a minus sign?"

The answer to that is either :math:`\sin()` or :math:`\cos()`, or an
exponential with an imaginary multiple of the argument.

So we expect the solution to look something like:

.. math::

   x(t) = A \cos(C t) + B \sin(C t)

where :math:`A` and :math:`B` are the familiar arbitrary constants,
and :math:`C` is some kind of folding of the parameters of the
differential equation, like mass and spring constant.

A harmless simplification is to assume that we are plucking and
letting go with an initial velocity of 0, which makes the
:math:`\sin()` term go away, and we are left with just the cosine
term:

.. math::

   x(t) = A \cos(C t)

If we plug that into the differential equation we see that it works
when we set the parameter :math:`C` to be:

.. math::

   C = \sqrt{\frac{k}{m}}

We usually call this :math:`\omega_0`, the natural frequency of the
oscillator:

.. math::

   \omega_0 = \sqrt{\frac{k}{m}}

and our solution is:

.. math::

   x(t) = A \cos(\omega_0 t)

This is a sinusoidal wave with amplitude :math:`A` and frequency
:math:`\omega_0`.

The velocity will be:

.. math::

   v(t) = -A \omega_0 \cos(\omega_0 t)

Think through the proportionalities involved here: the frequency is
higher when the spring is stiffer, and lower when the mass is bigger.
The same goes for the velocity.  Does that match your intuition?

Without any other forces acting on it, this spring will keep going
forever.

The damped harmonic oscillator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To model the spring as if it was happening in the real world, we need
to account for friction.  In physics the force given by sliding
friction is usually proportional to the velocity:

.. math::

   F_{\rm friction} = - C \frac{dx}{dt}

adding this to the spring force gives this differential equation for
the damped harmonic oscillator:

.. math::

   m \frac{d^2x}{dt^2} + C \frac{dx}{dt} + k x & = 0 \\
   \frac{d^2x}{dt^2} + \frac{C}{m} \frac{dx}{dt} + \omega_0^2 x & = 0

This can be solved analytically and we get:

.. math::

   x(t) = A e^{-\zeta \omega_0 t} \cos\left(\omega t\right)

If you look at this closely you see that:

* The frequency :math:`omega` is shifted a bit from the natural
  :math:`\omega_0` of the undamped system.  The full relationship is:

   .. math::
      
      \omega  = \omega_0 \sqrt{1 - \left(\frac{C}{2 \sqrt{m
      k}}\right)^2}

* There is a damping term :math:`\exp{(-\zeta \omega_0 t)}` where
  :math:`\zeta = C / (2\sqrt{m k})`.  This will make the oscillations
  damp out very quickly if :math:`C` is big, and more gradually if
  :math:`C` is small.

In the program ``damped-spring-with-friction.c`` below, you can model
a damped oscillating spring.

.. _damped-spring-with-friction-c:

.. literalinclude:: damped-spring-with-friction.c
   :language: c
   :caption: ``damped-spring-with-friction.c`` shows exact solution
             and approximation for a spring.

Run this program with
::

   gcc -o damped-spring-with-friction damped-spring-with-friction.c -lm
   ./damped-spring-with-friction > damped-spring-with-friction.dat

Now let's use gnuplot to graph the damped spring:

::

   gnuplot
   # and at the gnuplot> prompt:
   set grid
   set title "damped harmonic oscillator"
   set xlabel "time"
   set ylabel "amplitude"
   plot "damped-spring-with-friction.dat" using 1:2 with lines title  "damped oscillator approximate"
   replot  "damped-spring-with-friction.dat" using 1:4 with lines title "damped oscillator exact"

It should look something like this:

.. _fig-damped-spring:

.. figure:: damped-spring.*
   :width: 75%

If you look at the code you can see that you can do experiments by
changing the ``damp`` parameter at the top, and you could even
experiment wtih adding a driving force to the oscillator by changing
the function ``acceleration()``.


The non-linear pendulum
~~~~~~~~~~~~~~~~~~~~~~~

The force along the trajectory of mass on a string (pendulum) is:

.. math::

   F(t) = - m g \sin(\theta)

This gives us an expression of Newton's law as:

.. math::

   m L \frac{d^2 \theta(t)}{dt^2}  + m g \sin(\theta(t)) & = 0 \\
   \frac{d^2 \theta(t)}{dt^2}  + (g / L) \sin(\theta(t)) & = 0 \\

For small angles we have :math:`\sin(\theta) \approx \theta`, so we
get:

.. math::

   \frac{d^2\theta}{dt^2} + (g / L) \theta(t) = 0 

this last equation is the same as the simple harmonic oscillator
equation, where our fundamental frequency is given by:

.. math::

   \omega_0 = \sqrt{\frac{g}{L}}

once again you should look at the proportionalities in that equation
and see if you agree with them.

The full non-linear equation, where we have :math:`\sin(\theta(t))`, is
very difficult to solve.  This has been solved in closed form, but the
solutions are very complex and involve their own set of approximations
to calculate *elliptical integrals.*

Since the solution is so complex, and the slightest variation in the
kind of force we are contemplating would make the equation
completely unsolvable, it becomes interesting to use our differential
equation solver to calculate it.

Type or paste in the program ``nonlinear-pendulum.c``:

.. _nonlinear-pendulum-c:

.. literalinclude:: nonlinear-pendulum.c
   :language: c
   :caption: ``nonlinear-pendulum.c`` approximates the equation for a
             nonlinear pendulum.


Run this program with
::

   gcc -o nonlinear-pendulum nonlinear-pendulum.c -lm
   ./nonlinear-pendulum > nonlinear-pendulum.dat

Now let's use gnuplot to graph the damped spring:

::

   gnuplot
   # and at the gnuplot> prompt:
   set grid
   set title "damped harmonic oscillator"
   set xlabel "time"
   set ylabel "amplitude"
   plot "nonlinear-pendulum.dat" using 1:2 with lines title  "damped oscillator approximate"
   replot  "nonlinear-pendulum.dat" using 1:4 with lines title "damped oscillator exact"

It should look something like this:

.. _fig-nonlinear-pendulum:

.. figure:: nonlinear-pendulum.*
   :width: 75%

Take a careful look at that last plot, and at the source code,
specifically the setting of:

.. code-block:: c

   double theta_0 = 0.3;           /* radians */

0.3 radians is a rather small angle, so the approximation
:math:`\sin(\theta) \approx \theta` almost holds at :math:`\sin(0.3) =
0.29552`.  But after a few periods you see that the solutions diverge.

If you set ``theta_0`` to be 0.1 then the approximation will work for
a long time.  If you set it to be 0.8 you will see the curves diverge
quite soon.
