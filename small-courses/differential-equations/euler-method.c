// this program uses Euler's method to approximate solutions to the
// differential equation dy/dx = f(x, y)

// compile with: gcc -o euler-method euler-method.c -lm
// run with: ./euler-method > euler-method.dat
// plot in gnuplot with:
// plot "euler-method.dat" using 1:2 with lines
// replot  "euler-method.dat" using 1:3 with lines

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/* prototypes for functions below */
double RHS_parab(double x, double y);
double exact_parab(double x, double y);


int main(int argc, char *argv[])
{
  double xinitial = -5;
  double yinitial = 35;
  double interval = 10; 	/* how many seconds */

  /* handle command line options that allow the user to select number
     of steps */
  int n_steps;
  if (argc == 1) {
    n_steps = 1000;
  } else if (argc == 2) {
    n_steps = atoi(argv[1]);
  } else {
    fprintf(stderr, "error: usage is %s [n_steps]\n", argv[0]);
    return 1;
  }

  /* put out a wee bit of metadata with the "low effort metadata"
     (LEM) structured comment approach */
  printf("##COMMENT: Euler method solution to dy/dx = f(x, y)\n");
  printf("##N_STEPS: %d\n", n_steps);
  printf("##COLUMN_DESCRIPTIONS: x    y_approx    y_exact   slope\n");

  double dx = interval/n_steps;
  double xprev = xinitial;
  double yprev = yinitial;
  double exact = exact_parab(xprev, yprev);
  printf("%lf   %lf   %lf\n", xprev, yprev, exact);
  for (int j = 0; j < n_steps; j++) {
    // calculate right hand side (RHS) with (xi, yi)
    double slope = RHS_parab(xprev, yprev);
    // increase x by step size
    double xnew = xprev + dx;
    // use tangent line to approximate next y value from previous y value
    double ynew = yprev + dx * slope;
    // find exact exact_solution to compare approximation with
    double exact = exact_parab(xnew, ynew);
    printf("%g   %g   %g   %g\n", xnew, ynew, exact, slope);
    // now reset the cycle
    xprev = xnew;
    yprev = ynew;
  }
  return 0;
}

// returns RHS (right hand side)
double RHS_parab(double x, double y)
{
  return 2*x - 2;
}

// returns exact exact_solution
double exact_parab(double x, double y)
{
  return x*x - 2*x;
}
