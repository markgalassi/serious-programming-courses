// this program uses Euler's method to approximate solutions to the
// falling body equation d^2y/dt^2 = -g

// compile with: gcc -o falling-body falling-body.c -lm
// run with: ./falling-body > falling-body.dat
// plot in gnuplot with:
// plot "falling-body.dat" using 1:2 with lines
// replot  "falling-body.dat" using 1:3 with lines

#include <stdio.h>
#include <math.h>

//acceleration due to gravity
double g = 9.8;
double yinit = 100;
double vinit = 0.1;

double acceleration(double t, double y)
{
  return -g;
}

// returns exact solution
double falling_body_exact(double st)
{
  return yinit + vinit*st - 0.5*g*pow(st,2);
}

int main()
{
  double tinit = 0;

  // define initial variables, step size
  double duration = 10; /* how many seconds */
  int n_steps = 10000;
  double dt = duration/n_steps;

  printf("##COMMENT: Euler method solution to falling body d^y/dx^2 = -g\n");
  printf("##N_STEPS: %d\n", n_steps);
  printf("##COLUMN_DESCRIPTIONS: t    y_approx   v_approx    y_exact   acc\n");

  // set the variables based on our initial values
  double tprev = tinit;
  double yprev = yinit;
  double vprev = vinit;
  double exact = falling_body_exact(tprev);
  double acc = acceleration(tprev, yprev);
  printf("%g   %g   %g   %g   %g\n", tprev, yprev, vprev, exact, acc);
  for (int j = 0; j < n_steps; j++) {
    // solve differential with initial values
    acc = acceleration(tprev, yprev);
    // using acceleration, update velocity
    double vnew = vprev + dt * acc;
    // now, using velocity, update position
    double ynew = yprev + vnew * dt;
    // increase t by step size
    double tnew = tprev + dt;
    tprev = tnew;
    yprev = ynew;
    vprev = vnew;
    // find exact solution to compare approximation with
    double exact = falling_body_exact(tnew);
    printf("%g   %g   %g   %g   %g\n", tnew, ynew, vnew, exact, acc);
  }
  return 0;
}  
