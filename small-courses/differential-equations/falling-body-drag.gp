set grid
plot [] [0:] "falling-body-drag.dat" using 1:2 with lines title "with drag", \
     "falling-body-drag.dat" using 1:4 with lines title "without drag"
