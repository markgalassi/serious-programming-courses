// compile with: gcc -o damped-spring-with-friction damped-spring-with-friction.c -lm
// run with: ./damped-spring-with-friction > damped-spring-with-friction.dat
// plot in gnuplot with:
// plot "damped-spring-with-friction.dat" using 1:3 with lines
// replot  "damped-spring-with-friction.dat" using 1:4 with lines

#include <stdio.h>
#include <math.h>

//acceleration due to gravity
double g = 9.8;
//some physics values for the spring equation
double k = 3.0;
double m = 2.0;
//physics for air friction
double air_friction = 3;
//physics for frictional damping force
double damp = 0.5;
/* double damp = 0.0000005; */
double F0 = 10;			/* constant driving force */
double amplitude = 2;

//returns differential
double acceleration(double t, double x, double v)
{
  double omega_0 = sqrt(k / m);
  return - damp * v / m - omega_0*omega_0 * x;
  /* return (-k*x - damp*v + F0*cos(8*t)) / m; */
}

// returns exact solution for harmonic motion
double harmonic_exact(double t)
{
  return amplitude*cos(sqrt(k/m - damp*damp/(4*m*m))*t)*exp((-damp/(2*m)) * t);
}
 
int main()
{
  double tinit = 0;

  // define initial variables, step size
  double duration = 30; /* how many seconds */
  int n_steps = 10000;
  double dt = duration/n_steps;

  printf("##COMMENT: Euler method solution to damped harmonic oscillator\n");
  printf("###COMMENT: m d^y/dx^2 + C dx/dt + k x = 0");
  printf("##N_STEPS: %d\n", n_steps);
  printf("##COLUMN_DESCRIPTIONS: t    x_approx   v_approx no_damp_exact acc\n");

  // set the variables based on our initial values
  double tprev = tinit;
  double xprev = amplitude;
  double vprev = 0;             /* pluck, then release from rest position */
  double exact_harmonic = harmonic_exact(tinit);
  double acc = acceleration(tprev, xprev, vprev);
  printf("%g   %g   %g   %g   %g\n", tprev, xprev, vprev, exact_harmonic, acc);
  for (int j = 0; j < n_steps; j++) {
    // solve differential with initial values
    acc = acceleration(tprev, xprev, vprev);
    // using acceleration, update velocity
    double vnew = vprev + dt * acc;
    // now, using velocity, update position
    double xnew = xprev + vnew * dt;
    // increase t by step size
    double tnew = tprev + dt;
    tprev = tnew;
    xprev = xnew;
    vprev = vnew;
    // find exact solution to compare approximation with
    double exact_nodamp = harmonic_exact(tnew);
    printf("%g   %g   %g   %g   %g\n", tnew, xnew, vnew, exact_nodamp, acc);
  }
}  
