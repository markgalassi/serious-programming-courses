#! /bin/sh

echo "running: $0"
echo "args: $*"

## make pdf files out of some svg files - this allows us to "make latexpdf"
echo "euler-method.pdf: euler-method.svg ;	rsvg-convert -f pdf -o euler-method.pdf euler-method.svg" | make -f-

echo "%.pdf: %.svg ;	rsvg-convert -f pdf -o \$@ \$<" | make -f- Euler-method-from-wikipedia.pdf


## for the simple harmonic oscillator, we need to convert to png (so
## that latex with pdf output can pick it up) and then pick a specific
## frame
# convert Simple-harmonic-oscillator.gif Simple-harmonic-oscillator.png
# cp Simple-harmonic-oscillator-0.png Simple-harmonic-oscillator.png

## run the programs to get output for the plots
echo "%: %.c ;	gcc \$^ -o \$@ -lm" | make -f- falling-body
echo "%.dat: % ;	./\$< > \$@" | make -f- falling-body.dat

echo "%: %.c ;	gcc \$^ -o \$@ -lm" | make -f- damped-spring-with-friction
echo "%.dat: % ;	./\$< > \$@" | make -f- damped-spring-with-friction.dat

echo "%: %.c ;	gcc \$^ -o \$@ -lm" | make -f- nonlinear-pendulum
echo "%.dat: % ;	./\$< > \$@" | make -f- nonlinear-pendulum.dat

echo "%: %.c ;	gcc \$^ -o \$@ -lm" | make -f- euler-method
echo "%.dat: % ;	./\$< > \$@" | make -f- euler-method.dat
## now run it with fewer steps to get a bad approximation
echo "%-bad-approximation.dat: % ;	./\$< 10 > \$@" | make -f- euler-method-bad-approximation.dat

echo "%: %.c ;	gcc \$^ -o \$@ -lm" | make -f- falling-body-drag
echo "%.dat: % ;	./\$< 1.7 1000 0.4 > \$@" | make -f- falling-body-drag.dat
# gcc falling-body-with-air-resistance.c -o falling-body-with-air-resistance -lm
# ./falling-body-with-air-resistance > falling-body-with-air-resistance.dat
