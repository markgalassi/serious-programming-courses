#! /usr/bin/env python

import random

def main():
    N_sq = 0
    N_circ = 0
    for i in range(100):
        N_sq += 1
        x = random.random()
        y = random.random()
        if x**2 + y**2 < 1:     # it's in the circle
            N_circ += 1
        ## we're done so we use the formula A_sq = 4, A_circ = pi, so pi =
        ## 4*N_circ/N_sq
        pi_estimate = 4.0*float(N_circ)/float(N_sq)
        print('%d    %g' % (i, pi_estimate))

main()
