#! /usr/bin/env python3

import sys
import math

def main():
    """Calculate a series up to a certain value of k which is given on the
    command line
    """
    # series_term = series_term_leibniz
    series_term = series_term_zeta_2
    verbose = True
    try:
        lower = int(sys.argv[1])
        upper = int(sys.argv[2])
    except:
        print('usage: %s lower_bound upper_bound' % sys.argv[0])
        sys.exit(1)

    sum_to_here = 0
    for k in range(lower, upper+1):
        sum_to_here += series_term(k)
        # pi_approximation = 4 * sum_to_here
        pi_approximation = math.sqrt(6 * sum_to_here)
        if verbose:             # print the intermediate values
            print(k, '  ', sum_to_here, '  ', pi_approximation,
                  '  ', math.fabs(math.pi - pi_approximation), '  ',
                  math.fabs(math.pi - pi_approximation) / math.pi)

    print('result after %d iterations: %g' % (upper, sum_to_here))
    ## FIXME: the formula below changes for different series
    print('## pi_approximation_final:', pi_approximation)


def series_term_leibniz(k):
    """Returns the kth term of the leibniz series for pi.  Sum starts at
    1.  Note that to print the resulting calculation of pi you will
    need to print 4*sum_to_here.
    """
    return (-1)**(k+1) / (2*k - 1)


def series_term_zeta_2(k):
    """Returns the kth term of the riemann function zeta(2).  Sum starts
    at 1.  Note that to print the resulting calculation of pi you will
    need to print sqrt(6.0*sum_to_here)."""
    return 1.0/(k**2)

## you can add more series term functions here

if __name__ == '__main__':
    main()
