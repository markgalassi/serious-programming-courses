#! /bin/sh

echo "entering script $0"
echo "command line $*"

## word frequency analysis based on Proust
echo "swanns-way-english.txt:  ;	wget --continue --output-document swanns-way-english.txt http://www.gutenberg.org/cache/epub/1128/pg1128.txt" | make -f-
echo "swanns-way-freq-english.out: swanns-way-english.txt ./word-freq-rank.py;	./word-freq-rank.py swanns-way-english.txt > swanns-way-freq-english.out" | make -f-
echo "swanns-way-french.txt:  ;	wget --continue --output-document swanns-way-french.txt https://www.gutenberg.org/files/2650/2650-0.txt" | make -f-
echo "swanns-way-freq-french.out: swanns-way-french.txt ./word-freq-rank.py;	./word-freq-rank.py swanns-way-french.txt > swanns-way-freq-french.out" | make -f-

## Benford's law based on physical constants
echo "allascii.txt: ;	wget --continue https://physics.nist.gov/cuu/Constants/Table/allascii.txt" | make -f-
echo "first-digits.dat: allascii.txt ;	cat allascii.txt | cut -c 61 | grep '[1-9]' > first-digits.dat" | make -f-
echo "first-digit-freq.svg: first-digits.dat ;	./first-digit-hist.py first-digits.dat first-digit-freq.svg" | make -f-
echo "first-digit-freq.pdf: first-digits.dat ;	./first-digit-hist.py first-digits.dat first-digit-freq.pdf" | make -f-
echo "nasdaq-list.csv: ;        wget --continue --output-document nasdaq-list.csv 'blob:https://www.nasdaq.com/d686e5b3-5758-4885-96bd-b496e65b8661'" | make -f-
echo "first-digit-nasdaq_sale.svg: nasdaq-list.csv;	./first-digit-nasdaq.py nasdaq-list.csv first-digit-nasdaq.svg" | make -f-
echo "first-digit-nasdaq_sale.pdf: nasdaq-list.csv;	./first-digit-nasdaq.py nasdaq-list.csv first-digit-nasdaq.pdf" | make -f-
echo "first-digit-nasdaq_cap.svg: nasdaq-list.csv;	./first-digit-nasdaq.py nasdaq-list.csv first-digit-nasdaq.svg" | make -f-
echo "first-digit-nasdaq_cap.pdf: nasdaq-list.csv;	./first-digit-nasdaq.py nasdaq-list.csv first-digit-nasdaq.pdf" | make -f-
