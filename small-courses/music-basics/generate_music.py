#! /usr/bin/env python3

"""This program generates a sequence of musical notes of a given
duration.  You specify the sequence as a list of note specifications
(for example 'Sol' or 'G', or ('G', 5), or ('G', 5, 'sharp')) and time
specifications (for example 1/4 or 1/2)."""

## Authors: Jacqueline Bell and Mark Galassi

## resources:
## https://en.wikipedia.org/wiki/Musical_note#Note_frequency_(hertz)
## http://www.animations.physics.unsw.edu.au/jw/frequency-pitch-sound.htm

import math
import sys
from math import sin, pi

def main():
    # test_note_functions()

    #POPCORN: 
    notes_popcorn = [
        (('A', 4), 1/8), ('G', 1/8), ('A', 1/8), ('E', 1/8),
        ('C', 1/8), ('E', 1/8), (('A', 3), 1/4),
        (('A', 4), 1/8), ('G', 1/8), ('A', 1/8), ('E', 1/8),
        ('C', 1/8), ('E', 1/8), (('A', 3), 1/4),
        (('A', 4), 1/8), ('B', 1/8), (('C', 5), 1/8),
        (('B', 4), 1/8), (('C', 5), 1/8), (('A', 4), 1/8),
        ('B', 1/8), ('A', 1/8), ('B', 1/8), ('G', 1/8),
        ('A', 1/8), ('G', 1/8), ('E', 1/8), ('G', 1/8),
        ('A', 1/4),
        (('C', 4), 1/8), ('A', 1/8), ('B', 1/8), (('F', 4, 'sharp'), 1/8),
        ('D', 1/8), (('F', 4, 'sharp'), 1/8), (('B', 3), 1/4),
        (('A', 4), 1/8), ('G', 1/8), ('A', 1/8), ('E', 1/8),
        ('C', 1/8), ('E', 1/8), (('A', 3), 1/4),
        (('A', 4), 1/8), ('B', 1/8), (('C', 5), 1/8),
        (('B', 4), 1/8), (('C', 5), 1/8), (('A', 4), 1/8),
        ('B', 1/8), ('A', 1/8), ('B', 1/8), ('G', 1/8),
        ('A', 1/8), ('G', 1/8), ('E', 1/8), ('G', 1/8),
        ('A', 1/4),
    ]
    notes_starwars = [(('C', 4), 1/2), ('G', 1/2), ('F', 1/8), ('E', 1/8), ('D', 1/8),
                      (('C', 5), 1/2), (('G', 4), 1/2)]

    notes_bigben = [(('E', 4), 1/2), ('C', 1/2), ('D', 1/8), ('G', 1/8), ('G', 1/8),
                    (('D', 4), 1/2), (('E', 4), 1/2), (('C', 4), 1/2)]

    #SUPERMARIO:
    # step_sequence = [14 ,11  ,14  , 8 ,  4  , 8  , 0,14, 11 , 14  , 8 , 4  , 8  , 0]
    # step_sequence = [14 ,11  ,14  , 8 ,  4  , 8  , 0,14, 11 , 14  , 8 , 4  , 8  , 0]
    # step_duration = [.5 ,0.5 ,0.5 ,0.5, 0.4, 0.4 , 0.5,  .5 , 0.5 ,0.5 ,.5 ,0.4,0.4,0.5]
    # assert(len(step_sequence) == len(step_duration))

    # notes = notes_popcorn
    # notes = notes_starwars
    notes = notes_bigben
    play_tune(notes)


def play_tune(note_sequence):
    """Takes a note sequence and plays it out.  At this time it plays a
    simple sin wave with no harmonics.
    """
    duration_base = 2           # 2 seconds for a full note
    sample_rate = 48000
    print('; Sample Rate %d' % sample_rate)
    print('; Channels 2')

    time = 0 # we track time throughout, since it is used in the format
    for note_spec, duration_frac in note_sequence:
        # duration_frac = duration_sequence[seq_number]
        octave = 4              # default octave
        sharp_or_flat = None
        if type(note_spec) == tuple:
            print('; TUPLE', note_spec)
            note = note_spec[0]
            octave = note_spec[1]
            if len(note_spec) == 3:
                sharp_or_flat = note_spec[2]
        else:
            print('; NOTE', note_spec)
            note = note_spec
        ## decide how many samples to play
        n_samples = duration_frac * duration_base * sample_rate
        ## now we to handle a "rest" note
        print('; time_before:', time, '-', note_spec, ' -- ', note, 'rest', note == 'rest')
        freq = note2freq(octave, note, sharp_or_flat)
        print('; octave, note, frequency, duration_frac', 
              octave, note, freq, duration_frac)
        # print('n_samples: %d' % int(n_samples))
        time = play_freq(time, freq, n_samples)
        print('; time_after:', time)


def play_freq(time, freq, n_samples):        
    """Plays the note specified by freq, for a duration of n_samples,
    starting at the given time.  Note that if freq is zero then we
    are basically playing a rest note."""
    for i in range(int(n_samples)):
        time = time + 1.0 / 48000.0
        left = sin(2*pi*freq*time) # simple sin wave
        right = sin(2*pi*freq*time)
        print('%16.10f      %16.10f      %16.10f' % (time, left, right))
    return time


def note2freq(octave, note, sharp_or_flat):
    """Takes a note specification and returns the frequency of that note.
    If note is 'rest' then we return a frequency of zero."""
    ## refer to https://en.wikipedia.org/wiki/Musical_note#Note_frequency_(hertz)
    if note == 'rest':
        freq = 0
    else:
        A4_freq = 440               # A above middle C
        n_steps = note2steps(octave, note, sharp_or_flat)
        freq = A4_freq * math.pow(2, n_steps/12.0)
    return freq


def note2steps(octave, note, sharp_or_flat):
    """Takes a note specification (octave, note and sharp_or_flat) and
    converts it to the number of steps above (or below if negative)
    middle A
    """
    ## a mapping from the notes within an octave to the number of
    ## steps between that note and the La (A) of that octave
    note2steps_within_octave = {'Do': -9,
                                'Re': -7,
                                'Mi': -5,
                                'Fa': -4,
                                'Sol': -2,
                                'La': 0,
                                'Si': 2,
                                'C': -9,
                                'D': -7,
                                'E': -5,
                                'F': -4,
                                'G': -2,
                                'A': 0,
                                'B': 2,
                                'rest': 0}
    n_steps = (octave - 4) * 12 # first give the octave offset
    n_steps = n_steps + note2steps_within_octave[note]
    if sharp_or_flat:
        if sharp_or_flat == 'sharp':
            n_steps = n_steps + 1
        elif sharp_or_flat == 'flat':
            n_steps = n_steps - 1
        else:
            raise Exception('sharp_or_flat must be either "sharp" or "flat"')
    return n_steps


def test_note_functions():
    """A simple set of notes to test the note2steps() function."""
    test_note_sequence = [(4, 'Sol', None),
                          (3, 'Do', 'sharp'),
                          (4, 'La', None),
                          (4, 'B', None),
                          (6, 'E', 'flat'),
                          (0, 'C', 'sharp')]
    for note_spec in test_note_sequence:
        print(note_spec, note2steps(*note_spec), note2freq(*note_spec))


main()
