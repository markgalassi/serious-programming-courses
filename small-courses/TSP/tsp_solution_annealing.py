#! /usr/bin/env python3

"""This program demonstrates a greedy solution to the traveling
salesman problem.
"""

import random
import time
import math
import scipy
import scipy.optimize
# from scipy.optimize import anneal
# import scipy.optimize.anneal
from scipy.optimize import basinhopping
import numpy as np
# import scipy.optimize.basinhopping

## we use the tkinter widget set; this seems to come automatically
## with python3 on ubuntu 16.04, but on some systems one might need to
## install a package with a name like python3-tk
from tkinter import *

# canvas_width = 640
# canvas_height = 480
canvas_width = 1280
canvas_height = 1024
n_cities = 200
algo_name = 'basinhopping'

# func = lambda x: np.cos(14.5 * x - 0.3) + (x + 0.2) * x
def func(x):
    print('.', end="")
    return np.cos(14.5 * x - 0.3) + (x + 0.2) * x

class RealTakeStep(object):
    def __init__(self, stepsize=0.5):
        self.stepsize = stepsize
    def __call__(self, x):
        s = self.stepsize
        return s*random.random()

def realcallback(x, f, accept):
    print(x, f, accept)

def main():
    x0=[1.]
    realstep = RealTakeStep()
    ret = basinhopping(func=func, x0=x0, take_step=realstep, niter=22,
                       callback=realcallback)
    print('ret:', ret)

def other_main():
    ## prepare a basic canvas
    root = Tk()
    w = Canvas(root, 
               width=canvas_width,
               height=canvas_height)
    w.pack()       # boiler-plate: we always call pack() on tk windows
    city_list = make_random_cities(0, canvas_width-1, 
                                   0, canvas_height-1, n_cities)
    update_drawing(w, city_list)

    ## now use scipy's annealing routines
    # scipy.optimize.basinhopping(func=calculate_path_length, 
    #                             x0=[], args=(city_list,))
    x0 = city_list[:]
    print('x0:', x0)
    scipy.optimize.basinhopping(func=calculate_path_length, 
                                x0=[], niter=100, T=6, stepsize=1,
                                take_step=siman_take_step)

    mainloop()

def update_drawing(w, city_list):
    w.delete("all")
    ## we draw the full list of cities (without connections) and
    ## then the current path in a different color, so we can see
    ## the progress.
    draw_city_path(w, city_list, color='black', connect=True)
    write_info_at_bottom(w, canvas_width, canvas_height, city_list)

def find_closest_city(city, remaining_cities):
    """returns the index of the closest city in remaining_cities"""
    closest_index = 0
    closest_distance = sys.float_info.max
    for i in range(len(remaining_cities)):
        r = distance(city, remaining_cities[i])
        if r < closest_distance:
            closest_distance = r
            closest_index = i
    return closest_index

def draw_city_path(w, city_list, color='white', connect=False):
    """draws lines between the cities"""
    for city in city_list:
        draw_city(w, city[0], city[1], color=color)
    draw_city(w, city_list[0][0], city_list[0][1], color='blue', name='Home')
    ## now draw lines between them
    if connect:
        for i in range(len(city_list)-1):
            w.create_line(city_list[i][0], city_list[i][1], 
                          city_list[i+1][0], city_list[i+1][1])

def make_random_cities(xmin, xmax, ymin, ymax, n_cities):
    """returns a list of randomly placed cities in the given rectangle"""
    city_list = []
    for i in range(n_cities):
        x = random.randint(xmin, xmax)
        y = random.randint(ymin, ymax)
        city_list.append((x, y))
    return city_list

def draw_city(w, x, y, color='yellow', name=None):
    """draws a city; if a name is given also writes the name of it"""
    w.create_oval(x-5, y-5, x+5, y+5, fill=color)
    ## if a name was given, write in the name
    if name:
        w.create_text(x, y+10, text=name)

def write_info_at_bottom(w, width, height, city_list):
    n_cities = len(city_list)
    total_distance = calculate_path_length(city_list)
    w.create_text(width/2, height-60,
                  text='algorithm picked: %s' % algo_name,
                  fill='red')
    w.create_text(width/2, height-40, text='n_cities: %d' % n_cities,
                  fill='red')
    w.create_text(width/2, height-20,
                  text='total_distance: %16.12g' % total_distance,
                  fill='red')

def distance(c1, c2):
   x1 = c1[0]
   y1 = c1[1]
   x2 = c2[0]
   y2 = c2[1]
   r = math.sqrt((x2-x1)**2 + (y2-y1)**2)
   return r

def calculate_path_length(city_list):
    print('entering calculate_path_length', len(city_list))
    total_length = 0
    for i in range(len(city_list)-1):   ## iterate up to the second-last city
        c1 = city_list[i]
        c2 = city_list[i+1]
        length = distance(c1, c2)
        total_length += length
        print('tl:', total_length)
    ## now add in the path length required to get back home
    print('list:', city_list)
    total_length += distance(city_list[-1], city_list[0])
    # print('=========================================')
    # print('city_list:', city_list)
    print('tl:', total_length)
    return total_length

def siman_take_step(city_list):
    print('STEP:', len(city_list))
    tmp = city_list[3]
    city_list[3] = city_list[7]
    city_list[7] = tmp
    return city_list


def hillclimbing_take_step(city_list, shorter=True):
    """Swap two adjacent cities, if the new path is shorter we
    take that step."""
    print('ENTER: step', len(city_list))
    ## the first city of the pair to be swapped needs not be the first
    ## or the last city
    print('before', len(city_list))
    path_before = calculate_path_length(city_list)
    c1_ind = random.randint(1, len(city_list)-2)
    c2_ind = c1_ind
    while c2_ind == c1_ind:
        c2_ind = random.randint(1, len(city_list)-2)
    ## extract the cities
    c1 = city_list[c1_ind][:]
    c2 = city_list[c2_ind][:]
    ## make the swap
    city_list[c1_ind] = c2[:]
    city_list[c2_ind] = c1[:]
    ## see if we got better or not
    print('after', len(city_list))
    path_after = calculate_path_length(city_list)
    take_step = False
    if shorter and path_after <= path_before:
        take_step = True
    if not shorter and path_after >= path_before:
        take_step = True
    if take_step:
        print('(IND_%d, %d, %d, %16g) <-> (IND_%d, %d, %d, %16g): YAY!' 
              % (c1_ind, c1[0], c1[1], path_before,
                 c2_ind, c2[0], c2[1], path_after))
        return (c1, c2)
    else:                       # no step
        print('(IND_%d, %d, %d, %16g) <-> (IND_%d, %d, %d, %16g): sigh...' 
              % (c1_ind, c1[0], c1[1], path_before,
                 c2_ind, c2[0], c2[1], path_after))
        ## undo the swap
        city_list[c1_ind] = c1[:]
        city_list[c2_ind] = c2[:]
        return None


def hillclimbing_n_swaps(city_list, n_swaps=2, shorter=True):
    """Swap two adjacent cities, if the new path is shorter we
    take that step."""
    ## the first city of the pair to be swapped needs not be the first
    ## or the last city
    print('before:', len(city_list))
    path_before = calculate_path_length(city_list)
    c1_ind = random.randint(1, len(city_list)-2)
    c2_ind = c1_ind
    while c2_ind == c1_ind:
        c2_ind = random.randint(1, len(city_list)-2)
    ## extract the cities
    c1 = city_list[c1_ind][:]
    c2 = city_list[c2_ind][:]
    ## make the swap
    city_list[c1_ind] = c2[:]
    city_list[c2_ind] = c1[:]
    ## see if we got better or not
    print('after:', len(city_list))
    path_after = calculate_path_length(city_list)
    take_step = False
    if shorter and path_after <= path_before:
        take_step = True
    if not shorter and path_after >= path_before:
        take_step = True
    if take_step:
        print('(IND_%d, %d, %d, %16g) <-> (IND_%d, %d, %d, %16g): YAY!' 
              % (c1_ind, c1[0], c1[1], path_before,
                 c2_ind, c2[0], c2[1], path_after))
        return (c1, c2)
    else:                       # no step
        print('(IND_%d, %d, %d, %16g) <-> (IND_%d, %d, %d, %16g): sigh...' 
              % (c1_ind, c1[0], c1[1], path_before,
                 c2_ind, c2[0], c2[1], path_after))
        ## undo the swap
        city_list[c1_ind] = c1[:]
        city_list[c2_ind] = c2[:]
        return None

main()
