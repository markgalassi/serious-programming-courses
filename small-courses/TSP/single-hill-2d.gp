set pm3d
set samples 150
set isosamples 70
set hidden3d
splot exp(-(x-1)**2 - (y-0.5)**2)
