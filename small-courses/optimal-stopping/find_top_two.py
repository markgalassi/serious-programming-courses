import sys
import random
import matplotlib.pyplot as plt

def main():
    first_stopping_bounds = [0, 1] # bounds for stopping point
    second_stopping_bounds = [0, 1]
    n_candidates = 100
    n_trials = 10000

    lower_first_stop = round(first_stopping_bounds[0] * n_candidates)
    upper_first_stop = round(first_stopping_bounds[1] * n_candidates)
    lower_second_stop = round(second_stopping_bounds[0] * n_candidates)
    upper_second_stop = round(second_stopping_bounds[1] * n_candidates)
    
    max_chance_best_found = 0
    chances_best_found = []
    for first_stop in range(lower_first_stop, upper_first_stop):
        # order best found chances by both stopping points
        chances_best_found.append([])
        for second_stop in range(lower_second_stop, upper_second_stop):
            if second_stop <= first_stop:
                chances_best_found[-1].append(0)
                continue
            else:
                chance_best_found = find_chance_best_found(first_stop,
                                                           second_stop,
                                                           n_candidates,
                                                           n_trials)
                chances_best_found[-1].append(chance_best_found)
                if chance_best_found > max_chance_best_found:
                    max_chance_best_found = chance_best_found
                    best_first_stop = first_stop
                    best_second_stop = second_stop
                print(f'Tried stopping points {first_stop} and {second_stop}, '
                      f'resulting in a chance of {chance_best_found}.')

    print(f'Best stopping points were after candidates {best_first_stop} and '
          f'{best_second_stop}, with a {round(max_chance_best_found * 100, 4)}%'
          f' chance of finding one of the five best candidates')
    ys = []
    for i in range(len(chances_best_found)):
        ys.append(chances_best_found[i][best_second_stop - lower_second_stop])
    fig, ax = plt.subplots()
    ax.set_xlabel('Stopped after candidate')
    ax.set_ylabel('Chance top two found')
    ax.scatter(range(lower_first_stop, upper_first_stop), ys)
    ax.scatter(range(lower_second_stop, upper_second_stop),
               chances_best_found[best_first_stop - lower_first_stop])
    fig.savefig('find-top-two.png')
    fig.savefig('find-top-two.svg')
    fig.savefig('find-top-two.pdf')
    print('Saved file to find-top-two.png, .svg, and .pdf')
    if not (len(sys.argv) > 1 and sys.argv[1] == '--non-interactive'):
        plt.show()
    

def find_chance_best_found(first_stop, second_stop, n_items, n_trials):
    """returns the chance of finding the best item when stopping
    looking at given stopping points"""
    chance_best_found = 0
    for trial in range(n_trials):
        items = list(range(n_items))
        random.shuffle(items)
        bar = 0 # bar is the lowest you would accept after the looking period
        second_bar = 0
        for i in range(first_stop):
            if items[i] > bar:
                second_bar = bar
                bar = items[i]
        # if no further candidates hit the bar, you're stuck with the last one
        selection = items[-1]
        for i in range(first_stop, second_stop):
            if items[i] > bar:
                selection = items[i]
                break
            if items[i] > second_bar:
                second_bar = items[i]
        for i in range(second_stop, n_items):
            if items[i] > second_bar:
                selection = items[i]
                break
        if selection >= n_items - 2:
            chance_best_found += 1 / n_trials
    return chance_best_found

main()
