import sys
import random
import matplotlib.pyplot as plt

def main():
    stopping_bounds = [0, 1] # bounds for stopping point
    n_candidates = 100
    n_trials = 10000
    # chance that a candidate will still want the job after calling them back
    call_back_chance = 0.5

    lower_stop = round(stopping_bounds[0] * n_candidates)
    upper_stop = round(stopping_bounds[1] * n_candidates)

    max_chance_best_found = 0
    best_stop = 0
    chances_best_found = []
    for stop in range(lower_stop, upper_stop):
        chance_best_found = find_chance_best_found(stop, n_candidates,
                                                   n_trials, call_back_chance)
        chances_best_found.append(chance_best_found)
        if chance_best_found > max_chance_best_found:
            max_chance_best_found = chance_best_found
            best_stop = stop

    print(f'Best stopping point was after candidate {best_stop}, with a '
          f'{round(max_chance_best_found * 100, 4)}% chance of finding the '
          f'best candidate')
    fig, ax = plt.subplots()
    ax.set_xlabel('Stopped after candidate')
    ax.set_ylabel('Chance best found found')
    ax.scatter(range(lower_stop, upper_stop), chances_best_found)
    fig.savefig('call-back.png')
    fig.savefig('call-back.svg')
    fig.savefig('call-back.pdf')
    print('Saved file to call-back.png, .svg, and .pdf')
    if not (len(sys.argv) > 1 and sys.argv[1] == '--non-interactive'):
        plt.show()

def find_chance_best_found(stop, n_items, n_trials, call_back_chance):
    """returns the chance of finding the best candidate when stopping
    looking at a given stopping point"""
    chance_best_found = 0
    for trial in range(n_trials):
        items = list(range(n_items))
        random.shuffle(items)
        bar = 0 # bar is the lowest you would accept after the looking period
        for i in range(stop):
            if items[i] > bar:
                bar = items[i]
        # if no further candidates hit the bar, you're stuck with the last one
        selection = items[-1]
        for i in range(stop, n_items):
            if items[i] > bar:
                selection = items[i]
                break
        for i in reversed(range(selection, n_items)):
            if random.random() < call_back_chance * (1 - items.index(i)/100):
                selection = i
                break
        if selection >= n_items - 1:
            chance_best_found += 1 / n_trials
    return chance_best_found

main()
