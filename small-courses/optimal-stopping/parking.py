import sys
import random
import matplotlib.pyplot as plt

def main():
    stopping_bounds = [0, 1] # bounds for stopping point
    n_spots = 100
    n_spots_taken = 90
    n_trials = 10000
    
    lower_stop = round(stopping_bounds[0] * n_spots)
    upper_stop = round(stopping_bounds[1] * n_spots)

    # a number guaranteed to be greater than the greatest possible
    # walking distance
    min_avg_walking_distance = n_spots + 1 
    avg_walking_distances  = []
    best_stop = -1 # assign arbitrary value
    for stop in range(lower_stop, upper_stop):
        avg_walking_distance = find_avg_walking_distance(stop, n_spots,
                                                         n_spots_taken,
                                                         n_trials)
        avg_walking_distances.append(avg_walking_distance)
        if avg_walking_distance < min_avg_walking_distance:
            min_avg_walking_distance = avg_walking_distance
            best_stop = stop
        print(f'Tried stopping after spot {stop}, resulting in a walking '
              f'distance of {avg_walking_distance}.')

    print(f'Best stopping place was {best_stop}, with an average walking '
          f'distance of {min_avg_walking_distance}.')
    fig, ax = plt.subplots()
    ax.set_xlabel('Started looking after spot')
    ax.set_ylabel('Distance walked')
    ax.scatter(range(lower_stop, upper_stop), avg_walking_distances)
    fig.savefig('parking.png')
    fig.savefig('parking.svg')
    fig.savefig('parking.pdf')
    print('Saved file to parking.png, .svg, and .pdf')
    if not (len(sys.argv) > 1 and sys.argv[1] == '--non-interactive'):
        plt.show()
    
def find_avg_walking_distance(stop, n_spots, n_spots_taken, n_trials):
    """returns how far you would have to walk given the parameters"""
    total_walking_distance = 0
    for i in range(n_trials):
        spots = list(range(n_spots))
        random.shuffle(spots)
        taken_spots = spots[:n_spots_taken]
        spots.sort()
        look_spots = spots[:stop]
        available_look_spots = list(reversed([i for i in look_spots if i not in taken_spots]))
        # penalty for not finding a spot
        available_look_spots.append(n_spots + 1)
        total_walking_distance += available_look_spots[0]
    return total_walking_distance / n_trials

main()
