#! /usr/bin/env python3
import sys

def main():
    original   = list('abcdefghijklmnopqrstuvwxyz')
    #substitute = list('bomltzhiqjgxwdnsvackeuyfrp')
    substitute = list('omltzniqjgxwdhsvackeuyfrpb')

    encrypted_line = ''
    fname = ''
    if len(sys.argv) > 2:
        print('error: usage is "%s [filepath]"' % sys.argv[0])
        sys.exit(1)
    elif len(sys.argv) == 2:
        fname = sys.argv[1]
        f = open(fname, 'r')
    else:
        fname = 'standard-input'
        f = sys.stdin
    ## now go through the file, one character at a time
    for c in f.read():
        if c < 'a' or c > 'z':  # we only handle 'a' <= c <= 'z'
            encrypted_line = encrypted_line + c
            continue
        ## pos is the position in the original list
        pos = ord(c) - ord('a')
        ## find the new character by taking that position in the
        ## substitute list
        crypt_char = substitute[pos]
        # print(c, '->', crypt_char)
        encrypted_line = encrypted_line + crypt_char
    print('encrypted: ', encrypted_line)

main()
