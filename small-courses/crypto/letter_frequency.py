#! /usr/bin/env python3

import sys

def main():
    fname = ''
    if len(sys.argv) > 2:
        print('error: usage is "%s [filepath]"' % sys.argv[0])
        sys.exit(1)
    elif len(sys.argv) == 2:
        fname = sys.argv[1]
        f = open(fname, 'r')
    else:
        fname = 'standard-input'
        f = sys.stdin

    letter_count = [0]*26
    for c in f.read():
        if not c.isalpha(): # special cases: character is not alphabetic
            continue
        if ord(c) > 127:        # or not ascii
            continue
        ## special case: character is uppercase, so we fold it down to
        ## lowercase
        if c.isupper():
            c = c.lower()
        ## now we have a lower chase char, so we find its position in
        ## the alphabet
        pos = ord(c) - ord('a')
        ## now increase the letter_count histogram for that position
        letter_count[pos] += 1
    f.close()
    print_frequency_histogram(fname, letter_count, offset=ord('a'))

def print_frequency_histogram(title, hist, offset=0):
    print('====== frequency histogram for %s ======' % title)
    for i in range(len(hist)):  # simple ascii table+histogram
        c = chr(i+offset)
        if not (i + offset in range(ord('A'), 1+ord('Z'))
                or i + offset in range(ord('a'), 1+ord('z'))
                or i + offset in range(ord('0'), 1+ord('9'))):
            c = ' '
        print('%3d -- %s %8d --  '
              % (i, c, hist[i]), end="")
              # % (i, chr(i + ord('a')), hist[i]), end="")
        ## now print a histogram of the letter X, but scale it down so
        ## the max has some 50 Xs
        n_Xs_to_print = float(hist[i]) * 50.0 / max(hist)
        for j in range(int(n_Xs_to_print)):
            print('X', end="")
        print()

if __name__ == '__main__':
    main()
