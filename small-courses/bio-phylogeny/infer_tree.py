#! /usr/bin/env python3

from Bio import Phylo
from Bio import AlignIO
from Bio.Phylo.TreeConstruction import DistanceTreeConstructor
from Bio.Phylo.TreeConstruction import DistanceCalculator

tree_alignment = AlignIO.read('simple-animals.fa', 'fasta')
calculator = DistanceCalculator('identity')
distance_matrix = calculator.get_distance(tree_alignment)
constructor = DistanceTreeConstructor(calculator, 'upgma')
tree = constructor.upgma(distance_matrix)
Phylo.draw(tree)
