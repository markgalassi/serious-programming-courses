#! /bin/sh

echo "running: $0"
echo "args: $*"

## convert the native .svg files to .pdf for latex output
echo "%.pdf: %.svg ;	rsvg-convert -f pdf -o \$@ \$<" | make -f- Tree_of_life_SVG.pdf
echo "%.pdf: %.svg ;	rsvg-convert -f pdf -o \$@ \$<" | make -f- tree-sample-animals.pdf
