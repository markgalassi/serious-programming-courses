#! /usr/bin/env python3
import matplotlib.pyplot as plt
from io import StringIO
from Bio import Phylo
from Bio.Phylo.TreeConstruction import DistanceCalculator

t = Phylo.read(StringIO("((((Eagle,Alligator),Antelope),Sea Bass),Lamprey);" ), format="newick")
Phylo.draw(t)
Phylo.draw(t, do_show=False)
plt.savefig('simple-tree.png')
print('Saved tree to simple-tree.png')
