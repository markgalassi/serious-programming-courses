===========================================
 A workshop on programming by yourself (!)
===========================================

.. rubric:: Motivation

Here's how I announce this mini-course to the mailing list::

  This week on [...] I will teach a mini-course in which we practice
  writing programs ourselves.  Most of our mini-courses involve a
  program which I or one of the other students have written, but it's
  important to break that umbilical cord and actually carry out some
  exercises or write some programs from scratch.  So this week we will
  do that: I will propose a program to write, but you can also pick
  any program to work on, or exercise from the book.  You will work on
  it on your own, but I will occasionally give tips, either to
  individuals or to the whole class. [...]

.. rubric:: Plan

I start out by proposing a problem.  The one I have used gives some
exposure to :index:`chemistry` and an opportunity to explain some
ideas.
