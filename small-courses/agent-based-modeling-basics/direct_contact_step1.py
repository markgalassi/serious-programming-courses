#!/usr/bin/python3

from mesa import Agent, Model
from mesa.time import RandomActivation

class InfectionAgent(Agent):
    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.infected = False
    
    def step(self):
        print(f"agent {self.unique_id}; infected {self.infected}")

        
class InfectionModel(Model):
    def __init__(self, N):
        self.num_agents = N
        self.schedule = RandomActivation(self)
        
        for i in range(self.num_agents):
            a = InfectionAgent(i, self)
            self.schedule.add(a)
    def step(self):
        self.schedule.step()
