#! /usr/bin/env python3

"""Take many one-simensional random walks so that we can study the
distribution of how far you get in a random walk.  This version starts
from multiple-walks.py and adds the ability to plot a histogram of how
far we got.

"""

import random
import math
import sys

import numpy as np
import matplotlib.pyplot as plt

def main():
    n_dimensions = 2
    n_steps = 1000
    n_walks = 1000
    if len(sys.argv) == 4:
        n_dimensions = int(sys.argv[1])
        n_steps = int(sys.argv[2])
        n_walks = int(sys.argv[3])
    elif len(sys.argv) != 1:
        sys.stderr.write('error - usage: %s [n_steps n_walks]\n' % sys.argv[0])
        exit(1)
    x = [0]*n_dimensions
    print(x)
    print('#n_dimensions:', n_steps)
    print('#n_steps:', n_steps)
    print('#n_walks:', n_walks)
    distances = []            # we'll store all the distances we found
    ## now run several random walks
    for i in range(n_walks):
        final_x = take_walk(x, n_dimensions, n_steps)
        final_distance_sq = 0
        for dim in range(n_dimensions):
            final_distance_sq += final_x[dim]*final_x[dim]
        # how far did we get?
        final_distance = math.sqrt(final_distance_sq)
        distances.append(final_distance)
        # print('%d: %g  %g' % (i, final_x, distance))
    plot_distance_histogram(distances, n_dimensions, n_steps, n_walks)

def take_walk(x, n_dimensions, n_steps):
    """take a simple random walk"""
    for i in range(n_steps):
        ## generate a step of -1 or +1 in the x direction
        for dim in range(n_dimensions):
            step_x = random.randint(0, 1) * 2 - 1
            x[dim] = x[dim] + step_x
        # print(i, x, math.sqrt(i), math.fabs(math.fabs(x) - math.sqrt(i)))
    ## return the final location
    return x

def plot_distance_histogram(distances, n_dimensions, n_steps, n_walks):
    n_bins = min(51, int(2+len(distances)/2))
    # print(np.array(distances))
    n, bins, patches = plt.hist(distances, n_bins)
    plt.xlabel('final value')
    plt.ylabel('number of walks')
    plt.title('%dD random walk arrival values (%d, %d)'
              % (n_dimensions, n_steps, n_walks))
    print(bins)
    plt.grid(True)
    plt.show()

main()
