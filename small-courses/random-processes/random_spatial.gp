##REQUIRED_FILE: random_spatial.dat
##PRE_EXEC: ./random_spatial.py > random_spatial.dat
##CAPTION: Random (x, y) points.  You should be able to see
##CAPTION: some structure: occasional filaments, clustering, and empty 
##CAPTION: spaces.
set size ratio -1
set xlabel 'x'
set ylabel 'y'
plot 'random_spatial.dat' using 1:2 with points pt 7 \
    ps 0.3 title 'random (x, y) points'
