#! /usr/bin/env python3

import random

## now put the numbers in a file
f = open('time_series.out', 'w')
f.write('## time  time_interval  what_happened\n')
n_samples = 3000
t = 0
for i in range(n_samples):
    dt = random.random()
    t = t + dt
    f.write(f'{t}  {dt}  EVENT!!\n')

f.close()
print(f'wrote {n_samples} samples to file time_series.out')
