##REQUIRED_FILE: time_diffs.dat
##PRE_EXEC: ./lightning_time_distribution.py
set xlabel 'lightning number'
set ylabel '{/Symbol D} t (days)'
plot 'time_diffs.dat' using 2 with points pt 4 \
    ps 0.3 title 'time between lightning strikes'
