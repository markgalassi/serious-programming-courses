#! /bin/sh

echo "running: $0"
echo "args: $*"

## prepare the data for lightning:
echo "time_diffs.dat: lightning_time_distribution.py ;	./lightning_time_distribution.py" | make -f-
echo "time_diffs.dat.hist: ../plotting-intermediate/float_histogram_maker.py time_diffs.dat ;	../plotting-intermediate/float_histogram_maker.py time_diffs.dat 1 80" | make -f-
## for x, y spatial random:
echo "random_spatial.dat: ./random_spatial.py ;	./random_spatial.py > random_spatial.dat" | make -f-
## for histograms of x, y spatial random:
echo "random_spatial.dat.distances: ./xy_to_distances.py random_spatial.dat ;	./xy_to_distances.py random_spatial.dat" | make -f-
echo "random_spatial.dat.nearest: ./xy_to_distances.py random_spatial.dat ;	./xy_to_distances.py random_spatial.dat" | make -f-

echo "%.hist: % ;	../plotting-intermediate/float_histogram_maker.py \$< 1 80" | make -f- random_spatial.dat.distances.hist
echo "%.hist: % ;	../plotting-intermediate/float_histogram_maker.py \$< 1 80" | make -f- random_spatial.dat.nearest.hist
