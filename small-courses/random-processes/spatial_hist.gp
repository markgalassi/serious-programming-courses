##REQUIRED_FILE: random_spatial.dat
##REQUIRED_FILE: random_spatial.dat.distances
##REQUIRED_FILE: random_spatial.dat.nearest
##REQUIRED_FILE: random_spatial.dat.distances.hist
##REQUIRED_FILE: random_spatial.dat.nearest.hist
##PRE_EXEC: ./random_spatial.py > random_spatial.dat
##PRE_EXEC: ./xy_to_distances.py random_spatial.dat
##PRE_EXEC: ./float_histogram_maker.py random_spatial.dat.distances
##PRE_EXEC: ./float_histogram_maker.py random_spatial.dat.nearest
set grid
set multi layout 2,1
set style data histogram
set style fill solid 0.8 border -1
set xlabel 'distance between random (x, y) points'
set ylabel 'frequency of that distance'
plot [0:] 'random_spatial.dat.distances.hist' using 1:2 with boxes
set xlabel 'nearest distance to a point'
set ylabel 'frequency of that nearest distance'
plot [0:] 'random_spatial.dat.nearest.hist' using 1:2 with boxes
