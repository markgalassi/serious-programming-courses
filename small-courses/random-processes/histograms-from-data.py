#! /usr/bin/env python3

"""Takes a file with data columns, picks one column out and makes a
histogram of it with the desired number of bins."""

import sys
import numpy as np

def main():
    fname = sys.argv[1]
    which_col = int(sys.argv[2])
    dataset = np.fromfile(fname, sep=' ')
    print('read %s samples, max %g, min %g' 
          % (len(dataset), dataset.max(), dataset.min()))
    (hist, edges) = np.histogram(dataset, bins=150)
    print(len(hist), hist)
    print(len(edges), edges)
    z = zip(edges[:-1], hist)
    fout = fname + '.hist'
    np.savetxt(fout, np.array(list(z)), delimiter=' ')
    print('saved edges and histogram to file %s' % fout)

if __name__ == '__main__':
    main()
