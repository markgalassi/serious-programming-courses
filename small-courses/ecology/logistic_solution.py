#! /usr/bin/env python3

import math

def main():
    P0 = 30
    delta_t = 0.01
    print('# time   P_bigr   P_medr   P_smallr')
    for tstep in range(5000):
        t = delta_t * tstep
        P_0 = logistic(P0, t, 0.1, 500)
        P_1 = logistic(P0, t, 0.25, 500)
        P_2 = logistic(P0, t, 0.5, 500)
        P_3 = logistic(P0, t, 0.75, 500)
        P_4 = logistic(P0, t, 1.0, 500)
        print(f'{t:3}', '   ', P_0, '   ', P_1, '   ', P_2, '   ', P_3, '   ', P_4)


def logistic(P0, t, r, K):
    A = (K - P0) / P0
    result = K / (1 + A * math.exp(-r*t))
    return result

main()
