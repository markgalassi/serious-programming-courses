.. _chap-ecology:

=========
 Ecology
=========

[status: barely-started]


Motivation, Prerequisites, Plan
===============================

As I write this, in April of 2020, it seems like a good opportunity ot
get comfortable with some of the equations that come up when we talk
about growth.  We will look at the growth of a population, or growth
of the number of infected humans (which is related to the growth of
the population that carries the infection).

Let us start with mosquitoes and West Nile Virus in Texas.  Watch this
crash course video at:

https://www.youtube.com/watch?v=RBOsqmBQBQk&index=2&list=PL8dPuuaLjXtNdTKZkV_GiIYXpV9w4WxbX

Then have ready this nature paper on basics of ecology:

https://www.nature.com/scitable/knowledge/library/an-introduction-to-population-growth-84225544/


Factors that come up in modeling population ecology
===================================================

.. _table-factors-population-ecology:

.. table:: Factors in population ecology

   +---------------+-----------+---------------+
   |name           | variable  |               |
   +---------------+-----------+---------------+
   |initial pop:   | N         |               |
   +---------------+-----------+---------------+
   |birth rate:    | B         |               |
   +---------------+-----------+---------------+
   |death rate:    | D         |               |
   +---------------+-----------+---------------+
   |growth rate:   | r         | = (B - D) / N |
   +---------------+-----------+---------------+
   |predation      |           |               |
   +---------------+-----------+---------------+
   |immigration    |           |               |
   +---------------+-----------+---------------+
   |emigration     |           |               |
   +---------------+-----------+---------------+
   |mates          |           |               |
   +---------------+-----------+---------------+
   |food           |           |               |
   +---------------+-----------+---------------+
   |space          |           |               |
   +---------------+-----------+---------------+



Exponential growth
==================

Unlimited
---------

Spend some time plotting exponentials in gnuplot.  Show how they dwarf
linear growth, and how you need log scale to compare them.

The equation that represents population growth is:

.. math::

   \frac{dP(t)}{dt} = r P(t)

This is studied in detail in :numref:`sec-diffeq-population-growth`,
with solutions looking like

.. math::

   P(t) = P_0 e^{r x}

But if you could not solve it easily then you would write it out like
this:

.. math::

   \frac{P_{n+1} - P_n}{\Delta t} = r P_n \\
   \rightarrow \\
   P_{n+1} = P_n + r P_n \times \Delta t

This can be demonstrated with a simple program that calculates:

.. code-block:: python

   P0 = 10
   rate = 1.2
   delta_t = 1
   
   P = P0
   print('## generation population')
   for generation in range(100):
       P = P + P * (rate-1) * delta_t
       print(generation, '    ', P)


Resource-constrained - r-selection versus K-selection
-----------------------------------------------------

r-selection
   A strategy in which you reproduce as quickly as you can.
K-selection
   A strategy in which you reproduce less and put more effort into
   your children.

Simulating with the logistic equation
.....................................

The balance between these two is given by having large or small
:math:`r` or :math:`k` in the *logistic equation:*

.. math::

   \frac{dP}{dt} = r P (1 - \frac{P}{K})

This can be solved with:

.. math::

   P(t) = \frac{K}{1 + A e^{-r t}}

where

.. math::

   A = \frac{K - P_0}{P_0}

This can be visualized with a simple program:

.. literalinclude:: logistic_solution.py
   :caption: logistic_solution.py
   :language: python


.. code-block:: console

   $ python3 logistic_solution.py > rk.out
   $ gnuplot
   plot [] [0:500] 'rk.out' using 1:2 title 'r = 0.1'
   replot 'rk.out' using 1:3
   replot 'rk.out' using 1:4
   replot 'rk.out' using 1:5
   replot 'rk.out' using 1:6




Simulating with the logistic equation
.....................................


https://en.wikipedia.org/wiki/Population_growth


History of the human population on earth
========================================

Peruse the Wikipedia page on historical population estimates:

https://en.wikipedia.org/wiki/Estimates_of_historical_world_population

and the study at:

https://www.prb.org/howmanypeoplehaveeverlivedonearth/

Spend some time exploring the interactive graphs at:

https://ourworldindata.org/world-population-growth

Expand the title on "All our charts on World Population Growth", and
pick the population by country since 1500 and try to understand what
areas are exponential.

Then look at the link "World population since 10,000 BCE
(OurWorldInData series)".

Download the data for this graph and zoom in on some specific
periods.  Look at both linear and logarithmic scales.

Following the indications shown in

https://www.nature.com/scitable/knowledge/library/an-introduction-to-population-growth-84225544/

we can look at the table below and seek certain interesting periods in
the data.

.. _table-human-population-history:

.. table:: Periods of interest in human population history

   ==========  ========  =================================
   Start       End       What to look for
   ==========  ========  =================================
   -10000      -4000     Agricultural revolution
    -4000       -600     Early empires
    -1000        300     Alexander and Rome
        1        300     Imperial Rome
        1       1600     Largely steady world population
     1200       1400     Medieval black death
     1500       present  Modern world
     1850       present  Industrial revolution
     1900       present  Large scale science
   ==========  ========  =================================


The logistic function
=====================

Although the earth's population as a whole appears to still be in an
exponential growth phase, the Pew Research Center predicts that it
will flatten by the end of the 21st century:

https://www.pewresearch.org/fact-tank/2019/06/17/worlds-population-is-projected-to-nearly-stop-growing-by-the-end-of-the-century/

This type of function is not exponential growth anymore: it shows
exponential growth, but that then slows down and we end up with what
is called the Logistic Function:

https://en.wikipedia.org/wiki/Logistic_function

Think of fidget spinners.

Let's start getting to work on the equations that represent population growth


The Lotka-Volterra differential equations
=========================================


Further reading
===============

* https://www.youtube.com/watch?v=NYq2078_xqc - Khan Academy video
  with pleasant intro to cycles and real examples, 5min.

* https://www.youtube.com/watch?v=mFDiiSqGB7M - crash course on
  predator-prey ecology

* https://en.wikipedia.org/wiki/Lotka%E2%80%93Volterra_equations#A_simple_example

* http://www.scholarpedia.org/article/Predator-prey_model

* http://mc-stan.org/users/documentation/case-studies/lotka-volterra-predator-prey.html

Still have to look at articles on 3-way predator-prey:

* http://emis.ams.org/journals/HOA/JAMDS/3/2155.pdf

* chrome-extension://oemmndcbldboiebfnladdacbdfmadadm/https://www.cs.unm.edu/~forrest/classes/cs365/lectures/Lotka-Volterra.pdf

* http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.360.1552&rep=rep1&type=pdf

* http://disi.unal.edu.co/~gjhernandezp/sim/lectures/DeterministicModelsAndChaos/PopulationModels/LotkaVolterra3species.pdf


And how about simple models of a full society collapse?

* https://en.wikipedia.org/wiki/Collapse:_How_Societies_Choose_to_Fail_or_Succeed

* http://necsi.edu/projects/evolution/co-evolution/pred-prey/co-evolution_predator.html

* https://www.ted.com/talks/jared_diamond_on_why_societies_collapse

* https://arxiv.org/abs/1002.0068

* https://faustusnotes.wordpress.com/2014/05/15/mathematical-modeling-of-civilization-collapse/

Cliometrics

* https://en.wikipedia.org/wiki/Cliometrics

