.. -*- mode: rst -*-

.. _chap-emergent-behavior:

=================
Emergent behavior
=================

[status: content-mostly-written]

Motivation, prerequisites, plan
===============================

Purpose: a fun exploration of how complex large-scale behavior emerges
from very simple rules on the small scale.

Prerequisites:

* The 10-hour "serious programming" course.


Before you start
================

* Watch some of the videos from Melanie Mitchell's course
  "Introduction to Complexity".  At this time only watch the video for
  Section 1.1 (overall introduction to complexity) and all the videos
  for Section 6.3 (introduction to 1-dimensional cellular automata):
  https://www.youtube.com/playlist?list=PLF0b3ThojznRyDQlitfUTzXEXwLNNE-mI


* Read the Nova slides on Everyday Examples of Emergence (4min)
  http://www.pbs.org/wgbh/nova/nature/emergence-examples.html

* Watch the video "How do schools of fish swim in harmony?" (6min)
  https://www.youtube.com/watch?v=dkP8NUwB2io


.. _sec-write-the-simple-ca-py:

Write the simple_ca.py program which implements a cellular automaton
====================================================================

You can download the :download:`simple_ca.py` program and we can read
through it to see how it works.


Conway's game of life
=====================

You can download the :download:`conway_life.py` program and we can
read through it to see how it works.


Install and run the golly program
=================================

Command line: type "golly", it will tell you how to install it.
Experiment with drawing your own initial cells and use TAB to advance
one at a time, or the run button to advance more.

Then explore the library of starting positions, specifically:

Life -> Guns -> 2c5spaceship-gun-p416

Life -> Guns -> golly-ticker


Further study
=============

Play with the simple_ca.py program
----------------------------------

Make modifications to simple_ca.py to make it

* Run longer runs so you can see more of the patterns.

* Have more cells in each row -- make it as wide as your terminal will
  go.

* You will find the following lines of python code in simple_ca.py:

.. code-block:: python

        new_row[i] = new_cell(neighbors)
        ## NOTE: new_cell_with_rule() is part of the extended code (at
        ## the bottom)
        # new_row[i] = new_cell_with_rule(neighbors)

..

  comment out the line that sets new_row[i] to be the result of
  new_cell(), and uncomment the line that sets it to be
  new_cell_with_rule().  Then look at the code in new_cell_with_rule()
  and play with setting different rules and seeing what the cellular
  automata look like.

Further reading
===============

* Godel, Escher, Bach

  Read the dialogues "Prelude ... Ant Fugue".  All the dialogues in
  the book might be quite interesting to you.


* Watch the rest of the online course on Complexity

  In the prerequisites I had you watch some sections of Melanie
  Michtell's online course on Complexity.  You could watch the rest of
  the series.  At a very minimum watch the sections on genetic
  algorithms and the game of life.

  https://www.youtube.com/playlist?list=PLF0b3ThojznRyDQlitfUTzXEXwLNNE-mI
