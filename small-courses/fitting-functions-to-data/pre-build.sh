#! /bin/sh

echo "running: $0"
echo "args: $*"

## prepare the data
wget --continue https://raw.githubusercontent.com/rmcelreath/rethinking/master/data/Howell1.csv

## make pdf files out of some svg files - this allows us to "make latexpdf"
#rsvg-convert -f pdf -o Linear_least_squares_example2.pdf Linear_least_squares_example2.svg
echo "%.pdf: %.svg ;	rsvg-convert -f pdf -o \$@ \$<" | make -f- Linear_least_squares_example2.pdf

echo "%.svg: fit-battery.py %.dat ;	python3 \$^ \$@" | make -f- battery-discharge.svg
echo "%.pdf: fit-battery.py %.dat ;	python3 \$^ \$@" | make -f- battery-discharge.pdf

echo "%_deg2.pdf: fit-falling-body.py %.csv ;       python3 \$^ 2" | make -f- riccioli-table_deg2.pdf
echo "%_deg13.pdf: fit-falling-body.py %.csv ;       python3 \$^ 13" | make -f- riccioli-table_deg13.pdf
# echo "%_deg2.svg: fit-falling-body.py %.csv ;	python3 \$^ \$@ 2" | make -f- riccioli-table_deg2.svg
# echo "%.pdf: fit-falling-body.py %.csv ;	python3 \$^ \$@ 13" | make -f- riccioli-table_deg2.pdf
