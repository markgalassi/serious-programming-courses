#! /usr/bin/env python3

import math
import sys
import numpy as np
from scipy import optimize
import matplotlib.pyplot as plt

def main():
    if not len(sys.argv) in (2, 3):
        print('error: please give a battery data file argument')
        sys.exit(1)
    infile = sys.argv[1]
    outfile = None
    if len(sys.argv) == 3:
        outfile = sys.argv[2]

    times, OCVs = load_battery_file(infile)
    print('times:', times)
    print('open current voltages:', OCVs)
    p0=[3.5, -0.0334, -0.106, 0.7399, 1.403, 2]
    # p0=[2, 2, 2, 2, 2, 2]
    # SOCs = np.flip(times, 0) / (times[-1] - times[0])
    SOCs = times / (times[-1] - times[0])
    print('SOCs:', SOCs)
    params, params_covariance = optimize.curve_fit(Vocv, SOCs, OCVs, p0=p0)
    print('curve fit parameters:', params)
    print('a = %g' % params[0])
    print('b = %g' % params[1])
    print('c = %g' % params[2])
    print('d = %g' % params[3])
    print('m = %g' % params[4])
    print('n = %g' % params[5])
    ## now visualize the results
    plt.figure(figsize=(6, 4))
    ## plot a scatter plot of the original data
    plt.scatter(SOCs, OCVs, label='Data')
    ## plot the function fit - use a denser range of times for this,
    ## since our time data is sparse
    # trange = np.arange(times[0], times[-1], 0.1)
    # SOCrange = np.arange(SOCs[0], SOCs[-1], 0.1)
    SOCrange = np.arange(0.0, 1.01, 0.005)
    plt.plot(SOCrange, Vocv(SOCrange, params[0], params[1], params[2],
                          params[3], params[4], params[5]),
             label='Fitted function')
    plt.xlabel('1 - state of charge (1 - SOC)')
    plt.ylabel('open circuit voltage (Vocv)')
    plt.legend(loc='best')
    if outfile:
        plt.savefig(outfile)
    else:
        plt.show()

def Vocv(SOC, a, b, c, d, m, n):
    # SOC = (t[-1] - t + 1) / (t[-1] - t[0])
    # print(a, b, c, d, m, n)
    # return a + b*(-np.log(SOC))**m + c*SOC + d*np.exp(n*(SOC-1))
    return a + b*SOC**(-m) + c*SOC + d*np.exp(n*(SOC-1))

def load_battery_file(fname):
    times, OCVs = np.loadtxt(fname, usecols=(0, 2), unpack=True)
    return times, OCVs

main()
