.. _chap-fitting-functions-to-data:

===========================
 Fitting functions to data
===========================

[status: content_complete_needs_review]


Motivation, Prerequisites, Plan
===============================

In :numref:`chap-a-tour-of-functions` we looked at how to take a
function and represent it as points and lines in a plot.  Here we do
the opposite: if we are given a collection of :math:`(x, y)` points we
try to find *what kind of function might have generated those points*.

There are so many types of functions that there is some artistry
involved in picking which kind of function to fit to a set of data.

In many cases we will want to fit a straight line to our data.
Sometimes it will be a polynomial.  Our intuition on what functions to
try to fit can come from two sources: (a) looking at the data visually
("hey! that looks like a parabola!"), and (b) having some intuition
about the *process* that underlies that data ("should be an
exponential decay; let's try fitting an exponential function!")

Terminology: the techniques we use here are sometimes referred to as
regression, linear regression, fitting, curve fitting, line fitting,
least squares fitting, ...


Examples to get started
=======================

Let us start by visualizing some data sets.  We'll start with the
dataset we had for age, height and weight of the !Kung people of the
Kalahari desert.  We downloaded and analyzed this data set back in
:numref:`sec-intermediate-plotting-a-worked-example`, but there we
were looking at height and weight historgrams.  Here we look at
something simpler: height versus age.

In this example we will grab the data set, 

.. code-block:: console

   $ wget https://raw.githubusercontent.com/rmcelreath/rethinking/master/data/Howell1.csv
   $ gnuplot
   gnuplot> set datafile separator ";"
   gnuplot> plot 'Howell1.csv' using 3:1 with points

You can carry it all out with the following instructions:


.. _listing-plot-height-gp:

.. literalinclude:: plot-height.gp
   :language: gnuplot
   :caption: Instructions to plot the height vs. age for the !Kung.


and you should get the figure in :numref:`fig-height`.


.. _fig-height:

.. figure:: plot-height.*
   :width: 70%

   Height vs. age for the !Kung people of the Kalahari desert.  Note
   that we have two separate behaviors of the height data: one for
   before the age of 18 (rapid growth), the other for after the age of
   20 (mostly norizontal).


We see this drastic difference between lower and higher age.  One
thing that comes to mind is that the mechanisms that cause this are
obvious biological ones: we grow fast when we are young, then we stop
growing taller.

The rising height part of the graph (up to age 18) seems to be
reasonably close to a straight line, as is the top part (from 20 years
on).  The entire plot does not at all look like a straight line.

Let us look at those separate areas in separate plots:

.. literalinclude:: plot-height-2-18.gp
   :language: gnuplot
   :caption: Zooming in on the straight line between ages 2 and 18.

and you should get the figure in :numref:`fig-height-2-18`.

.. _fig-height-2-18:

.. figure:: plot-height-2-18.*
   :width: 70%

   Zooming in on the age 2-18 region of the height plot.


Straight line fits
==================

Our goal
--------

Let us state a bold goal: can we find a straight line that seems to
fit the points in the rising area of the height plot of the !Kung
people (:numref:`fig-height-2-18`)?

Remember from :numref:`sec-linear-functions` that a straight line
looks like :math:`y = m x + b` is described by two parameters: the
*slope* :math:`m` and the *y intercept* :math:`b`, so another way to
phrase this goal is:

   Given a collection of points :math:`(x_i, y_i)`, find the slope and
   intercept for the line :math:`y = m x + b` which most closely fits
   the points.

You could do this visually: print it, take a ruler, place it so that
it runs through the points in the plot, and draw a line.  The result
is not really optimal, so we look for better techniques.

.. _sec-stepping-back-just-two-points:

Stepping back: just two points
------------------------------

Let us start with just two points, one for a 3-year-old and the other
for a 17-year-old.  From the data file we can pick the (age, height)
pairs: (3, 96.52) and (17, 142.875).

In middle school math we learn how to find the line that goes through
these points.  We write out the equation :math:`y = m x + b` using the
specific :math:`(x, y)`:

.. math::
   :label: line-fit-2-points

   96.52 & = m \times 3 + b \\
   142.875 & = m \times 17 + b

By subtracting the equations we get:

.. math::

   142.875 & - 96.52 = m \times 17 - m \times 3 = m \times 14 \\
   & \implies
   m = (142.875 - 96.52) / 14 = 3.311

and substituting back into the first equation we get:

.. math::

   96.52 & = 3.311 \times 3 + b \\
   & \implies
   b = 96.52 - 3 \times 3.3111 = 86.5867

So the equation for our line is:

.. math::

   m & = 3.311 \\
   b & = 86.5867 \\
   y & = 3.311 \times x + 86.5867

.. note::

   This is just a first stab at it!  We do not know if we chose those
   two points well -- in fact it seems from this picture that the
   17-year-old we picked was shorter than average, which would skew
   the results.  The 3-year-old is also taller than average.  This
   example was just to get going so that we can talk about line fits.
   In :numref:`sec-proper-line-fitting` we will do proper line
   fitting.


Let's plot that line with our data
----------------------------------

Now that we have a line fit we should feel really excited about
plotting it together with our data, so that we can get some visual
satisfaction.  I always encourage people to do this right away.

Let us take the plotting instructions in
:numref:`listing-plot-height-gp` and add to it the plotting of our
fitted line:

.. _listing-plot-height-with-fit-gp:

.. literalinclude:: plot-height-with-fit.gp
   :language: gnuplot
   :caption: Height vs. age and a line fit for ages 2-18.

and you should get :numref:`fig-height-with-fit`.

.. _fig-height-with-fit:

.. figure:: plot-height-with-fit.*
   :width: 80%

   Height vs. age for the !Kung people of the Kalahari desert, with a
   line fit for ages 2-18.


Physical interpretation of the line fit
---------------------------------------

One realy cool thing we learn about fitting functions to data is that
those functions have a *physical interpretation*!  This gets
scientists really excited, so I will mention it before we move on to a
better procedure for fitting curves.

If you look closely at the *units of measure* in our scatter plot you
see that we are plotting *height* (a measure of length) versus *age*
(a measure of time).  This means that the slope :math:`m` is measured
in units of length divided by time, in this case in centimeters per
year.  So the slope we found of 3.311 should really be reported as
3.311 centimeters/year, and it tells us how much children grow per
year between the ages of 2 and 18.

Now let us look at the intercept :math:`b` (in our case 86.5867).
This number tels us what the value of y (height) is at time zero
(birth).  This would seem to imply that babies are born some 87cm
tall.

.. important::

   But wait!  Babies are not born 87cm tall.  They are born around
   50cm tall.  Then why did our straight line fit report that height
   at birth??  This is explained by remembering the original plot: in
   :numref:`fig-height` we see that the height forms a straight line
   between the ages of 2 and 18, but *not* for children less than 2.
   Those children grow much faster, so the straight line is not a
   valid approximation for infants.  This means that, sadly, the
   intercept :math:`b` does not give a gratifying physical
   interpretation in this case.


.. _sec-proper-line-fitting:

Proper line fitting
===================

We've seen how to pick two points from our data set and make a line
that goes through them, but this approach has some real problems: if I
had chosen a very tall 3-year-old and a very short 17-year-old, then
the slope would have been much smaller.  I could have also skewed it
the other way by picking a very short 3-year-old and a very tall
17-year-old, which would have given a much steeper slope.  [FIXME:
write exercise to do all of this]

So what is a good objective way of finding the "best fit"?  This is
much debated, and one get get quite subtle and make a real profession
of the line fitting business, but for our purposes we will use the
most comon approach.  It is called "least squares fitting".

Look at :numref:`fig-linear-least-squares-example`:

.. _fig-linear-least-squares-example:

.. figure:: Linear_least_squares_example2.*
   :width: 60%

   From wikipedia,
   https://en.wikipedia.org/wiki/Linear_least_squares_(mathematics) A
   plot of the data points (in red), the least squares line of best
   fit (in blue), and the residuals (in green).

You see four points and an example of a line that *tries* to go
through those four points.  How well did it succeed?

Look at those green lines in the figure: they show how far the point
is from the line, and they give a measure of the error in your fit.
If you had chosen a radically different blue line, then the sum of all
those green lines might be quite big, and you would have a poor fit.

The idea behind "least squares" is that you want to *minimize* the sum
of the *squares* of those errors.

There is a lot going on here.  You might immediately ask at least two
questions: (a) why the sum of the *squares* of the errors?  (b) how do
you minimize?

For now I will give a very brief answer to the first question: squares
are often considered "nicer" functions than just taking the distance
itself.  You can see this with this plot:

.. code-block:: console

   $ gnuplot
   gnuplot> plot [-3:3] x*x
   gnuplot> replot abs(x)

both the lines tend to grow bigger when your error is bigger, but the
smooth one is more suited to various mathematical techniques.

As for the second question: we will minimize the sum of the squares of
the errors using techniques from calculus.  I don't describe them
here, but I will show a cool pair of equations.  First wrap your head
around this expression:

.. math::
   E = \sum_{i=1}^N (m x_i + b - y_i)^2
   :label: least-squares-error

I like formulae like this one because that capital greek sigma letter
is visually appealing, but what does it all mean?

First note that in math we use the symbol :math:`\sum_{i=1}^N` to mean
"take a sum of all these terms, where the letter i will be replaced in
turn by 1, 2, 3, ... up to N.

Now let's say you have N data points :math:`(x_1, y_1), (x_2, y_2),
..., (x_N, y_N)`.  Then the E in equation :eq:`least-squares-error` is
the sum of a bunch of terms, each of which looks like :math:`(m x_i +
b - y_i)^2`.  That is difference between "what the fitted line
would have given you" and "the real data point that you have".
Squared.  These differences are often called the "residuals".

So E is a pretty good measure of how poor your line fit is, so if you
find the values of m and b that *make E as small as possible* then
they might give you a nice straight line fit through your data.

To show you some more cool math typesetting, and to entice you to
study calculus, I will show you the equations that are written to
solve this problem:

.. math::
   E & = \sum_{i=1}^N (m x_i + b - y_i)^2 \\
   \frac{\partial E}{\partial m} & = 0 \\
   \frac{\partial E}{\partial b} & = 0
   :label: least-squares-equations

This gives you two equations which you can solve to get m and b.  We
will not go through the details of it since it is calculus material
("finding minima using derivatives"), but we will now learn to use a
Python library that does this calculation for us.

.. note::

   There are many pleasing aspects to learning about least squares
   fitting.  One of them is that this is one of the earliest places
   where you run in to ... FIXME


Using Python's scientific libraries to fit lines
================================================

Python comes with an extensive scientific library called *scipy*.
Scipy has a statistics subpackage, which in turn has a function called
``linregress()`` which does all that work for us.

Enter the program in 

.. _listing-fit-height-py:

.. literalinclude:: fit-height.py
   :language: python
   :caption: fit-height.py -- fit a line through the height data and
             print out the slope and intercept.

Run the program with:

.. code-block:: console

   $ ./fit-height.py  Howell1.csv 2 18
   the least squares fit returns slope, intercept:
   m = 3.99026836995 , b = 79.6805438775

Now compare these values to those found in in
:numref:`sec-stepping-back-just-two-points` where we just picked two
points to work with.  It turns out that 3.99 is not too far from 4.12,
and 79.68 is not far from 79.06, but they are different.  Let us
update :numref:`fig-height-with-fit` to also show the line found by
least squares linear regression:

.. _listing-plot-height-with-two-fits-gp:

.. literalinclude:: plot-height-with-two-fits.gp
   :language: gnuplot
   :caption: Height vs. age, a naive line fit and a least squares fit
             for ages 2-18.

and you should get :numref:`fig-height-with-two-fits`.

.. _fig-height-with-two-fits:

.. figure:: plot-height-with-two-fits.*
   :width: 80%

   Height vs. age for the !Kung people of the Kalahari desert, with a
   naive line fit for ages 2-18, and a least squares fit as well.
   Note the difference between the two lines - the steeper line from
   the least squares fit is a better fit for the age 2-18 range.

This line fit from the least squares method was a much better fit to
the 2-18 year old data, so we can now say that the !Kung youth grow
approximately 3.99 centimeters/year.


When to not try a linear fit
============================

We should only try linear fits if we have reason to believe that the
data is linear (either visually or from the scientific mechanism that
underlies the data).

In the case of our height plot, what would happen if we were to try to
fit the *entire* plot, from age 0 to the maximum age?

Let us try and see what it looks like.  We run our program with ages 0
and 90:

.. code-block:: console

   $ ./fit-height.py  Howell1.csv 0 90
   the least squares fit returns slope, intercept:
   m = 0.909605221912 , b = 111.571782869

What would this line look like?  Plotting like this:

.. _listing-plot-height-fit-inappropriate-gp:

.. literalinclude:: plot-height-with-inappropriate-fit.gp
   :language: gnuplot
   :caption: Gnuplot instructions to plot an inappropriate line fit.

and you should get the plot in
:numref:`fig-plot-height-with-inappropriate-fit`.

.. _fig-plot-height-with-inappropriate-fit:

.. figure:: plot-height-with-inappropriate-fit.*
   :width: 70%

   Height vs. age for the !Kung people of the Kalahari desert.  We
   also plot a fit based on the entire plot, and we see that it is a
   poor fit to the data, since the data is not linear.

We see that the mechanisms that determine height are quite different
for infants, youths and older people, so there is no single line fit.


Fitting curves
==============

.. _sec-polynomial-fits:

Polynomial fits
---------------

Let us look at a falling body.  In fact let us look at the first
careful measurement that was ever made of a falling body.  In the
mid-17th-century Italian astronomer Riccioli
:cite:`graney2012doubting` carried out careful experiments from a
tower in Bologna and got the data in this table:

.. csv-table:: Riccioli's acceleration data
   :file: riccioli-table.csv
   :widths: 30, 70
   :header-rows: 1

You can download a CSV format file for this data:
:download:`riccioli-table.csv`

Newton's law with a constant acceleration of gravity :math:`-g` tells
us that the position reached by a falling body from height :math:`h_0`
(280 meters), with initial velocity :math:`v_0 = 0` is:

.. math::
   y(t) = h_0 + v_0 t - \frac{1}{2} g t^2 = h_0 - \frac{1}{2} g t^2
   :label: eq-falling-body

When we calculate the coefficients for :eq:`eq-falling-body` we expect
the first degree coefficient (initial velocity :math:`v_0`) to be
close to zero since we are letting it drop rather than tossing it.  We
also expect the constant term (initial height :math:`h_0`) to be close
to 280 roman feet, the height from which we drop it.

So we are interested in using the data from Riccioli's
mid-17th-century experiment that we put in file
``riccioli-table.csv``:

.. literalinclude:: fit-falling-body.py
   :language: python
   :caption: fit-falling-body.py - fit a 2\ :sup:`nd` degree
             polynomial to the data, then plot the data and the fit.

You can run the program with:

::

   chmod +x fit-falling-body.py
   ./fit-falling-body.py riccioli-table.csv

It will find that the polynomial coefficients :math:`(a, b, c)` are
``-14.6918224, 0.62669318, 280.40412405`` and the polynomial fit looks
like:

.. math::
   y(t) = y = -14.69182224 \times t^2 - 0.62669318 t + 280.40412405
   :label: eq-riccioli-fit

The program saves the output in files ``riccioli-table_deg2.pdf`` and
``riccioli-table_deg2.svg``, and we show it in
:numref:`fig-riccioli-table`.

.. _fig-riccioli-table:

.. figure:: riccioli-table_deg2.*
   :width: 60%

   Position as a function of time for a falling body.  The data is
   taken from Riccioli's classic experiment in the mid 17th century.
   The line is a 2\ :sup:`nd` degree polynomial fit of the data.

The coefficients for the polynomial in equation :eq:`eq-riccioli-fit`
and :numref:`fig-riccioli-table` teach us that:

* The 2\ :sup:`nd` degree polynomial is a good fit to the data.

* The coefficients in the falling body equation :eq:`eq-falling-body`
  have physical meanings.  The constant term :math:`h_0` is the height
  from which we drop the body, :math:`v_0` is the initial velocity,
  and in the :math:`\frac{1}{2} g t^2` term, g is the acceleration of
  gravity.

* In our fit we have a very small value for :math:`v_0` (close to 0),
  which matches the physical situation.

* In our fit we have :math:`h_0` very close to 280 roman feet, which
  matches the physical situation.

* In our fit we have :math:`g` close to :math:`29.38 \; {\rm roman \;
  feet}/s^2`.  In modern units that gives us :math:`g = 8.84 \;
  m/s^2`, which is about 10% off of the current measured value of
  :math:`9.81 \; m/s^2`.

https://physicstoday.scitation.org/doi/pdf/10.1063/PT.3.1716

https://physicstoday.scitation.org/doi/full/10.1063/PT.3.1716


Overfitting
-----------

We all know that if you have two (distinct) points you can fit a
*unique* straight line through them.  If you write the equation for
that straight line as :math:`y = m x + b` you might find yourself
thinking "hmm, there were two points that constrained that line, and
there are two parameters :math:`(m, b)` (slope and intercept).

So two points gave unique values to two parameters.  Is that "2" a
coincidence?

It turns out that if you have three points in a plane, and they are
not on a line together, then you can fit a *unique* parabola (2\
:sup:`nd` degree polynomial) through them.  If that parabola looks
like :math:`y = a x^2 + b ^x + c`, then you will notice that there are
*three* parameters :math:`(a, b, c)` being fixed by the *three* points
that the curve must visit.

You might ask if you have 7 points, is there exactly one 6th degree
polynomial that fits through all 7 points?  The answer is yes, unless
those 7 points are somewhat contrived.  For example, if they all lie
perfectly on a straight line, or on a parabola, or other lower order
polynomial, then you will have *multiple* possible 6th degree
polynomials that go through those pointes.

So you might ask the question:

.. note::

   Do I get better fidelity to the data if I fit a very high degree
   polynomial to the data?  For example, if I take the Riccioli data
   set, and fit a 9\ :sup:`th` degree polynomial, will that capture
   some deep nuance in the data?

The answer is a clear *no*, and we will demonstrate that by trying it
out and visualizing the results.  After visualizing, we will try to
gain some insight.

Start by modifying the program ``fit-falling-body.py`` to remove some
of the data.  It has 14 data points, so we could fit up to a 13\
:sup:`th` degree polynomial.  We will do a few experiments by calling
the program with arguments different from two.  Start with:

::

   ./fit-falling-body.py riccioli-table.csv 13

This should give you a plot like the one shown here:

.. image:: riccioli-table_deg2.*
   :width: 49%
.. image:: riccioli-table_deg13.*
   :width: 49%

where you can clearly see that the 2nd degree polynomial makes more
sense than the 13th degree polynomial.

So what just happened?  The 13th degree polynomial is a *perfect* fit:
it passes exactly through every point in the data set, so why is it so
clearly bogus?

This is a good topic to discuss in class, and it leads to discussions
of "overfitting".


Fitting arbitrary functions
---------------------------

Curve fitting is described here:

https://www.scipy-lectures.org/intro/scipy/auto_examples/plot_curve_fit.html

The equation we fit comes from Zhang, Jiang, Zhang, Liu, Wang and Loh
"A Generalized SOC-OCV Model for Lithium-Ion Batteries and the SOC
estimation for LNMCO Battery".  :cite:`zhang2016generalized`

.. math::

   V_{\rm OCV}(s) = a + b (-\log_e s)^m + c s + d e^{n (s-1)}

where :math:`s` is the "state of chage" (SOC) - how much charge the
battery still has.

I found that with the data sets I had I needed to adapt it slightly
and used:

.. math::

   V_{\rm OCV} = a + b s^{-m} + c s + d e^{n (s-1)}

Each term in this equation corresponds to a different chemical effect
in the battery's journey.

.. code-block:: text
   :caption: Battery discharge data: closed and open circuit voltage
             (volts) versus time (hours) for the discharge of two
             consumer-grade AA batteries in series.  Source: Abby
             Wilson, private communication.

   #time closed open
   0     2.48   2.74
   1     2.33   2.59
   2     2.29   2.55
   3     2.28   2.54
   3.5   2.27   2.52
   4.5   2.25   2.50
   5.5   2.23   2.48
   6     2.21   2.46
   8     2.01   2.28
   8.5   1.84   2.10
   8.75  0.80   1.90

Run this program with:

.. code-block:: console

   $ ./fit-battery.py battery-discharge.dat

to get interactive graphical output, or to get the plot in a file you
can use something like:

.. code-block:: console

   $ ./fit-battery.py battery-discharge.dat battery-discharge.pdf

The resulting plot should look like that in
:numref:`fig-battery-discharge`.

.. _fig-battery-discharge:

.. figure:: battery-discharge.*
   :width: 50%

   A curve fit for the battery discharge data.

The parameters found were:

::

   a = 2.53651
   b = 0.0512756
   c = -0.182952
   d = -0.499311
   m = 0.151882
   n = 15.5123

So the fitted function looks like:

.. math::

   V_{\rm OCV}(s) = 2.54 + 0.051 s^{-0.152} - 0.183 s - 0.499  e^{15.5 (s-1)}

There are many assumptions I made here.  For example, I assumed that
(1 - SOC) is linearly proportional to time as you discharge.  That's
not quite correct.  But this is still an interesting example of how
you can take a rather complicated function with various parameters (6
in this case) and fit it to a set of data.


Topics for further study
========================

Interpolation and extrapolation
-------------------------------

What do interpolation and extrapolation mean?  Show an example where
it works well, like with falling bodies.

Discuss what is a *predictive* model.

How high should the degree of the polynomial be?
------------------------------------------------

Discuss overfitting and crazy polynomial behaviors.

Discuss "fitting unerlying truth" versus "fitting noise".

Discuss a physical reason for having a certain degree of polynomial
(or a certain functional form), and what happens when you don't have
that physical reason.

Discuss objective criteria for limiting the number of dimensions to
avoid crazy polynomial behaviors.

Mention approaches that use functions other than polynomials, like
Fourier analysis.
