import numpy as np
from matplotlib import pyplot as plt

def main():
    grass = 0.1
    deer = 0.8
    wolves = 0.3 ## initial conditions
    grass_pops = [grass]
    deer_pops = [deer]
    wolf_pops = [wolves]
    step_count = 1500
    step_size = 0.01
##               g     d     w
    rate_arr = [[-0.5, 0.3,  0],    # g
		[-1,   0.3,  0.7],  # d
		[0,    -1,   -0.5], # w
		[3,    -0.1, -0.5]] # base rate
    for _ in range(step_count):
        grass, deer, wolves = iterate([grass, deer, wolves], step_size, rate_arr)
        if grass < 0.001:
            grass = 0
        if wolves < 0.001:
            wolves = 0
        if deer < 0.001:
            deer = 0   
        grass_pops.append(grass)
        deer_pops.append(deer)
        wolf_pops.append(wolves)
    plt.plot(np.arange(step_count + 1) * step_size, grass_pops)
    plt.plot(np.arange(step_count + 1) * step_size, deer_pops)
    plt.plot(np.arange(step_count + 1) * step_size, wolf_pops)
    plt.show()
		    
def iterate(pops, step_size, rate_arr):
    """step some parameters forward"""
    rate_arr = np.array(rate_arr).T.copy()
    d_pops = np.zeros_like(pops)
    for i in range(len(pops)):
        d_pops[i] += rate_arr[i][-1] * pops[i]
        for j in range(len(rate_arr[i]) - 1):
            d_pops[i] += rate_arr[i][j] * pops[i] * pops[j]
    pops += d_pops * step_size
    return pops

main()
