from matplotlib import pyplot as plt
import numpy as np
		
def main():
    wolves = 1
    deer = 1 ## initial conditions
    wolf_pops = [wolves]
    deer_pops = [deer]
    step_count = 1500
    step_size = 0.01
    for _ in range(step_count):
        wolves, deer = iterate(wolves, deer, step_size)
        if wolves < 0.001:
            wolves = 0
        if deer < 0.001:
            deer = 0
        wolf_pops.append(wolves)
        deer_pops.append(deer)
    plt.plot(np.arange(step_count + 1) * step_size, deer_pops)
    plt.plot(np.arange(step_count + 1) * step_size, wolf_pops)
    plt.show()
		    
def iterate(x, y, step_size):
    """step some parameters forward"""
    alpha = 1.1
    beta = -1
    gamma = -0.4
    delta = 0.5
    x += (alpha * x + beta * x * y) * step_size 
    y += (gamma * y + delta * x * y) * step_size
    return x, y

main()
