:tocdepth: 2

.. _app-proposed-chapters:

=============================
 Appendix: Proposed chapters
=============================

.. rubric:: Motivation

This appendix is a grabbag of possible chapters -- ideas for topics
that are interesting, especially to middle and high school students,
and which are waiting for someone to write a chapter about them.

It is structured as a simple list of sections with a chapter topic in
each.  Each section has some references on materials.

Once a co-author wants to write a chapter on that subject, she should
transfer that material into a chapter, flesh out the chapter (see
:numref:`app-how-to-add-a-chapter`), and remove it from this appendix.

.. _sec-a-tour-of-datasets:

A tour of datasets
===================

[status: unwritten]

* https://www.dataquest.io/blog/free-datasets-for-projects/


Climate change
--------------

* NOAA weather stations

538 blog data
-------------

the 538 github area


.. _sec-molecules-in-three-dimensions:

Molecules in three dimensions
=============================

First install paraview, that very ambitious volume rendering tool.
For molecules we can also install avogadro, jmol, and gdis.  And
others:

.. code-block:: bash

   sudo apt install paraview avogadro jmol gdis
   sudo apt install garlic dssp garlic qutemol viewmol pymol openbabel-gui rasmol

The ones that seem to work are paraview, avogadro, jmol, rasmol: you
can just run them on a .pdb molecular description file.

You can follow this tutorial for paraview:

https://www.paraview.org/Wiki/The_ParaView_Tutorial

The others probably also have tutorials, but they are less immediately
daunting than paraview.


Small Molecules
---------------

Start with small molecules, for example from here:

https://ww2.chemistry.gatech.edu/~lw26/structure/small_molecules/index.html

But also at http://www.rcsb.org/pdb/ligand/chemAdvSearch.do you can do
a search for smaller molecules.  For example I found water with a
"search for ligands" in the rcsb.org web site.  That gives me:

http://www.rcsb.org/pdb/results/results.do?tabtoshow=Ligand&qrid=9C66910A

which gives, down below, the H2O which is "water" rather than
"deuterated water".  This lets you download it as:

::

   wget http://files.rcsb.org/ligands/view/HOH.cif

From that I guessed that I could also get things like Methane and
Sulphur atom:

::

   wget http://files.rcsb.org/ligands/view/CH4.cif
   wget http://files.rcsb.org/ligands/view/S.cif

You can also browse like this: go to
http://files.rcsb.org/ligands/ and start navigating, for example into
C and then C2H.  From there you can find:

::

   wget http://files.rcsb.org/ligands/view/C2H_ideal.pdb

Here are a few more:

::

   wget http://files.rcsb.org/ligands/C/C6H/C6H_ideal.pdb
   wget http://files.rcsb.org/ligands/C/CH4/CH4_ideal.pdb



Then in the search menu you find "Chemical components" and in there
"Chemical name"

Now it looks like the way to find molecules by colloquial names is to
go to http://ligand-expo.rcsb.org/ld-search.html and do a "molecular
name" search, with "molecular name (exact)".

Try putting "Water" and you get this result:

http://ligand-expo.rcsb.org/pyapps/ldHandler.py?formid=cc-index-search&target=Water&operation=name-exact

pick the HOH from there, go to its "Chemical details" and download the
"PDB format (ideal coordinates)":

::

   wget http://ligand-expo.rcsb.org/reports/H/HOH/HOH_ideal.pdb
   jmol HOH_ideal.pdb &

and searching for Sucrose (table sugar) you end up at
http://ligand-expo.rcsb.org/pyapps/ldHandler.py?formid=cc-index-search&target=Sucrose&operation=name-exact
and can download with:

::

   wget http://ligand-expo.rcsb.org/reports/S/SUC/SUC_ideal.pdb
   jmol SUC_ideal.pdb &



Big molecules
-------------

Then let's look at big molecules.  For example download pdb of this
molecule and others at the remarkable rcsb.org site
https://www.rcsb.org/structure/6DPA :

::

   wget https://files.rcsb.org/download/6DPA.pdb
   wget https://files.rcsb.org/download/6YI3.pdb  # seems related to covid-19

and load one or all of them in to paraview and the other 3D
visualizers, for example with:

::

   paraview 6YI3.pdb &
   avogadro 6YI3.pdb &
   jmol 6YI3.pdb &
   rasmol 6YI3.pdb &

My first impression: gdis is weird and sometimes goes out to lunch, so
I don't understand it enough yet.  avogadro and jmol and rasmol are
fast and immediate.  with paraview you have to do 2 things after
loading: (a) enable views by clicking on those two eyes in the left
side tree structure, and (b) hit the "apply" button top part of the
"properties" tab of the lower left area.

Looking a bit more: avogadro does not work on .cif files, so I'm
leaning toward jmol as being the most solid.




.. _sec-zeros-of-polynomials-and-other-functions:

Zeros of polynomials and other functions
========================================

Start from our comfortable solution to :math:`a x^2 + b x + c = 0`.

Talk a bit about cubics (how many real/complex roots, ...).

Talk a bit about quartics (biquadratic, how many roots, ...).

Discuss higher order polynomials and Galois theory.

Introduce numerical methods: Newton's method in particular (connection
to calculus).

Extend numerical methods to discuss finding several roots instead of
just the closest.

Connect this to optimization, TSP.


Artificial life
===============

Areas: biology, ecology, complex systems

* https://avida-ed.msu.edu/

* https://en.wikipedia.org/wiki/Avida


Genetic algorithms
==================

beautiful quote:

   Every boat is copied from another boat... Let’s reason as follows
   in the manner of Darwin. It is clear that a very badly made boat
   will end up at the bottom after one or two voyages, and thus never
   be copied... One could then say, with complete rigor, that it is
   the sea herself who fashions the boats, choosing those which
   function and destroying the others.

      Propos d’un Normand (1908); as quoted in "Natural selection and
      cultural rates of change" by D. S. Rogers
      and P. R. Ehrlich (2008) Proceedings of the National Academy of
      Sciences 105:3416–3420

      https://en.wikiquote.org/wiki/%C3%89mile_Chartier

   compare that to "coin tossing championship" (from Malcolm
   Gladwell's "Outliers".  discuss polynesian canoes.


Ottawa house of commons: John Sullivan uses evolutionary architecture.

Othello

Robbie the Robot (from Melanie Mitchell's book "Complexity: a Guided Tour")

you'll get to Melanie Mitchell's mooc when you get to chapter 4 on
emergent behavior it will point you here:
https://www.youtube.com/playlist?list=PLF0b3ThojznRyDQlitfUTzXEXwLNNE-mI
and for that section of my book the prerequisite is to watch section
6.3 of the mooc:
https://www.youtube.com/watch?v=ZVSseAnEzxs&index=85&list=PLF0b3ThojznRyDQlitfUTzXEXwLNNE-mI
watching the full MOOC and the section on Robbie the Robot should be
done at home since it goes somewhat afield of what we are doing at
work

Other resources:

* GAs on the TSP:
  https://cs.stanford.edu/people/eroberts/courses/soco/projects/1997-98/genetic-algorithms/apps.html

* Book: 

  - Genetic Algorithms in Python

* Examples:

  https://www.codeproject.com/Articles/1104747/Introduction-to-Genetic-Algorithms-with-Python-Hel

* For how to encode a game strategy:

  - To practice Othello: learn here: https://www.eothello.com/ and
    play here: https://hewgill.com/othello/


  - https://pdfs.semanticscholar.org/9fbb/a0583259b70e318003f5b6861faf9447060f.pdf

  - https://www.codingame.com/blog/genetic-algorithms-coders-strike-back-game/

  - This does a full 9-square-puzzle game encoding:
    https://www.researchgate.net/publication/220176829_Applying_genetic_algorithms_to_game_search_trees

  - tic-tac-toe:
    http://www.genetic-programming.org/sp2003/Hochmuth.pdf

  - https://www.cs.auckland.ac.nz/research/gameai/projects/GA%20in%20FreeCiv.pdf

  - othello:
    https://www.researchgate.net/publication/2599927_EVOLVING_COMPLEX_OTHELLO_STRATEGIES_USING_MARKER-BASED_GENETIC_ENCODING_OF_NEURAL_NETWORKS_David_Moriarty_and_Risto_Miikkulainen

  - programming it in Python and knapsack problem:
    https://blog.sicara.com/getting-started-genetic-algorithms-python-tutorial-81ffa1dd72f9

  - this goes in to chess; mentions othello in passing, and has an
    appendix on Elo ratings formulae
    https://arxiv.org/pdf/1711.08337.pdf

  - this has interesting discussions, but is ultimately about neural
    networks and thus harder than what I'm thinking of for this
    chapter.  http://nn.cs.utexas.edu/?moriarty:discovering,
    http://nn.cs.utexas.edu/downloads/papers/moriarty.discovering.pdf
    -- still, Moriarty and Miikkulainen do an interesting job of
    explaining why humans don't see some of the machine moves.

  - the previous article is one in this site:
    http://satirist.org/learn-game/methods/ga/marker.html -- this
    includes
    http://nn.cs.utexas.edu/downloads/papers/fullmer.genetic-encoding.pdf
    which seems to be about encodings.


  - this looks like a very nice intro to encoding an othello GA:
    https://pdfs.semanticscholar.org/ca88/76e8d8232403dfa4dcb75c67dd1031dd6bf3.pdf

  - other othello resource: https://github.com/blanyal/alpha-zero

  - much more academic stuff at
    https://scholar.google.com/scholar?hl=en&as_sdt=0%2C32&as_vis=1&q=othello+genetic+algorithm&btnG=

* Animated games:
  https://luckytoilet.wordpress.com/2011/05/27/coding-a-tetris-ai-using-a-genetic-algorithm/

* Evolving cellular automata:

  - https://arxiv.org/pdf/adap-org/9303003.pdf


Projects to work on: [... unfinished ...]



.. _chap-fourier-analysis:

Fourier Analysis
================

This is largely already written in my old latex book.  I just need to
bring it in here, and revise what I think of the prerequisites.

Latest: this has mostly been moved to the "mark working group" book.

Simpson's paradox
=================

https://qr.ae/TWNhZd?share=1
https://www.quora.com/What-is-Simpsons-paradox?share=1
