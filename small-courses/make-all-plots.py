#! /usr/bin/env python3

import os
import glob
import sys

## read all the gnuplot files (those that end in .gp) and run gnuplot
## on them to generate .svg output.  the .gp files need to have no
## "set terminal" and "set output" commands.

def main():
    for extension in ['svg', 'pdf']:
        # gp_file_list = []
        gp_file_list = glob.glob('*.gp')
        if len(sys.argv) >= 2:
            gp_file_list = sys.argv[1:]
        for gp_fname in gp_file_list:
            fname_base = gp_fname[:-3]
            c_fname = fname_base + '.c'
            py_fname = fname_base + '.py'
            data_fname = fname_base + '.txt'
            data_fname_dat = fname_base + '.dat'
            # form a list of dependencies based on whether the .c or
            # .py file exists
            for prog in (c_fname, py_fname):
                required_file_str = gp_fname
                if os.path.exists(prog):
                    required_file_str = required_file_str + ' ' + prog
                if os.path.exists(data_fname_dat):
                    required_file_str = required_file_str + ' ' + data_fname_dat
                # now find input data file names for gnuplot
                gp_data_filenames = find_required_data_for_gnuplot(gp_fname)
                # add those to the list of prerequisites
                for data_fname in gp_data_filenames:
                    required_file_str = required_file_str + ' ' + data_fname
                output_fname = gp_fname[:-2] + extension
                gp_cmd = ("""gnuplot -e \\"set terminal \'%s\'; set output '%s'; \\" %s"""
                          % (extension, output_fname, gp_fname))
                make_cmd = """(echo -n "%s: %s ;	" ; echo "%s") | make -f-""" % (output_fname, required_file_str, gp_cmd)
                print('## MAKE_CMD: %s' % make_cmd)
                os.system(make_cmd)

def find_required_data_for_gnuplot(gp_fname):
    """Heuristics to find the data files that might be called for in a
    plot file"""
    print('gp_fname:', gp_fname)
    required_files = []
    with open(gp_fname) as f:
        continuation_line = False
        for line in f.readlines():
            stripline = line.strip()
            if not stripline or stripline[0] == '#':
                continue
            words = line.split()
            # heuristics to see if any of the words on a line is a
            # file name.  my simple heuristic for now is to say that
            # if a word has single quotes around it, and it does not
            # come right after xlabel, ylabel or title, then it's a
            # filename to be plotted
            for i, word in enumerate(words):
                if word[0] == "'" and word[-1] == "'":
                    if not (i > 0 and words[i-1] in ('xlabel', 'ylabel', 'title')):
                        word = word.strip("'")
                        print('APPEND_DATA_FILE:', word)
                        required_files.append(word)
                if word == '\\' and i == len(words)-1:
                    # global continuation_line
                    continuation_line = True
                else:
                    continuation_line = False
                    
                
    return required_files

if __name__ == '__main__':
    main()
