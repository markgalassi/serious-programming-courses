import networkx as nx
import matplotlib.pyplot as plt

def print_data(filepath):
    with open(filepath) as bitcoin_data_unformatted:
        for row in bitcoin_data_unformatted:
           print(row)


print_data('soc-sign-bitcoinalpha.csv') # this assumes your file is in the same directory as your data file. If it isn't you'll have to get an absolute path (e.g. one starting with a /)
