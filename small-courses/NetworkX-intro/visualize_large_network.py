import networkx as nx
import matplotlib.pyplot as plt
import random # for remove_nodes()

correct_negative_weights = 11
# because the trusts in the bitcoin data range from -10 to 10, to make all of them
# positive, we can add 11. The reason we don't add 10 is that weights of 0 result in
# division by 0, which causes problems.

def main():
    bitcoin_network = convert_data_to_network('soc-sign-bitcoinalpha.csv') # make sure .csv file is in the same directory as this .py file
    bitcoin_network = remove_nodes(3000, bitcoin_network)
    # 3000 is almost certainly more than you'll have to
    # remove, but it's a good starting place.
    show_data(bitcoin_network)

def convert_data_to_network(filepath):
    """this is a function that will convert data from the .csv file to a NetworkX variable
    called bitcoin_network."""
    bitcoin_network = nx.DiGraph()
    # remember that we need a directed network because one user is rating the other.
    with open(filepath) as bitcoin_data_unformatted:
        for line in bitcoin_data_unformatted:
            words = line.split(',')
            words[3].replace('\n', '')
            # the format of the data adds a \n to the end of every row
            row = []
            for i in range(len(words)):
                row.append(int(words[i]))
            row[2] = row[2] + correct_negative_weights
            # weights must be positive. We'll undo this later.
            bitcoin_network.add_edge(row[0], row[1], weight=row[2])
            
    return bitcoin_network

def remove_nodes(num_to_remove, network):
    """this is a very large network, and for the sake of your computer we'll remove some
    of the nodes and edges."""
    nodes = list(network.nodes)
    random.shuffle(nodes)
    for i in range(num_to_remove):
        network.remove_node(nodes[i])

    return network

def show_data(network):
    """simple function that will display the bitcoin network using matplotlib."""
    fig, ax = plt.subplots()
    nx.draw(network, ax=ax, arrows=False, node_size=1, alpha=0.4, width=0.15)
    # with this many edges arrows just muddle the graph and make it hard to read
    ax.set_title('Bitcoin network with ' + str(network.number_of_nodes()) + ' nodes')
    plt.savefig('bitcoin_network.png')
    print('Saved bitcoin network as .png file in bitcoin_network.png')
    
main()
