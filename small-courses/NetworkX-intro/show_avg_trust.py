import networkx as nx
import matplotlib.pyplot as plt

correct_negative_weights = 11
# because the trusts in the bitcoin data range from -10 to 10, to make all of them
# positive, we can add 11. The reason we don't add 10 is that weights of 0 result in
# division by 0, which causes problems.

def main():
    bitcoin_network = convert_data_to_network('soc-sign-bitcoinalpha.csv')
    show_avg_trust(bitcoin_network)

def convert_data_to_network(filepath):
    """this is a function that will convert data from the .csv file to a NetworkX
    variable called bitcoin_network."""
    Data = nx.DiGraph()
    # remember that we need a directed network because one user is rating the other.
    with open(filepath) as data:
        for row in data:
            row = row.split(',')
            row[3].replace('\n', '') # the format of the data adds a \n to the end of every row
            for i in range(len(row)):
                row[i] = int(row[i])
            row[2] = row[2] + correct_negative_weights # weights must be positive. We'll undo this later.
            Data.add_edge(row[0], row[1], weight=row[2])
            
    return Data

def get_avg_trust(network, node):
    """given a node, find the average trust assigned to it by other nodes"""
    raters = list(network.predecessors(node))
    total_rating = 0
    for rater in raters:
        total_rating += network[rater][node]['weight']
    if len(raters) != 0:
        total_rating /= len(raters)
        # when extracting the data, we added a constant, we now have to subtract it off again.
        total_rating -= correct_negative_weights
    else:
        total_rating = None
    return total_rating
        
def show_avg_trust(network):
    """given a network, find average trust of all nodes in the network and display the
    result as a graph"""
    avg_trusts = []
    for node in network.nodes:
        avg_trust = get_avg_trust(network, node)
        if avg_trust != None:
            avg_trusts.append(avg_trust)
    avg_trusts.sort(reverse=True)
    fig, ax = plt.subplots()
    plt.plot(range(len(avg_trusts)),avg_trusts)
    plt.savefig('avg_trust.png')
    print('Trust graph saved to .png file called avg_trust.png')
    
main()
