import networkx as nx
import matplotlib.pyplot as plt

correct_negative_weights = 11
# because the trusts in the bitcoin data range from -10 to 10, to make all of them
# positive, we can add 11. The reason we don't add 10 is that weights of 0 result in
# division by 0, which causes problems.

def main():
    bitcoin_network = convert_data_to_network('soc-sign-bitcoinalpha.csv')
    show_avg_trust_bar(bitcoin_network, 20)

def convert_data_to_network(filepath):
    """this is a function that will convert data from the .csv file to a NetworkX
    variable called bitcoin_network."""
    bitcoin_network = nx.DiGraph()
    # remember that we need a directed network because one user is rating the other.
    with open(filepath) as data:
        for row in data:
            row = row.split(',')
            row[3].replace('\n', '')
            # the format of the data adds a \n to the end of every row
            for i in range(len(row)):
                row[i] = int(row[i])
            row[2] = row[2] + correct_negative_weights
            # weights must be positive. We'll undo this later.
            bitcoin_network.add_edge(row[0], row[1], weight=row[2])
            
    return bitcoin_network

def get_avg_trust(network, node):
    """returns the average trust of a given node."""
    raters = list(network.predecessors(node))
    total_rating = 0
    for rater in raters:
        total_rating += network[rater][node]['weight']
    if len(raters) != 0:
        total_rating /= len(raters)
        total_rating -= correct_negative_weights
        # when extracting the data, we added a constant, we now have to subtract
        # it off again
    else:
        total_rating = None
    return total_rating
        
def show_avg_trust_bar(network, num_baskets):
    """divides weights in the network into a certain number of baskets, then graphs it."""
    avg_trusts = []
    cutoffs = list(range(-10, 10, int(round(20/num_baskets))))
    baskets = []
    for i in range(num_baskets):
        baskets.append(0)
    cutoffs.append(10)
    formatted_baskets = []
    for i in range(len(cutoffs) - 1):
        formatted_baskets.append(str(cutoffs[i]))
    for node in network.nodes:
        avg_trust = get_avg_trust(network, node)
        if avg_trust != None:
            for i in range(len(baskets)):
                if avg_trust < cutoffs[i + 1] and avg_trust > cutoffs[i]:
                    baskets[i] += 1
    
    fig, ax = plt.subplots()
    plt.bar(formatted_baskets,baskets)
    plt.savefig('avg_trusts_bar.png')
    print('Trust bar graph saved to .png file avg_trusts_bar.png')
    
        
main()
