.. _chap-random-number-basics:

=======================
 Random number basics
=======================

[status: unwritten]

Purpose:

* Learning more about random numbers


Prerequisites
=============

* The 10 hour "serious programming" course.

* A GNU/Linux system with python3 and python3-tk installed (on an
  Ubuntu 16.04 system this can be done with ``sudo apt install python3-tk``)


Motivation
==========

In our "serious programming" course we used random numbers to program
a random strategy in the computer's tic-tac-toe algorithm.

It turns out that random numbers appear in very many areas of
scientific computing, so in this chapter we will become comfortable
with how to generate, plot and use random numbers.


Types of distributions
======================

Here we will go a bit further and look at the various calls in
Python's ``random`` library, explore some simple ... FIXME: to be
filled


Further reading
===============

[unfinished]

* https://www.youtube.com/watch?v=_tN2ev3hO14
