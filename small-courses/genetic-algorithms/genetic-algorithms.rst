.. _chap-genetic-algorithms:

====================
 Genetic Algorithms
====================

.. sectionauthor:: Joaquin Bas <jfbas22@gmail.com>

Motivation, Prerequisites, Plan
===============================

Motivation
-----------

Armed with powerful data processing and storage capabilities,
computers are ideal for performing repetitive tasks and sometimes can
"see" farther than humans in strategic situations.

It is therefore unsurprising that during the execution of a complex
task it may suboptimal to allow a human to formulate the "strategy"
necessary to complete the task and far more efficient to allow a
computer to "evolve" its own strategy, and this can be done using
genetic algorithms.

A subclass of evolutionary algorithms under the domain of artificial
intelligence, genetic algorithms have exciting and diverse
applications. From optimizing a janitorial robot's cleaning route
within a confined space(Mitchell, 2009) to minimizing risk while
maximizing profit for a given stock portfolio, genetic algorithms are
powerful tools with a growing impact on society.

In this chapter we will construct our own simple genetic algorithm in
order to explore, analyze, and understand these fascinating
algorithms.

Prerequisites
--------------

* The ten hour "serious programming" course
* The "Data Files and First Plots" mini course in section 2
* The "Intermediate Plotting" mini course in section 3
* The "Random Number Basics" mini course in section 11

Plan
-----
The construction of our genetic algorithm will be split into four parts:

#. Population Generation
#. Fitness Calculation
#. Selection 
#. Crossover and Mutation

Following the completion of our program we will create plots in
gnuplot to analyze and explore the functionality of genetic algorithms
more closely.

Constructing the Genetic Algorithm
==================================

Some Terminology Before we Begin
---------------------------------

Disclaimer: The following definitions are only analogously related to
biological terms, so none of the terms below are precisely parallel in
nature.

Important: The only way around jargon is through it!

Genetic Algorithm: A subclass of evolutionary computation(EA)
algorithm that "evolves" a solution space to optimize the solution to
a problem.

Algorithm: A procedure with a series of logical, sequential steps constructed to
outline the process for completing a certain task.
 
Binary bit: A digit in the binary number system that can either have a
value of 1 or 0.

Binary Bitlist: A sequence of binary digits separated by commas.

Binary Bitstring: A sequence of unseparated binary digits.

Chromosome: A unique binary bitlist or bitsring of N digits, e.g 10001.

Population: A unique collection of M chromosomes e.g 10001, 00001,10101.

Fitness: The number of 1s in a chromosome and/or population.

Fitness Function: A function that evaluates the fitness of each
chromosome and of the population in a GA.

Crossover: Combination of 'parent' chromosomes to form 'children'
chromosomes, e.g. a crossover of 1000 and 0001 could be 10001.

Mutation: The random flipping at a certain probability of a bit in a
bitstring in a chromosome in a population of a GA.

Mutation Rate: Probabilty random bit in bitstring or bitlist  will we flipped. 

Elitist Selection: Selection of certain chromosomes to be 'parents'
based on passing a certain fitness threshold.

Generation: In genetic algorithms, a "generation" simply signifies
another population which the algorithms generates and operates on.

Our GA
-------

This genetic algorithm was created for pedagogical
purposes, so it will not be optimizing a real world problem, rather it
will simply be optimizing a binary bitlist to demonstrate its own
functionality.

Designing the Algorithm
------------------------

Simulating evolution is a delicate process, and thus we must plan our
algorithm carfully.

Our genetic algorithm will be subdivided into the following steps:

#. generate an initial population of M individuals(let's start with
   10 for simplicity) with each chromosome being of length N (10 for
   now as well)
   
#. for each chromosome in the population, calculate the fitness by
   counting the number of 1s in the list, where the minimum value is 0
   and the maximum value is 10
   
#.  after generating the initial population, select the top twenty
    percent of individuals to be "parents" for the next generation(2
    bitlists with the highest fitnesses in that population are
    selected)
    
#. create the next generation by "breeding" the parents selected,
   whose children will become the new population
   
#. at a random probability(0.05 to start with) mutate the children to
   create a new population
   
#. go back to step 2 and repeat for G generations(100 for now)

As we can already see, the genetic algorithm will be evolving the best
bitlists by only selecting those with higher fitnesses to be parents
and so on.

Programming the GA
-------------------

We must first import relevant python modules:

.. code-block:: python

   import random 
   import math
   import sys

Then, we must set some initial parameters that will follow us
throughout our program:

.. code-block:: python

   prob_mutation=0.05
   n_bits=10
   n_members=10
   n_generations=10
 
Now we set the skeleton for the main program(without the real function
names of course):

.. code-block:: python

   def main():
       function1()
       function2()
       function3()
       # ....

   main()
 
Our main program with function names:

.. code-block:: python

    def main():
        global prob_mutation, n_bits, n_members, n_generations
        if len(sys.argv) != 1 and len(sys.argv) != 5:
             print('error: either run with no arguments or with 4 arguments')
	     print('%s [n_bits prob_mutation n_members n_generations]')
	     sys.exit(1)
        if len(sys.argv) > 1:
             prob_mutation = float(sys.argv[1])
	     n_bits = int(sys.argv[2])
	     n_members = int(sys.argv[3])
	     n_generations = int(sys.argv[4])

        pop = make_pop(n_members)
        display_pop(pop, 0)
        evolve(pop, n_generations)

   
Important: We will construct this program by first adding skeletons
and later adding functionality, which allows us to see what we want
from our program first before implementing it.

Moving on, to create outr popuation, we create the function make_pop:

.. code-block:: python

   def make_pop(n):
       """create the initial population"""
       pop=[]
       for i in range(n):
           pop.append(make_individual())  
       return pop

Next, we need to create individual chromosomes or individuals as well,
so let's create a funciton called make_individual:

.. code-block:: python

   def make_individual():
       """Make a random individual"""
       bitlist=[]
       for i in range(n_bits):
           bit=random.randint(0,1)
           bitlist.append(bit)
       return bitlist

Important: If at any time you find any of the above code confusing, do
not procedd until it is clear, as the complexity within the program
will only compound.


Then, to display our population, we create the funciton display_pop:

.. code-block:: python

   def display_pop(pop, gen_no):
       print('==== entire population (%d individuals) ====' % len(pop))
       for i, bitlist in enumerate(pop):
           fitness = calc_fitness(bitlist)
           print('    index: %d    bitlist: %s    fitness: %g' 
                 % (i, bitlist, fitness))
       avg_fit, top_fit = avg_and_top_fitness(pop)
       print("GEN_REPORT:", gen_no, avg_fit, top_fit)

Now, to calculate the fitness for an individual, we create the
funciton calc_fitness:

.. code-block:: python
 
   def calc_fitness(member):
       """calculate the fitness of an individual, a list of bits where the
       fitness is the sum of the bits"""
       fitness=0
       for i in range(len(member)):
           fitness += member[i]
       return fitness

Next, let's add in our average and top fitnesses for each population:

.. code-block:: python

   def avg_and_top_fitness(pop):
       top_fitness=-1
       avg_fitness=0
       sum_fitness=0
       for member in pop:
           fit=calc_fitness(member)
           sum_fitness+=fit
           if fit>top_fitness:
               top_fitness=fit
       return sum_fitness/len(pop),top_fitness

 We now must proceed to generate an evolutionary framework for our GA
 as we have just completed displaying and calculating fitnesses:

.. code-block:: python

   def evolve(pop, n_gen):
       """Evolve a population for n_gen generations"""
       for i in range(n_gen):
           parents=select_parents(pop)
           children=breed(parents,len(pop))
           pop=parents+children
           print('==== at generation %d ====' %i)
           display_pop(pop, i+1)

Now that we have an evolution skeleton, we will add some functionality, starting with the breeding mechanism by creating a function called breed.

.. code-block:: python

   def breed(parents, n_offspring):
       """Breed the parents to get a collection of offspring; apply mutation
          as part of the breeding process"""
       children=parents+parents+parents+parents
       assert(len(parents) + len(children) == n_offspring) # sanity check
       children=mutate_pop(children)
       return children

We now need selection criteria, so we will create the select_parents and the supporting select_20_pct below:

.. code-block:: python

   def select_parents(pop):
       """Select a part of the population to be the parents.  At this time we
       only use a top-20% approach; in the future it should be
       selectable
       """
       assert(len(pop)%5==0)
       parents=select_20_pct(pop)
       return parents


.. code-block:: python

   def select_20_pct(pop):
       """Select the top 20% of the population"""
       assert(len(pop)%5==0)
       fitnesses=[]
       for member in pop:
           fitness=calc_fitness(member)
           fitnesses.append((member, fitness))
       fitnesses.sort(key=lambda item: item[1])
       fitnesses[-len(pop)//5:]
       # print(fitnesses[-len(pop)//5:])
       result_top  = []
       for (member, fitness) in fitnesses[-len(pop)//5:]:
            result_top.append(member)
       return result_top

Moving on, some mutation functionality is needed for the evolution
framework, so we will create the mutate_pop and the supporting
mutate_individual below:

.. code-block:: python

   def mutate_pop(children):
       """Mutate an entire population, returning the result"""
       for child_no, child in enumerate(children):
           new_child = mutate_individual(child, prob_mutation)
       children[child_no] = new_child
   return children


.. code-block:: python

   def mutate_individual(old_member, prob):
       """Takes a member of the population, applies mutation to each bit with
       probability prob, and returns the result
       """
       member = old_member[:]      # make a copy of the original
       for i in range(len(member)):
           if random.random() > prob: # roll the die
               continue            # don't mutate this bit
           if member[i] == 1:
               member[i] = 0
           else:
               member[i] = 1
       return member


So, for the finished program, we have :numref:`listing-simple-ga-py`

.. _listing-simple-ga-py:

.. literalinclude:: simple_ga.py
   :language: python
   :caption: simple_ga.py - full program for a simple genetic algorithm


Note: You can either copy and paste this code directly into a file,
save, and run, or download the code from the appendix section.

Plots and Analysis
==================

In this section we will both plot the fitness function over time and
tweak the parameters in our genetic algorithm for the purpose of
gaining some intuition on how genetic algorithms function.

Plots
------

In gnuplot, we will first generate a simple plot of our
fitness as a function of the number of generations elapsed.

To begin we press control + alt + t to open up our linux terminal, in
which we navigate to the appropriate directory and type:

.. code-block:: bash

    python3 simple_ga.py > ga-first-run.out

    grep GEN_REPORT:ga-first-run.out
    grep GEN_REPORT:ga-first-run.out > gen_report-first-run.out
    gnuplot
    gnuplot> set xlabel 'number of generations'
    gnuplot> set ylabel 'average fitness'
    gnuplot> set title 'first-run'
    gnuplot> plot 'gen_report-first-run.out' using 2:3 with linespoints


So, our plot should look like this:

.. _fig-first-run:

.. figure:: first-run.*

   Generation report for the first run.

Important: If you do not understand any of these instructions, that is
ok, but you should probably go see section 2 to refresh yourself with
gnuplot.

In this next  plot, we will have the following parameters:

::

   mutation rate=25 percent
   chromosome length=10
   populaiton size=100
   generations=100

Now, we must  type:

.. code-block:: bash

    python3 simple_ga.py  0.25 10 10 100 | grep GEN_REPORT: > high-mut.out
    gnuplot
    gnuplot> set xlabel 'number of generations'
    gnuplot> set ylabel 'average fitness'
    gnuplot> set title '25 percent mutation rate'
    gnuplot> plot 'high-mut.out' using 2:3 with linespoints
  
So now our plot should like this:

.. warning::

   This the high-mut.png figure is not present.

..
   .. _fig-high-mut:

   .. figure:: high-mut.png

      Generation report for the high mutation rate.

We can save our plot by selecting the 'export to image' in the upper
left hand corner of the plot

Our next task is to create the standard plot with a population size
increased to 100 members, and thus the parameters for this plot are:

::

   mutation rate=0.05
   chromosome length=10
   population size=100
   generations=100

Note: In order to execute the following commands from the command line
successfully, it is important to know the order of the arguments
below.

The arguments are, in order from left to right: prob_mutation, n_bits,
n_members,n_generations

Now type:

.. code-block:: bash


    python3 simple_ga.py  0.05 10 100 100 | grep GEN_REPORT: > large-pop.out
    gnuplot
    gnuplot> set xlabel 'number of generations'
    gnuplot> set ylabel 'average fitness'
    gnuplot> set title 'large population'
    gnuplot> plot 'large-pop.out' using 2:3 with linespoints

You will repeat these instructions with different parameters and
output files below, so make sure you are clear on the instructions
above before proceeding.


So our plot should look like this:

.. _fig-large-pop:

.. figure:: large-pop.*

   Generation report for the large population.


The next factor we should alter for our next plot is the chromosome
lenght, more specifically, let's change it from ten to 100.

so our parameters for this plot are:

::

   mutation rate=0.05
   chromosome length=100
   population=10
   generations=100


Now type:

.. code-block:: bash


    python3 simple_ga.py  0.05 100 10 100 | grep GEN_REPORT: > long-chromo.out
    gnuplot
    gnuplot> set xlabel 'number of generations'
    gnuplot> set ylabel 'average fitness'
    gnuplot> set title 'long chromosome'
    gnuplot> plot 'long-chromo.out' using 2:3 with linespoints
  

So our plot should look like this:

.. _fig-long-chromo:

.. figure:: long-chromo.*

   Generation report for the long chromosome.

As for our last plot, we will change the number of generations to 1000
from 100

so our parameters for this plot are:

::

   mutation rate=0.05
   chromosome length=10
   population size=10
   generations=1000



.. code-block:: bash

    python3 simple_ga.py  0.05 10 10 1000 | grep GEN_REPORT: > gen1000.out
    gnuplot
    gnuplot> set xlabel 'number of generations'
    gnuplot> set ylabel 'average fitness'
    gnuplot> set title '1000 generations'
    gnuplot> plot 'gen1000.out' using 2:3 with linespoints
  

So our plot should look like what you see in :numref:`fig-gen1000`:

.. _fig-gen1000:

.. figure:: gen1000.*
   

   Generation report for 1000 generations.

We have finished our generating all of our plots for the parameter
variations of the standard ga generation/fitness plot and in the
following section will proceed to analyze and dissect each graph and
understand how the changes in parameters affected the components of
each graph.

Analysis
--------

In this section we will analyze and dissect each plot created in the
previous section as each was created to demonstrate the alteration of
one parameter in the genetic algorithm, our purpose beign to see how
each parameter affected the GA as a whole.

Standard or Plot with Default Parameters
-----------------------------------------
Analyzing the first plot is
important for gaining intuition for the standard behavior of our GA
for this fitness landscape, so we can compare it to other behavior
below.

With standard parameters, the GA we designed rises quickly in fitness
but then plateas with slight oscillation, with the fitnesses toward
the end of the program residing between 9.5 and 10.

Changing the Mutation Rate Parameter
-------------------------------------

In this section we will see how alteirng the mutation rate within our
genetic algorithm changes the behavior of the overall program.

With the high mutation rate, we see that the fitness does not converge
as nicely as it did with the standard plot, and our fitnessses towards
the end of the GA are lower, between 7.5 and 8.5.

Why? A higher mutation rate means more bits are being flipped, which
means even with the elitist selection, the population cannot converge
as well, and as we can imagine, were we to set the fitness to 1 or 100
percent, our GA would just be a random walk.

Changing the Population Parameter
----------------------------------

In this section we will see how altering the population parameter
changes the overall behavior of the genetic algorithm.

The difference we see between this plot and the standard plot is that
the standatd plot similarly plateaus, but doesn't converge as nicley
and oscillates more.

Why? Because a larger population means more diverse individuals, which
gives the GA a better chance of selceting very fit individuals earlier
on and thus converging more smoothly.

Changing the Chromosome Length Parameter
-----------------------------------------

In the last section we saw what altering the population parameter did
to the overall fitness of a population, so in this portion of this
chapter we're going to analyze what changing the chromosome length
parameter does to the overall fitness of the population.

The first thing we notice about this plot vs the standard plot is the more gradual increase in the fitness of the population.

Why?

Increased chromosome length means the same fitness in an individual
can contain a greater absolute value of zeroes, which means that a
convergence will not happen as quickly as there are more zeroes to get
rid of, and thus towards the end of the GA we can see the fitness is
slightly lower than that of the standard program , hovering between
8.5 and 90(adjusted to the scale of the standard plot of course).

Changing the Number of Generations Parameter
---------------------------------------------

The final parameter we altered in our genetic algorithm was that of
the number of generations, and in this section we will dissect this
parameter's role and contribution to the funcitonality of the genetic
algorithm.

This plot is essentially the standard plot run for ten times longer,
so nothing about the behavior has changed, and nothing interesting has
happened with the addition of 900 generations, as this GA has a simple
fitness landscape.

Then why would we bother to talk about a paramter that does nothing in our case?

Well, for optimal comprehension of this topic, one must be aware of
what afffects the GA as well as what does not to get a more compelte
picture of the algorithm's functionality.



Fitness landscape of simple_ga.py
-----------------------------------
In this brief section we will examine the fitness landscape of simple_ga.py

.. plot::

   import matplotlib.pyplot as plt
   import numpy as np
   def fitness_wavy(x):
       fitness = x
       return fitness
   x = np.linspace(0, 10, 100)
   y = fitness_wavy(x)
   plt.plot(x, y, 'r')
   plt.grid()
   plt.title(r'simplest fitness function: sum of bits')
   plt.show()


A more complex fitness landscape
---------------------------------

Here we move beyond the simple fitness function of
:file:`simple_ga.py` and explore a more interesting fitness function,
which presents some real challenges in finding the maximum point.

The fitness idea here is to think of our collection of bits as
representing *a floating point number*.  If you take 32 bits, those
represent the various parts of a floating point number.  The
particular way in which floats are represented as 32 bits is the IEEE
754 format.  Examples are:

::

   3.14159     01000000010010010000111111010000
   1.41421     00111111101101010000010011010101
   6.02e+23    01100110111111101111010011111001

Each bit has a different significance from the others, so randomly
flipping a bit can make a very small or very big difference.

Now that we have a real number corresponding to each bit string, we
can take a function of these real numbers, and call that the fitness.

Let us try this wavy function:

.. math::

   y = cos(x-40) + 10 \exp^{-(x - 40)^2/5000}

seen in this figure:

.. _fig-wavy-fitness-landscape:

.. plot::

   import matplotlib.pyplot as plt
   import numpy as np
   from math import *
   def fitness_wavy(x):
       fitness = np.cos(x-40) + 10*np.exp(-(x - 40)**2/5000.0)
       return fitness
   x = np.linspace(-200,200, 1000)
   y = fitness_wavy(x)
   plt.plot(x, y, 'r')
   plt.grid()
   plt.title(r'wavy fitness function $y = cos(x-40) + 10 \exp^{-(x - 40)^2/5000}$')
   plt.show()


The plot only shows a tiny central area of all the possible floating
point values, so our genetic algorithm search will need to search many
real numbers until it starts finding those peaks.  And once it finds a
lower peak, it might get stuck there for a while before finding a
higher peak.

.. note::

   You can see from the plot that the peaks seem to be at 6, 6 and a
   bit, and 8 and a bit.

Now download the program :download:`ga_wavy_fitness.py` and explore
this fitness landscape with it, similarly to how we did with
`simple_ga.py`

.. _fig-wavy-default:

.. figure:: wavy-default.*

   Generation report for the wavy fitness with default settings.

We got to see cool fitness steps, but we know that the global maximum
is bigger.  Try running longer:

.. _fig-wavy-long:

.. figure:: wavy-long.*

   Generation report for the wavy fitness with a long run:
   ``python3 ga_wavy_fitness.py 0.05 32 10 1000 | grep REPORT``

Stil no luck.  Try a larger population:

::

   python3 ga_wavy_fitness.py 0.05 32 100 100 | grep REPORT
   python3 ga_wavy_fitness.py 0.05 32 100 1000 | grep REPORT
   python3 ga_wavy_fitness.py 0.05 32 100 10000 | grep REPORT

Play with mutation rate:

::

   python3 ga_wavy_fitness.py 0.10 32 100 1000 | grep REPORT
   python3 ga_wavy_fitness.py 0.10 32 100 10000 | grep REPORT
   python3 ga_wavy_fitness.py 0.20 32 100 1000 | grep REPORT
   python3 ga_wavy_fitness.py 0.20 32 100 10000 | grep REPORT

That last one seems good.  Let's plot it:
   
.. _fig-wavy-pop-mut:

.. figure:: wavy-pop-mut.*

   Generation report for the wavy fitness with a large population,
   high mutation rate:
   ``python3 ga_wavy_fitness.py 0.20 32 100 1000 | grep REPORT``

Is this the global optimum?

Why did the average fitness go down like that?

Insights into the process of evolution?  Punctuated equilibrium.


Now study what happens with lower mutation and longer run:

.. _fig-wavy-low-mut-very-long:

.. figure:: wavy-low-mut-very-long.*

   Generation report for the wavy fitness with a low mutation rate but
   a very long run:
   ``python3 ga_wavy_fitness.py 0.10 32 100 10000 | grep REPORT``


Excercises
==========

Exploration and practice are what cement one's knowledge and
proficiency with a topic or area of expertise, be it academic or not,
and genetic algorithms and computer science are no exceptions.

Thus, in this section we will provide ways for the reader to cement
their knowledge by doing such practice, and thus the following
excercises would be encouraged.

.. note::

   These 'excercises' are meant to be exploratory, so don't take them
   as a homework assignment, and there will not be and answer key at
   the end of the chapter, complete the one's that you can, and if you
   struggle with any, just play around with them until they make sense
   and reach out for help, after all, coding is generally a team sport
   in the real world.


Modify simple_ga.py to select the top 10 percent of the population
instead of the top twenty percent as parents.

Modify simple_ga.py so that fitness is defined by the number of 0s in
he bitlist, not the number of 1s.

Try switching the function for ga_wavy_fitness from
cos(x-40)+10e^(-(x-40)^2/5000) to sin(x-40) + 10e^(-(x-40)^2/5000)

In the function cos(x-40)+10e^(-(x-40)^2/5000), change every 40 to a
1000, and run the code for different amounts of generations and write
down your observations and interesting results/conclusions.


Applications
============

Research
---------
Genetic Algorithms have real world applications which are very diverse
and intriguing, as we will see below.

Genetic algorithms are used in academic research in
mathematics, computer science, and physics.

Genetic algorithms are used in cryptography research as they can
search through large solution spaces and find decryptions.

Genetic algorithms are also used in machine learning research,
specifically in feature selection.

Genetic algorithms are also used in physics research for Feynman-Kac
formula models.

Industry
---------
Genetic algorithms are also utilized in industries where optimiation is key.

Genetic algorithms are used in RSO(resource scheduling optimization)
to optimize sshceduling for government and large corporations.

Genetic Algorithms are used in Finance to optimize portfolios, that
is, for a certain amount invested, mninimize the risk and maximize
profit.

Genetic Algorithms are used in electronic circuit design.

Genetic Algorithms are used in quality control in product engineering.


Further Study and Resources
============================

Complexity: A Guided Tour by  Melanie Mitchell

An Introduction to Genetic Algorithms by Melanie Mitchell

Adaptation in Natural and Aritficial Systems by John Holland


https://towardsdatascience.com/introduction-to-optimization-with-genetic-algorithm-2f5001d9964b

https://en.wikipedia.org/wiki/Genetic_algorithm

https://pathmind.com/wiki/evolutionary-genetic-algorithm

Source Code for GA
-------------------
:download:`simple_ga.py`

Sources
---------

Mitchell, M. (2011). Complexity: A Guided Tour (1st ed.). Oxford
University Press.

Mitchell, M. (1998). An Introduction to Genetic Algorithms (Complex
Adaptive Systems) (Reprint ed.). MIT Press.

Holland, J. (1975). Adaptation in Natural and Artificial Systems. MIT Press.

Acknowledgements
------------------

A special thanks to Mark Galassi, Melanie Mitchell, Nina Bunker, and
Rhonda Crespo for suggesting edits and other improvements to this
chapter.

A special thanks to Melanie Mitchell for introducing me and mentoring
me on genetic algorihtms.

A special thanks to Mark Galassi for helping me build and edit this
book chapter and everything that accompanies it.

A special thanks to the Institute for Computing in Research for having
me as an intern for their 2020 summer program.

