##REQUIRED_FILE: gen1000.out

plot 'gen1000.out' using 2:3 with linespoints
set xlabel 'number of generations'
set ylabel 'average fitness'
set title 'average fitness of a population as a funciton of the number of generations passed'
