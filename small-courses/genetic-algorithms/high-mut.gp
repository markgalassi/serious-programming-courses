##REQUIRED_FILE: high-mut.out

plot 'high-mut.out' using 2:3 with linespoints
set xlabel 'number of generations'
set ylabel 'average fitness'
set title 'average fitness of a population as a funciton of the number of generations passed'
