##REQUIRED_FILE: wavy-low-mut-very-long.out

set xlabel 'number of generations'
set ylabel 'fitness'
set title 'average fitness of a population as a function of the number of generations passed'
plot 'wavy-low-mut-very-long.out' using 2:3 with linespoints title "average fitness", 'wavy-low-mut-very-long.out' using 2:4 with linespoints title "best fitness"
