#! /bin/sh

echo "running: $0 in " `pwd`
echo "args: $*"

echo 'this chapter is not included; exiting'
exit 0

# first run data and plot
echo "first-run.out: simple_ga.py pre-build.sh ;	python3 simple_ga.py | grep GEN_REPORT: > first-run.out" | make -f-

# high mutation rate
echo "high-mut.out: simple_ga.py pre-build.sh ;	python3 simple_ga.py 0.25 10 10 100 | grep GEN_REPORT: > high-mut.out" | make -f-

# large population
echo "large-pop.out: simple_ga.py pre-build.sh ;	python3 simple_ga.py 0.05 10 100 100 | grep GEN_REPORT: > large-pop.out" | make -f-

# long chromosome
echo "long-chromo.out: simple_ga.py pre-build.sh ;	python3 simple_ga.py 0.05 100 10 100 | grep GEN_REPORT: > long-chromo.out" | make -f-

# 1000 generations
echo "gen1000.out: simple_ga.py pre-build.sh ;	python3 simple_ga.py 0.05 10 10 1000 | grep GEN_REPORT: > gen1000.out" | make -f-

# wavy fitness default
echo "wavy-default.out: ga_wavy_fitness.py pre-build.sh ;	python3 ga_wavy_fitness.py | grep GEN_REPORT: > wavy-default.out" | make -f-

# wavy fitness long run
echo "wavy-long.out: ga_wavy_fitness.py pre-build.sh ;	python3 ga_wavy_fitness.py 0.05 32 10 1000 | grep GEN_REPORT: > wavy-long.out" | make -f-

# wavy fitness big pop high mutation
echo "wavy-pop-mut.out: ga_wavy_fitness.py pre-build.sh ;	python3 ga_wavy_fitness.py 0.20 32 100 450 | grep GEN_REPORT: > wavy-pop-mut.out" | make -f-

# wavy fitness low mutation long run
echo "wavy-low-mut-very-long.out: ga_wavy_fitness.py pre-build.sh ;	python3 ga_wavy_fitness.py 0.10 32 100 10000 | grep GEN_REPORT: > wavy-low-mut-very-long.out" | make -f-
