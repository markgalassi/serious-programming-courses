##REQUIRED_FILE: wavy-pop-mut.out

set xlabel 'number of generations'
set ylabel 'fitness'
set title 'average fitness of a population as a function of the number of generations passed'
plot 'wavy-pop-mut.out' using 2:3 with linespoints title "average fitness", 'wavy-pop-mut.out' using 2:4 with linespoints title "best fitness"
