#! /usr/bin/env python3

import random
import math
import sys

prob_mutation = 0.05
n_bits = 10
n_members = 10
n_generations = 1000

def main():
    global prob_mutation, n_bits, n_members, n_generations
    if len(sys.argv) != 1 and len(sys.argv) != 5:
        print('error: either run with no arguments or with 4 arguments')
        print('%s [n_bits prob_mutation n_members n_generations]')
        sys.exit(1)
    if len(sys.argv) > 1:
        prob_mutation = float(sys.argv[1])
        n_bits = int(sys.argv[2])
        n_members = int(sys.argv[3])
        n_generations = int(sys.argv[4])

    pop = make_pop(n_members)
    display_pop(pop, 0)
    evolve(pop, n_generations)

def make_pop(n):
    """create the initial population"""
    pop=[]
    for i in range(n):
        pop.append(make_individual())  
    return pop

def make_individual():
    """Make a random individual"""
    bitlist=[]
    for i in range(n_bits):
        bit=random.randint(0,1)
        bitlist.append(bit)
    return bitlist

def display_pop(pop, gen_no):
    print('==== entire population (%d individuals) ====' % len(pop))
    for i, bitlist in enumerate(pop):
        fitness = calc_fitness(bitlist)
        print('    index: %d    bitlist: %s    fitness: %g' 
              % (i, bitlist, fitness))
    avg_fit, top_fit = avg_and_top_fitness(pop)
    print("GEN_REPORT:", gen_no, avg_fit, top_fit)
def calc_fitness(member):
    """calculate the fitness of an individual, a list of bits where the
    fitness is the sum of the bits"""
    fitness=0
    for i in range(len(member)):
        fitness += member[i]
    return fitness
def avg_and_top_fitness(pop):
    top_fitness=-1
    avg_fitness=0
    sum_fitness=0
    for member in pop:
        fit=calc_fitness(member)
        sum_fitness+=fit
        if fit>top_fitness:
            top_fitness=fit
    return sum_fitness/len(pop),top_fitness
    
def evolve(pop, n_gen):
    """Evolve a population for n_gen generations"""
    for i in range(n_gen):
        parents=select_parents(pop)
        children=breed(parents,len(pop))
        pop=parents+children
        print('==== at generation %d ====' %i)
        display_pop(pop, i+1)

def breed(parents, n_offspring):
    """Breed the parents to get a collection of offspring; apply mutation
    as part of the breeding process"""
    children=parents+parents+parents+parents
    assert(len(parents) + len(children) == n_offspring) # sanity check
    children=mutate_pop(children)
    return children

def select_parents(pop):
    """Select a part of the population to be the parents.  At this time we
    only use a top-20% approach; in the future it should be
    selectable

    """
    assert(len(pop)%5==0)
    parents=select_20_pct(pop)
    return parents

def select_20_pct(pop):
    """Select the top 20% of the population"""
    assert(len(pop)%5==0)
    fitnesses=[]
    for member in pop:
        fitness=calc_fitness(member)
        fitnesses.append((member, fitness))
    fitnesses.sort(key=lambda item: item[1])
    fitnesses[-len(pop)//5:]
    # print(fitnesses[-len(pop)//5:])
    result_top  = []
    for (member, fitness) in fitnesses[-len(pop)//5:]:
        result_top.append(member)
    return result_top

def mutate_pop(children):
    """Mutate an entire population, returning the result"""
    for child_no, child in enumerate(children):
        new_child = mutate_individual(child, prob_mutation)
        # print("=============       ", child, calc_fitness(child), "---------->", new_child, calc_fitness(new_child))
        children[child_no] = new_child
    return children


def mutate_individual(old_member, prob):
    """Takes a member of the population, applies mutation to each bit with
    probability prob, and returns the result

    """
    member = old_member[:]      # make a copy of the original
    # print('OLD:', member, end="")
    for i in range(len(member)):
        if random.random() > prob: # roll the die
            continue            # don't mutate this bit
        # print('{ROLL[%d], %d -> ' % (i, member[i]), end="")
        if member[i] == 1:
            member[i] = 0
        else:
            member[i] = 1
        # print(member[i], member, "}", end="")
    # print(' -> NEW:', member)
    return member

main()

