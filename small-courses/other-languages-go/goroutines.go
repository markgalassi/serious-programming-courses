package main

import (
	"fmt"
	"time"
)

func hello() {
	fmt.Println("Hello World")
}

func listnumbers() {
	for i := 0; i < 10; i++ {
		fmt.Println(i)
	}
}

func squareroot(x int) {
	if x == 2 {
		fmt.Println("Oh no, a 2")
	} else {
		fmt.Println("good choice")
	}
}

func main() {
	//run all three goroutines
	go hello()
	go listnumbers()
	go squareroot(3)
	//wait for the goroutines to finish
	time.Sleep(2 * time.Second)
	fmt.Println("All finished")
}
