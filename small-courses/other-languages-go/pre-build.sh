#! /bin/sh

echo "running: $0"
echo "args: $*"

go build euler-method.go
./euler-method 1000 > euler-method.dat
