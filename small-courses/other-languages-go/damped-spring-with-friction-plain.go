package main

import (
	"fmt"
	"math"
)

// acceleration due to gravity
var g = 9.8
// physics values for spring equation
var k = 3.0
var m = 2.0
// physics for air friction
var air_friction = 3
// physics for frictional damping force
var damp = 0.5
var F0 = 10.0			// constant driving force


func Acceleration (t float64, v float64, x float64) float64  {
	return ((-k*x) - (damp*v) + (F0*math.Cos(8.0*t))) / m
}

func Harmonic_exact (t float64, st float64, sx float64, sv float64) float64 {
	//return math.Cos(t)
	return sx*math.Cos(math.Sqrt(k/m - damp*damp/(4*m*m))*t)*math.Exp((-damp/(2*m)) * t)
}

func Falling_body_exact (t float64, st float64, sx float64, sv float64) float64 {
	return sx + sv*t - 0.5*g*math.Pow(t,2)
}

func Damped_spring () {
	//set variables for falling_body_exact()
	var set_t float64 = 0
	var set_x float64 = 5   /* try 10 for falling body, 5 for harmonic */
	var set_v float64 = 0	/* try 10 for falling body, 0 for harmonic */
	//initial variables
	var interval float64 = 0.001 // how many seconds
	var n_steps float64 = 1000000
	var dt = interval/n_steps // size of steps
	var ti = set_t
	var xi = set_x
	var vi = set_v
	for j := 0; j < int(n_steps); j++ {
		var exact float64 = Harmonic_exact(ti, set_t, set_x, set_v)
		var acc float64 = Acceleration(ti, vi, xi)
		var new_v = vi + (dt * acc)
		var new_x = xi + (dt * vi)
		// increase t by dt
		var new_t = ti + dt
		
		vi = new_v
		xi = new_x
		ti = new_t
		fmt.Println(ti, vi, xi, exact);
	}
}

func main () {
	Damped_spring();
}
