//channels-example.go
package main

import (
	"fmt"
	"time"
)


func pinger (c chan string) {
	for i := 0; ; i++ {
		//send "ping" to the channel
		c <- "ping"
	}
}
//channel gets "ping" and waits until printer() is ready to receive "ping"

func printer (c chan string) {
	for {
		//receive "ping" from channel and assign it to msg
		msg := <- c
		fmt.Println(msg)
		time.Sleep(time.Second)
	}
}

func main() {
	//makes a channel called c
	//strings are passed on it
	var c chan string = make(chan string)

	go pinger(c)
	go printer(c)

	//input receives data from the channel c
	//input := <- c
	//will print ping
	//fmt.Println(input)

	var input string
	//scans the input from c channel and prints it out
	//does not stop until you exit the program running
	fmt.Scanln(&input)
}
