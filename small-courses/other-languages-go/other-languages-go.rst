.. _chap-other-languages-go:

======================
 Other languages - Go
======================

.. sectionauthor:: Sophia Mulholland <smulholland505gmail.com>

https://golang.org/dl/

Hello World in Go
=================

Go is similar in syntax to C, yet simpler in many ways. C definitely has
more structure and syntax rules that Go does not have, which makes Go
simpler and easier to learn. Writing a short program in Go looks like a
combination of C and Python. Syntax like this allows you to get
comfortable with the language quickly and move on to working on harder
things.

First, we will get comfortable with running a Go program. In an editor
such as emacs, open up a new file called “hello.go”.

The first program to write in Go is a simple "Hello World" which is
shown below:

.. _hello-go:

.. literalinclude:: hello.go
      :language: go
      :caption: 'hello.go' 

**Notice:**
Every Go program should have a ‘\ **package main**\ ’ defined at the top
of the code, with the majority of programs using that package. The line
'**import "fmt"**' is basically like writing '#include <stdio.h>' in C.
It imports the package 'fmt' that allows you to print things. The last
thing to notice is that Go does not have any semicolons.

Compile and run this code with

::
 
   $ go build hello.go
   $ ./hello
   $ Hello, World!

OR optionally the Go compiler supports a ‘go run’ command which will
just compile and run it in the same step:

::
   
   $ go run hello.go
   $ Hello, World!

Writing a Go program with command line arguments
================================================

Please see the chapter on differential equations for the context to this
program. It is meant to graph the output of using the euler method to
solve equations and compare it with the exact answer.

Let's write another simple program to get familiar with the syntax of
the language called ‘euler-method.go’.

.. _euler-method-go:

.. literalinclude:: euler-method.go
   :language: go
   :caption: 'euler-method.go' solves differential equation using Euler's method

As we can see in the code, Go has a different way of declaring variables
and handling command line arguments. In Go, handling command line
arguments is simpler as you can import the package “os” and not have to
deal with the argc and \*argv[] or understanding pointers yet. By just
importing “os” you can deal with command line inputs as an array and can
check the length of that array rather than dealing with the number argc.

To deal with converting this array of string inputs into integers, we
can import “strconv” which converts the strings into ints. However, this
strconv function converts it into a number plus a <nil> at the end so
avoid this compiler error when we try to declare n\_steps, we can
declare the variable n\_steps along with the blank identifier ‘\_’ to
avoid having to declare all the return values (<nil>).

::

   $ go build euler-method.go
   $ ./euler-method 1000 > euler-method.dat

   gnuplot> plot "euler-method.dat" using 1:2 with lines
   gnuplot> replot "euler-method.dat" using 1:3 with lines

.. _fig-euler-method-go.svg:

.. figure:: euler-method.*
   :width: 100%
	    
   
Goroutines and channels
=======================

A cool thing that Go does well is run **goroutines**. These are
functions that can run concurrently with other functions.
**Concurrency** means that the functions are running at the same time
rather than waiting for one to finish and then the next program starts.
Basically it is used when we need to handle multiple things at the same
time. Go makes these kinds of programs simple to write and easier to
understand because concurrency is generally difficult to understand in
computing. Also, these goroutines do not take up a lot of memory and so
they are very fast.

Threads are most commonly used to run things concurrently. All processes
have at least one main thread that executes a task. Threads take a fixed
amount of memory in the stack to create and when many threads are used,
switching between them gets messy.

However, using goroutines is like a step above threads. The programmer
deals with them while the computer does not even know goroutines exist.
They take little memory to create, 2kB, which means you can have
millions of them running on one CPU. They can also shift in size, to
accommodate the stack. Go also deals with the switching of goroutines
for the programmer, pausing and running. This is an advantage to
goroutines because the programmer does not need to say ahead of time
when to stop and start the threads.

Let’s see how to implement these goroutines.

.. _goroutines-hello-go:

.. literalinclude:: goroutines-hello.go
   :language: go
   :caption: 'goroutines-hello.go'

We have a function called hello, which looks just like a normal
declaration of a function and what makes it a goroutine is putting ‘go’
in front of the function call. So let’s run it.

::

   $ go build goroutines-hello.go
   $ ./goroutines-hello

The output is

::

   $ main function

That’s weird. We called the function hello(), and it did not print
“Hello world” like we wanted. That is because by making it a goroutine,
the compiler did NOT wait for it to finish its task of printing “Hello
world” and it went on with the rest of the main function. When the main
function finishes, the program is over no matter what, and hello() did
not have enough time to finish.

Let’s write a program that deals with more goroutines and introduce a
way to make sure the program waits for the goroutines to finish.

.. _goroutines-go:

.. literalinclude:: goroutines.go
   :language: go
   :caption: 'goroutines.go'

::

   $ go build goroutines.go && ./goroutines

If we ran it in the exact same way, the goroutines would not have time
to finish so let’s help it do everything it’s supposed to. Notice how we
added a line for the computer to “sleep” as the goroutines finished and
it we run it, it prints out everything we wanted it to. If you run it
multiple times you’ll find the order of the outputs changes, as
depending on which goroutine is running on which core, it will finish
first.

So these goroutines are cleaner and easier to implement than in C where
you need pointers and lots of lines of code where it can get messy
quickly.

So, in these programs we have no way of knowing when the goroutines end.
Therefore, we have to make the program wait for two seconds to be sure
it does. This is where **channels** come in.

A way goroutines can communicate with each other is through channels.
Channels can send and receive data of one type. If a goroutine sends
something to a channel, it automatically waits until a goroutine can
receive that data. It will be blocked until the goroutine requests that
data and vice versa.

.. _channels-example-go:

.. literalinclude:: channels-example.go
   :language: go
   :caption: 'channels-example.go'

::

   $ go build channels-example.go && ./channels-example
   $ ping

So this program created the channel c, and ran two goroutines (pinger()
and printer()). Pinger() sends “ping” to the channel and Printer()
prints out the message it received from the channel. In the main
function, we also show that the variable ‘input’ can receive data from
the channel too and print it out.

So goroutines allow functions to be run concurrently and channels are
how these goroutines communicate. Go offers an easy way to implement
them and it is

More complicated Go program
===========================

Lastly, let’s take a look at a more complicated program from the
differential equations chapter rewritten in Go.

.. _damped-spring-with-friction-plain-go:

.. literalinclude:: damped-spring-with-friction-plain.go
   :language: go
   :caption: 'damped-spring-with-friction-plain.go' 

So this program outputs a solution to a differential equation solved by
Euler’s method. The length is pretty much the same as the C program
however you can see there are no double types in Go. Instead, it offers
the equivalent float64 type which indicates it requires 64 bits in
memory.

::

   $ go build damped-spring-with-friction-plain.go
   $ ./damped-spring-with-friction-tut > damped-spring.dat
   gnuplot> plot “damped-spring.dat” using 1:3 with lines
   gnuplot> replot “damped-spring.dat” using 1:4 with line
   
This output is the same as the chapter on differential equations, go
to that chapter for an explanation on what it means. That chapter has the same program as this damped spring one, except it is written in C.

   
SOURCES

`*https://golang.org/cmd/go/* <https://golang.org/cmd/go/>`__

`*https://github.com/vladimirvivien/go-cshared-examples* <https://github.com/vladimirvivien/go-cshared-examples>`__

`*https://golangbot.com/hello-world/* <https://golangbot.com/hello-world/>`__

`*https://medium.com/rungo/achieving-concurrency-in-go-3f84cbf870ca* <https://medium.com/rungo/achieving-concurrency-in-go-3f84cbf870ca>`__

`*https://www.sohamkamani.com/blog/2017/08/24/golang-channels-explained/* <https://www.sohamkamani.com/blog/2017/08/24/golang-channels-explained/>`__


