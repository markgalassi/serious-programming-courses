##REQUIRED_FILE: euler-method.dat
set xlabel 'x axis'
set ylabel 'y axis'
# set multiplot layout 3,1 title 'Three ways of using "plot"'
plot 'euler-method.dat' using 1:2 with lines, 'euler-method.dat' using 1:3 with lines
