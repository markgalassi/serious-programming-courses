package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
)

func differential (x float64, y float64) float64 {
	return (2*x)-2
}

func solution (sx float64, sy float64) float64 {
	return math.Pow(sx, 2) - (2*sx)
}

func main() {
	var n_steps int = 1
	if len(os.Args) == 1 {
		n_steps = 1000
	} else if len(os.Args) == 2 {
		n_steps, _ = strconv.Atoi(os.Args[1])
	} else {
		fmt.Println("error usage is %s [n_steps]\n", os.Args[0])
		os.Exit(1)
	}
	var set_y float64 = 0
	// define initial x, initial y, step size, and # of steps
	var interval float64 = 10
	var dt float64 = interval/float64(n_steps)
	var xi float64 = 0
	var yi float64 = set_y
	for j := 0; j < int(n_steps); j++ {
		//find exact solution to compare approximation with	
		var exact float64 = solution(xi, yi)
		//solve differential with (xi, yi)
		var m float64 = differential(xi, yi)
		//use tangent line to approximate next y value from previous y value
		var new_y float64 = yi + dt * m
		//increase x by step size
		var new_x float64 = xi + dt
		xi = new_x
		yi = new_y
		fmt.Println(xi, yi, exact)
	}
}
