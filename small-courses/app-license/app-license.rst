===========================
 Copying and legal matters
===========================


Copyright
=========

    Copyright (C)  2017-2022  Mark Galassi, Leina Gries, Sophia
    Mulholland, Almond Heil.

License for the book
====================

    This work is licensed under the Creative Commons
    Attribution-ShareAlike 4.0 International License. To view a copy
    of this license, visit
    http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to
    Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

We reproduce the full text of the CC BY-SA license below.

License for code samples
========================

All the software code samples are free software, and can be freely
redistributed under the GNU General Public License (GPL), version 3.

We reproduce the full text of the GPL below.


CC BY-SA 4.0 license
====================

.. literalinclude:: cc-by-sa-4.0.txt


GNU General Public License
==========================

.. literalinclude:: gpl-3.0.txt
