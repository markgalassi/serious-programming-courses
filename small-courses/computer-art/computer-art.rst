.. _chap-computer-art:

==============
 Computer art
==============

[status: content-partly-written]

Sadly the fractal flame screensaver ElectricSheep was made proprietary
several years ago, so it cannot grace our computer monitors anymore.
But there is much good computer art that can be generated using free
software tools.

Here are some references to explore to see if it is possible to
develop a chapter for this book on computer art.  At this time these
references are focused on evolutionary art, but other possibilities
should be examined.  Connections to 1d and 2d (like Conway's life)
cellular automata should be explored.

Preqrequisites:

::

   sudo apt install -y geeqie imagemagick
   sudo apt install -y gimp

Understanding photos and images
===============================

Discussion of graphics formats
------------------------------

Show a PDF and a .png or .jpg and try to zoom in on them.  Talk about
vector formats vs. raster formats.

Photo collection management
---------------------------

Introduce shotwell and digikam.  Urge students to take pictures with a
camera or phone, but bring a cheap camera to have students take some
80 photos at the start of class, then download them to the computer
with a USB cable.

Here is a file that I have put up:

::

   # prepare a space under ~/Pictures for stock photos
   cd ~/Pictures
   mkdir stock
   cd stock
   # get a couple of sets of images
   wget https://www.galassi.org/mark/.tmp/for-courses/eric-kim-stock-photos.zip
   unzip eric-kim-stock-photos.zip

And one from NASA:

::

   # prepare a space under ~/Pictures for other NASA photos
   cd ~/Pictures
   mkdir nasa
   cd nasa
   wget https://solarsystem.nasa.gov/system/downloadable_items/1304_JPG.zip
   unzip -d NASA_1304 1304_JPG.zip 

This should be enough to get started.  But we can get more from the
https://creativity103.com/ web site, using a tip on recursive wget
from stackoveflow:

https://stackoverflow.com/questions/4602153/how-do-i-use-wget-to-download-all-images-into-a-single-folder-from-a-url

..
   ::

      mkdir -p ~/Pictures/stock/creativity103/AbstractLight
      cd ~/Pictures/stock/creativity103/AbstractLight
      wget -nd -r -P . -A jpeg,JPEG,jpg,JPG,bmp,gif,png https://creativity103.com/collections/AbstractLight/index.html
      mkdir -p ~/Pictures/stock/creativity103/Aviation
      cd ~/Pictures/stock/creativity103/Aviation
      wget -nd -r -P . -A jpeg,JPEG,jpg,JPG,bmp,gif,png https://creativity103.com/collections/Aviation/index.html
      # or to get the full package 
      mkdir -p ~/Pictures/stock/creativity103
      cd ~/Pictures/stock/creativity103
      wget -nd -r -P . -A jpeg,JPEG,jpg,JPG,bmp,gif,png https://creativity103.com/

..
   Emphasize the importance of tagging and rating photos immediately.


Image manipulation: command line and GUI
----------------------------------------

.. _introduce_imagemagick:

Introduce ImageMagick
.....................

Work through some of the filters shown in the ImageMagick cookbook at:

https://www.imagemagick.org/Usage/transform/#art

Start with everyone taking their favorite image.  They should name it
``myimage-original.jpg``

If you don't have a favorite image handy then you can download NASA's
"Pillars of Creation":

::

   wget https://upload.wikimedia.org/wikipedia/commons/6/68/Pillars_of_creation_2014_HST_WFC3-UVIS_full-res_denoised.jpg
   mv Pillars_of_creation_2014_HST_WFC3-UVIS_full-res_denoised.jpg myimage-original.jpg

Then give it a workable size so things dont' take too long for the
purpose of this class.  I recommend it be approximately 1500x700
pixels in size so that it's not too slow to process, but if students
want to use a photo with a modern phone camera (which have many more
pixels than that) they can do so and then reduce them with something
like:

.. code:: bash

   convert -resize 1500x myimage-original.jpg myimage.jpg

So from here on let's call our picture myimage.jpg, so rename
your picture to that filename.

Next we see how the pixel spread works:

::

   convert myimage.jpg -spread 5 myimage-spread-005.jpg
   convert myimage.jpg -rotate 5 myimage-rotate-005.jpg

Now we'll create a sequence of files with different pixel spreads
using a quick inline shell script:

.. code-block:: bash

   for i in `seq -w 0 100`
   do
       echo $i
       convert myimage.jpg -spread $i myimage-spread${i}.jpg
   done

Now make a film with:

.. code-block:: bash

   ffmpeg -framerate 3 -i myimage-spread%03d.jpg spread-movie.mp4

and view it with

.. code-block:: bash

   totem spread-movie.mp4 &


.. code-block:: bash

   for i in `seq -w 0 360`
   do
       echo $i
       convert myimage.jpg -rotate $i myimage-rotate${i}.jpg
   done

   ffmpeg -framerate 20 -i myimage-rotate%03d.jpg rotate-movie-fr20.mp4


Introduce the Gimp
..................

Everyone should know how to start and use the GIMP.  Here is a
collection of 40 tutorials:

https://www.noupe.com/inspiration/tutorials-inspiration/best-of-gimp-40-professional-tutorials-to-level-up-your-skills-79428.html

We pick one of them and work through part of it with the students.



metapixel and photomosaics
==========================

* Collect a bunch of photos in a hurry, possibly with some cool use of
  wget and an image search.  For example, creativity103.com has
  archives of images licensed under the "creative commons
  "attribution" license: https://creativecommons.org/licenses/by/3.0/

* I already showed how to beef up your Pictures folder with commands
  like ``wget -r -np -nc https://creativity103.com/``

* Install metapixel on Linux

* the metapixel tutorial I have found has pointless racy images, so I
  need to find another one.

* install the webcollage screensaver:
  ``sudo apt install xscreensaver-screensaver-webcollage``

* we have beefed up our personal ~/Pictures directory so we can use
  that to make the photomosaic database

.. code:: bash

   mkdir ~/mp
   metapixel-prepare -r ~/Pictures ~/mp --width 32 --height 32

once the database is ready we can make our metapixel photo with:

.. code:: bash

   metapixel --metapixel --library ~/mp --scale=2 myimage.jpg myimage-mp2.jpg
   # and a scaled-by-4 version with:
   metapixel --metapixel --library ~/mp --scale=4 myimage.jpg myimage-mp4.jpg

Now use geeqie with its "control+scrollwheel" zooming feature to zoom
in on the individual tiles in the photomosaic.


ASCII art
=========

What is ASCII?  Explain encoding of characters, and show ``man
ascii``.

What is ASCII art?

Tools for drawing ASCII art.

Fun and amusing tools.  Some of these can be found at
https://www.binarytides.com/linux-fun-commands/

::

   echo an example of figlet | figlet
   banner "have a nice day"
   cowsay hey dude
   cowsay -f dragon "Run for cover, I feel a sneeze coming on."
   cowsay -l
   cowsay -f ghostbusters Who you Gonna Call
   sl

The grand old UNIX *fortune* command is still available, an dyou can
pipe it to others:

::

   fortune -s     # repeat a few times
   fortune -s | cowsay

``cmatrix`` uses the UNIX *curses* library to draw a matrix animation on
your terminal.

Converting raster images to ASCII art.  ``jp2a`` on GNU/Linux systems:
let us take pictures of famous computer scientists Margaret Hamilton
and Dennis Ritchie:

::

   wget https://upload.wikimedia.org/wikipedia/commons/6/6f/Margaret_Hamilton_-_restoration.png
   wget https://upload.wikimedia.org/wikipedia/commons/2/23/Dennis_Ritchie_2011.jpg


   ## make your terminal very big and try
   jp2a -f Margaret_Hamilton_-_restoration.png
   jp2a -f --color Margaret_Hamilton_-_restoration.png
   jp2a -f Dennis_Ritchie_2011.jpg
   jp2a -f --color Dennis_Ritchie_2011.jpg

To see jp2a work better, make your terminal have really tiny fonts (in
many terminal programs you can do this with :kbd:`ctl+-`), then resize
the window to be really big, and then run the jp2a commands again.
You might be astonished at the results.

emacs has an ASCII art drawing mode.  It might be worth exploring.

Joyce Levine has pointed me to tilde town: https://tilde.town/ which
might be worth exploring.

There was a famous ascii art movie of Star Wars that one could reach
with

::

   telnet towel.blinkenlights.nl

but it has gone offline recently.  You can find videos of it on
youtube.  A real tour-de-force.  According to this thread on reddit it
has been taken down:

https://www.reddit.com/r/sysadmin/comments/pbv7xm/is_towelblinkenlightsnl_dead/

but you can get it (without the fancy ascii colors) by typing a telnet
command followed by typing starwars:

::

   telnet telehack.com
   starwars

A discussion of the ascii animation used there can be found at:

https://www.asciimation.co.nz/index.php

https://www.asciimation.co.nz/asciimation/ascii_faq.html

A few more things to type:

::

   sl
   fortune
   factor 12103  # factoring numbers? can we use this to search for Mersenne primes?
   factor `echo "2^7-1" | bc` ; factor `echo "2^11-1" | bc` ; factor `echo "2^13-1" | bc`
   pi 50
   espeak "Hello Linux, where are the penguins"
   aafire -driver curses
   




Evolutionary art
================

* https://en.wikipedia.org/wiki/Electric_Sheep

* https://en.wikipedia.org/wiki/Software_art

* https://en.wikipedia.org/wiki/Evolutionary_art

* https://iasl.uni-muenchen.de/links/GCA-IV.3e.html

* transcend https://transcend.sourceforge.net/


Image manipulation from your own Python program
===============================================

We will learn to use Pillow, the python imaging library (PIL).

At the shell:

.. code:: bash

   sudo apt install python3-pil

Also give yourself a photo to work with.  For this tutorial let's keep
it small so it's faster and views better.  You can use a command like:

.. code:: bash

   convert -resize 800 my_big_picture.jpg myimage.jpg


In the python interpreter, following the tutorial at
https://pillow.readthedocs.io/en/stable/handbook/tutorial.html

.. code:: python

   ## import the Image portion of the PIL library
   from PIL import Image
   ## load an image
   im = Image.open('myimage.jpg')
   ## show its information
   print(im.format, im.size, im.mode)
   ## show the picture itself
   im.show()
   ## then close that window

We now have an image stored in the object ``im`` and we can work on
manipulating it.

Geometric transformations
-------------------------

.. code:: python

   import os, sys
   from PIL import Image

   size = 128, 128

   infile = 'myimage.jpg'
   outfile = os.path.splitext(infile)[0] + '.thumbnail' + '.jpg'
   print(outfile)
   thumb = Image.open(infile)
   thumb.thumbnail(size)
   thumb.save(outfile, 'JPEG')

.. code:: python

   ## copy a rectangle from the image
   box = (300, 300, 600, 600)
   region = im.crop(box)

   ## now play with that rectangle, then paste it back in
   region = region.transpose(Image.ROTATE_90)
   ## now paste it back in
   im.paste(region, box)
   ## now box is the image with the small region we chose
   im.show()
   im.save('myimage_pasted.jpg', 'JPEG')

You will now see that the region we cut out of the photo has been
rotated 90 degrees.

.. code:: python

   ## reload the image from scratch
   im = Image.open('myimage.jpg')
   ## rotate 45 degrees counterclockwise
   rotated = im.rotate(45)
   rotated.save('myimage_rotated_45.jpg', 'JPEG')
   ## now make an animation
   various_rotations = []
   for i in range(360):
       rotated = im.rotate(i)
       fname = 'myimage_rotated_%03d.jpg' % i
       print('writing out %s' % fname)
       rotated.save(fname, 'JPEG')

If you type ``ls`` you will see that you now have many files with
names like ``myimage_rotated_17.jpg``.  You can use ``geeqie`` to
view them and even get a crude animation by holding the space bar
down.  You can also use what we learned in
:numref:`sec-making-a-movie-animation-of-the-walk` and try this:

.. code:: bash

   ## make a movie with:
   ffmpeg -framerate 24 -i myimage_rotated_%03d.jpg myimage_animated.mp4
   ## view it with:
   vlc myimage_animated.mp4 &

Filters and enhancement
-----------------------

.. code::

   from PIL import Image
   from PIL import ImageFilter
   ## load an image
   im = Image.open('myimage.jpg')
   ## out = im.filter(ImageFilter.DETAIL)
   ## out.save('myimage_detail.jpg', 'JPEG')
   out = im.filter(ImageFilter.CONTOUR)
   out.save('myimage_contour.jpg', 'JPEG')
   out = im.filter(ImageFilter.EMBOSS)
   out.save('myimage_emboss.jpg', 'JPEG')
   out = im.filter(ImageFilter.FIND_EDGES)
   out.save('myimage_edges.jpg', 'JPEG')
   out = im.filter(ImageFilter.SMOOTH)
   out.save('myimage_smooth.jpg', 'JPEG')
   out = im.filter(ImageFilter.BLUR)
   out.save('myimage_blur.jpg', 'JPEG')

   ## NOTE: in the next one we start from "out", the smoothed photo
   restored = out.filter(ImageFilter.EDGE_ENHANCE)
   restored.save('myimage_restored.jpg', 'JPEG')

   out = im.filter(ImageFilter.SMOOTH_MORE)
   out.save('myimage_smooth_more.jpg', 'JPEG')

Now compare all the images we produced.  In particular, look carefully
at ``myimage_smooth.jpg`` and ``myimage_restored.jp`` to see how
the EDGE_ENHANCE transformation takes away the fuzziness that had been
introduced by SMOOTH.

Topics for further study
========================

Another Pillow tutorial which starts slowly but gets into interesting examples:

https://www.tutorialspoint.com/python_pillow/python_pillow_quick_guide.htm

https://realpython.com/fingerprinting-images-for-near-duplicate-detection/

https://www.imagemagick.org/script/compare.php

https://askubuntu.com/questions/209517/does-diff-exist-for-images

https://jeremykun.com/2012/01/01/random-psychedelic-art/

Take one of those classic old "over the top" movie transition effects,
or a modern parody of one, like this one from Spinal Tap:
https://youtu.be/QrJlyapt6OY?t=114

Try to take an image and create your own over the top transition using
Pillow.
