#! /usr/bin/env python3

import PySimpleGUI as sg

# layout = [[sg.Text("Hello from PySimpleGUI")], [sg.Button("OK")]]
layout = [[sg.Button("OK")]]

# Create the window
window = sg.Window("OK button", layout)

# Create an event loop
while True:
    event, values = window.read()
    # End program if user closes window
    if event == sg.WIN_CLOSED:
        break

window.close()
