#! /usr/bin/env python3

import sys
import os
import tkinter as tk

import tic_tac_toe as ttt

class TTTGui(tk.Frame):
    def __init__(self, parent, _xHuman, _yHuman):
        """Draw the initial board"""
        tk.Frame.__init__(self, parent)
        ## set up some constant variables that last across games
        self.imBlank = tk.PhotoImage(file='green_background.png')
        self.imX = tk.PhotoImage(file='green_back_X.png')
        self.imO = tk.PhotoImage(file='green_back_O.png')
        self.marker2image = {' ': self.imBlank,
                             'x': self.imX,
                             'o': self.imO}
        ## zero out 
        self.ResetGame(_xHuman=_xHuman, _yHuman=_yHuman)
        ngB = tk.Button(self, text='New game', command=lambda:
                        self.ResetGame(_xHuman=_xHuman, _yHuman=_yHuman))
        ngB.grid(row=4, column=0)
        qB = tk.Button(self, text='Quit', command=self.Quit)
        qB.grid(row=4, column=1)
        self.grid(sticky=tk.N+tk.S+tk.E+tk.W)

    def ResetGame(self, _xHuman=True, _yHuman=False):
        ## rest the game state variables
        self.bd = ttt.new_board()
        self.gameState = {'xHuman': _xHuman,
                          'yHuman': _yHuman,
                          'toMove': 'x',
                          'winner': ttt.find_winner(self.bd)}
        ## set up the board buttons from scratch
        self.buttons = [[None, None, None],
                        [None, None, None],
                        [None, None, None]]
        for row in range(3):
            for col in range(3):
                self.buttons[row][col] = tk.Button(self, image=self.imBlank)
                self.buttons[row][col].grid(row=row, column=col)
                ## we bind the button to the PlaceMarker() method,
                ## making sure to pass it the row and column
                self.buttons[row][col].bind('<Button-1>',
                                            lambda event, row=row, col=col:
                                            self.PlaceMarker(row, col))
        self.UpdateBoard()

    def PlaceMarker(self, row, col):
        if self.gameState['winner'] != ' ':
            print('GAME_IS_WON, not placing marker')
            return
        if ttt.board_is_full(self.bd):
            print('BOARD_IS_FULL, not placing marker')
            return
        if self.bd[row][col] == ' ':
            print('PLACE: %d, %d' % (row, col))
            ttt.set_cell(self.bd, row, col, self.gameState['toMove'])
            self.HandlePossibleEnd()
            self.UpdateBoard()
            self.gameState['toMove'] \
                = ttt.next_marker(self.gameState['toMove'])
            if self.gameState['winner'] == ' ':
                if (not self.gameState['xHuman']
                    or not self.gameState['yHuman']):
                    if not ttt.board_is_full(self.bd):
                        self.TriggerComputerMove()
        else:
            print('ILLEGAL: %d, %d' % (row, col))
            pass                # invalid move
        self.HandlePossibleEnd()

    def UpdateBoard(self):
        for row in range(3):
            for col in range(3):
                image = self.marker2image[self.bd[row][col]]
                self.buttons[row][col].configure(image=image)
        self.HandlePossibleEnd()
    
    def TriggerComputerMove(self):
        ttt.play_computer_opportunistic(self.bd, self.gameState['toMove'])
        self.gameState['toMove'] = ttt.next_marker(self.gameState['toMove'])
        self.UpdateBoard()
        self.HandlePossibleEnd()

    def HandlePossibleEnd(self):
        self.gameState['winner'] = ttt.find_winner(self.bd)
        print('WINNER: <%s>' % self.gameState['winner'])
        if ttt.board_is_full(self.bd):
            print('I *should* put up some info')
            print('WINNER: <%s>' % self.gameState['winner'])

    def Quit(self):
        self.master.destroy()

def main():
    app = TTTGui(tk.Tk(), True, False)
    app.mainloop()

if __name__ == '__main__':
    main()
