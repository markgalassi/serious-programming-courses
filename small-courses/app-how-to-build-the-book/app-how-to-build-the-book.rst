.. _app-how-to-build-the-book:

================================
 Appendix: How to build the book
================================

Motivation, prerequisites, plan
===============================

Our goal here is to show you how to build the book from its raw text
into the HTML and other formats in which the book is published.

We use the Sphinx typesetting system bla bla python docs use Sphinx
bla bla FIXME

Prerequisites:

* Installing the tools needed to clone and build the book.

* Learning how to use the Sphinx documentation system.

The tools needed
================

We need tools to get the book from the network repository (a git
repository, hosted on codeberg.org), and to build the book (using the
sphinx documentation system).

.. code:: console

   $ sudo apt install -y git make wget
   $ sudo apt install -y gnuplot-qt
   $ sudo apt install -y ffmpeg
   $ sudo apt install -y python3-pip
   ## we also need to run the examples that make plots for the books;
   ## for that we need scipy and matplotlib
   $ python3 -m pip install --user --upgrade matplotlib numpy scipy
   ## install the LaTeX packages needed to build the printable book
   ## with "make latexpdf"
   $ sudo apt install -y texlive-latex-base texlive-latex-recommended 
   $ sudo apt install -y texlive-fonts-recommended
   $ sudo apt install -y biber latexmk graphviz dot2tex
   $ sudo apt install -y librsvg2-bin pdf2svg
   $ python3 -m pip install --user --upgrade sphinx sphinxcontrib-bibtex sphinxcontrib-jsmath
   $ python3 -m pip install --user --upgrade sphinxcontrib-programoutput
   $ python3 -m pip install --user --upgrade sphinx-rtd-theme

   $ python3 -m pip install --user --upgrade sphinxcontrib.programoutput
   $ python3 -m pip install --user --upgrade sphinxcontrib.bibtex
   $ python3 -m pip install --user --upgrade recommonmark
   $ python3 -m pip install --user --upgrade sphinxcontrib.bibtex


Version control: cloning the repository (you only do this once)
===============================================================

We use git to access the book repository on the
``https://codeberg.org/`` version control site.

If you go to
https://codeberg.org/markgalassi/serious-programming-courses you will
find instructions to clone the repository with git.  They will look
something like:

.. code:: console

   ## cloning instruction if you have an account on codeberg:
   $ git clone git@codeberg.org:markgalassi/serious-programming-courses.git
   ## cloning instruction if you are coming in anonymously
   $ git clone https://codeberg.org/markgalassi/serious-programming-courses.git

The first link will work for anyone, but then you cannot push changes
back.  The second is for collaborators who are modifying the book.

You can then change in to the directory into which you have cloned the
repository, and examine what you have, with:

.. code:: console

   $ cd serious-programming-courses
   $ ls
   $ cd small-courses
   $ ls
   $ find . -name '*.rst'

That last ``find`` command will show you all the files that end in
``.rst``.  These files contain the text of the book written as simple
text with the light-weight *markdown* annotations.  Examine the files.


Building the book
=================

The sphinx documentation system will build the book in either HTML or
EPUB format.  The details are all taken care of and you can do so by
typing:

.. code:: console

   $ cd serious-programming-courses/small-courses
   $ make html           ## (or "make epub")

You can then point your browser to the file ``_build/html/index.html``
to see what it all looks like.


Making and committing changes (your day-to-day)
===============================================

You can make changes to the ``.rst`` files.  When you do so you need
to re-run ``make html`` and then view the changes by reloading the
HTML files in your browser.

First you should incorporate changes made by other people.  You type:

.. code:: console

   $ git pull

Then you can make some changes to files, after which you can *commit*
them to the git repository with

.. code:: console

   $ git commit -a

This commit will open an editor so that you can put in a log message.
This message should be thoughtful and describe briefly what changes
you made to each file you modified.

If you are a collaborator on the book and have write permission on
sourceforge you can now push your changes for other collaborators to
see with:

.. code:: console

   $ git push

(If you do not have write permission you can contact the authors to
get your changes to them.)

So the *workflow* is:

* Pull and update from sourceforge.

* Make changes to the book by modifying the ``.rst`` files.

* Commit the changes with ``git commit -a``

* If you have write permission on codeberg, push in the code with
  ``git push``
