.. _app-how-to-add-a-chapter:

================================
 Appendix: How to add a chapter
================================

..
   Motivation, prerequisites, plan
   ===============================

.. rubric:: Motivation

The purpose of this appendix is to show you how you might contribute a
chapter to this book.  You should pay attention to the *structure* of
how the topic is presented: what sections do we have at the beginning
and end of the chapter, how we introduce exercises, and then be able
to start writing about your topic.

.. rubric:: Prerequisites

* An idea you want to write about.

* Learning how to use the Sphinx documentation system to add to the
  book.  This is described in :numref:`app-how-to-build-the-book`.

.. rubric:: Plan

In keeping with our hands-on approach I will demonstrate the idea with
a sample chapter that is actually an interesting mini-course.

The topic I will use for this demonstration is the *quadratic
formula*.  This is an important topic: one of the few formulas that
you learn in middle school and then might go on using for the rest of
your life.


Anatomy of the chapter
======================

#. Motivation, prerequisites, plan

#. Simplest example of a calculation.

#. Simplest example of a plot.

#. A short Python program which generates or analyzes data related to
   the topic.

#. Applications to real-world situations, such as science or "life".

#. Exercises.  Exercises can be inserted at any point in the text.
   The exercises can also lead in to more advanced work.

#. Further study.  References and ideas to learn more about the topic
   than can be done in the 1.5 hour mini-course.

I will now show what the sample chapter looks like, prefixing every
section title with "The chapter:" and putting notes in square brackets
explaining the "pedagogical" motivation for some of what I put into
the chapter.

*NOTE:* from here on we show the mock chapter.

The chapter: Title
==================

Title: The quadratic formula


The chapter: frontmatter
========================

.. rubric:: Motivation

Our goal here is to learn to find *roots of second degree
polynomials*.  What does this mean?  If we have a second degree
polynomial, which looks like :math:`ax^2 + b x + c`, we look for
values of x that give the result :math:`ax^2 + bx + c = 0`.

We will work toward the *quadratic formula*.  This, like the
*Pythagoras theorem*, is one of the few formulas which many people
continue to use in life after they leave school.  This is because
second degree polynomials come up in many areas of science, even areas
so simple that we might deal with them in life.

Our examples will involve:

physics
   figuring out how long it takes a ball that you drop to hit the
   ground

geometry
   or what is the biggest area rectangle you can draw with a fixed 
   perimeter.

.. rubric:: Prerequisites

* The 10-hour "serious programming" course.

* The "Data files and first plots" mini-course in
  :numref:`chap-data-files-and-first-plots`

.. rubric:: Plan

We will start by introducing the equation to be solved, then we will
show the solution, after which we will look at some simple examples
and then some examples of scientific calculations we can make with the
quadratic formula.

For some very simple polynomials we will see that we can *guess* the
solution, but most of the time it will not be that easy, so we will
show:

* The quadratic formula, which finds the roots to second degree
  polynomials (:numref:`sec-the-quadratic-formula`).

* A *numerical approximation*.  We will write a computer program which
  scans through the real number line looking for zeros
  (:numref:`sec-numerical-approximation`).  This technique also
  applies to more complicated functions than the quadratic equation.


The chapter: The problem
========================

A basic equation
----------------

The problem is that, given numbers a, b and c, we want to find a value
of x for which:

.. math::
   :label: eq-quadratic-formula

   a x^2 + b x + c = 0

For example, if :math:`a=1` and :math:`b=2` and :math:`c=0` then we
have a simple case of Equation :eq:`eq-quadratic-formula`:

.. math::
   :label: eq-quadratic-simplified

   x^2 - 5x + 6 = 0

and a bit of guesswork tells us that both :math:`x=2` and :math:`x=3`
will make Equation :eq:`eq-quadratic-simplified` out:

Plugging :math:`x=3` into Equation :eq:`eq-quadratic-simplified` we
get:

.. math:: 3^2 - 5 \times 3 + 6 = 9 - 15 + 6 = 4 - 4 = 0

And plugging :math:`x=0` into Equation :eq:`eq-quadratic-simplified`
we get:

.. math:: 2^2 - 5 \times 2 + 6 = 4 - 10 + 6 = 0

so Equation :eq:`eq-quadratic-simplified` is satisfied when you plug 2
into x, and also when you plug 3 into x.

For this polynomial we were able to find the roots by guesswork.  As I
mentioned, most of the time we will not be able to guess the solutions.

Some terminology
----------------

Polynomial
   A function :math:`f(x)` which is a sum of terms with x
   at various powers.  For example :math:`f(x) = 7x^3 + 2x^2 - 4x + 2`

Second degree polynomial
   A polynomial where the highest power is :math:`x^2`.

Parabola
   The shape of the plot of a second degree polynomial.

Roots of a polynomial
   The values for x for which :math:`y = 0`.  These are the
   *solutions* to the equations we have been looking at.

Numerical approximation
   An approximate solution to a problem found by carrying out
   calculations that get you closer and closer to a solution.

Numerical analysis
   The academic discipline which resolves around finding numerical
   approximations to the solutions of mathematical problems.

But wait! Two solutions??
-------------------------

[Pedagogical goal: I want to toss in a mnemonic which has always
helped me in understanding solutions to equations.  A first order
equation has up to one solution; a second order equation has up to two
solutions, and so forth.]

You should find it notable that there are two solutions to this
equation.  You might have previously studied equations like :math:`7
x + 4 = 0` which have a single solution :math:`x = -4/7`.  So why do
we have two solutions for this one?

Here are at least three ways of looking at this:

Squaring numbers removes the minus sign
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you take a simple second degree equation like :math:`x^2 = 16`, you
can easily see that :math:`x=4` and :math:`x=-4` will both give you 16
when you square x.  So the existence of the term :math:`x^2` means
that you can have up to two different values of x that satisfy the
same equation.

Multiplying two first degree polynomials
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Returning to Equation :eq:`eq-quadratic-simplified`, let's take a look
at the two solutions 2 and 3.  Now form two first degree equations
that are easily solved by 2 and 3: :math:`x-2` and :math:`x-3`.  If
one of those expressions is always zero when x is a solution, then the
product of them :math:`(x-2) \times (x-3)` will always be zero.  So we
can write:

.. math:: (x-2) \times (x-3) = 0

But we can also work out that product of :math:`(x-2)\times(x-3)`:

.. math:: (x-2) \times (x-3) = x^2 - 2x - 3x + 2 \times 3 = x^2 - 5x + 6

which is our euqation again!  This is another way to see clearly that
both settings for x, 2 and 3, will make that equation be zero.

Plotting the polynomial and looking at zero crossings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you plot the polynomial, as we will see below in
:numref:`fig-poly-x2` and other figures in that section, you see that
the shape of the plot is what we call a *parabola*, and that the
values of x for which it crosses the x axis also depend on the values
of a, b and c.

.. _sec-the-chapter-plots:

The chapter: Plots
==================

A very energetic math professor in college used to always say "Let's
get a picture going!" when embarking on understanding something new.

So here are some plots to get a visual feel for what these *solutions*
are.  We start with the simplest polynomial :math:`y=x^2` which has a
single solution at :math:`x=0`.

.. literalinclude:: poly-x2.gp
   :language: gnuplot
   :caption: Simplest second degree polynomial :math:`y = x^2`

.. _fig-poly-x2:

.. figure:: poly-x2.*
   :width: 40%

   Plot of the simplest second degree polynomial :math:`y = x^2`.
   Note the only root is at :math:`x=0`.


Then we look at a more complicated second degree polynomial.  The
shape is the mostly similar, but the position is different.  You can
see from :numref:`fig-poly-x2-2roots` that it cuts through the x axis
in two points: :math:`x=2` and :math:`x=3`.


.. literalinclude:: poly-x2-2roots.gp
   :language: gnuplot
   :caption: Second degree polynomial :math:`y = x^2 - 5x + 6`

.. _fig-poly-x2-2roots:

.. figure:: poly-x2-2roots.*
   :width: 40%

   Plot of the second degree polynomial :math:`y = x^2 - 5x`.  Note
   the roots at :math:`x=2` and :math:`x=3`.  Also note that we had to
   narrow the values of x to go between 1 and 4 (instead of the
   default -10 to 10 that gnuplot does).  This is so that we can see
   more closely where :math:`y = 0` occurs.

And now for an example where you have *no* solutions: y will never be
zero.


.. literalinclude:: poly-x2-no-roots.gp
   :language: gnuplot
   :caption: Second degree polynomial :math:`y = x^2 - x + 24`

.. _fig-poly-x2-no-roots:

.. figure:: poly-x2-no-roots.*
   :width: 40%

   Plot of the simplest second degree polynomial :math:`y = x^2 - x +
   24`.  Note that there are no roots: the plot is entirely above the
   x axis.

Our final plot will show the example of a plot which points downward.
This happens when the :math:`x^2` term has a minus sign on it.



.. literalinclude:: poly-x2-upside-down.gp
   :language: gnuplot
   :caption: Second degree polynomial :math:`y = -x^2 + x + 2`

.. _fig-poly-x2-upside-down:

.. figure:: poly-x2-upside-down.*
   :width: 40%

   Plot of the second degree polynomial :math:`y = -x^2 + x + 2` which
   points the opposite way: the minus sign in :math:`-x^2` makes the
   branches of the parabola point down instead of up.  It has roots at
   :math:`x=1` and :math:`x=-2`.

What do we get out of all these plots?  We have explored the landscape
of second degree polynomials and seen that changing the parameters a,
b and c affects the direction and placement of the figure, including
the places in which the plot crosses the x axis.  These are called the
*roots* of the polynomial.

We have also seen how the values of a, b and c determine if there will
be 2 solutions (the two arms of the parabola intersect the x axis), or
one solution (the bottom of the parabola grazes the x axis), or no
solutions (the parabola is entirely above or entirely below the x
axis).


.. _sec-the-quadratic-formula:

The chapter: The quadratic formula
==================================

Here is our centerpiece: the quadratic formula.  The two solutions to
the equation

.. math:: ax^2 + bx +c = 0

are:

.. math:: x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}

As we mentioned above, there can be up to two solutions.  Sometimes
there will be one solution (when :math:`b^2-4ac = 0`), and sometimes
there will be *no* real solutions (when :math:`b^2-4ac < 0`, since as
you know the square root of a negative number is not a real number).

So how do we apply this?  Here are some examples from the plots we
showed in :numref:`sec-the-chapter-plots`.

In the equation :math:`x^2=0` we have :math:`a=1, b=0, c=0` so there
is a single root:

.. math:: x = \frac{-0 \pm \sqrt{0^2 - 4 \times 1 \times 0}}{2 \times 1} = 0

Looking at :math:`y = x^2 - 5x + 6` we have :math:`a=1, b=-5, c=6`, so
the two roots are:

.. math::
   x = \frac{-(-5) \pm \sqrt{(-5)^2 - 4 \times 1 \times 6}}{2
   \times 1} = \frac{5 \pm \sqrt{25 - 24}}{2} = \frac{5 \pm
   1}{2} = (3, 2)

Now for the polynomial :math:`y = x^2 - x + 24`.  This has :math:`a=1,
b=-1, c=24` and the quadratic formula gives:

.. math::
   x = \frac{-(-1) \pm \sqrt{(-1)^2 - 4 \times 1 \times 24}}{2
   \times 1} = \frac{1 \pm \sqrt{-95}}{2} = \frac{1 \pm
   \sqrt{-95}}{2}

which has no solutions because :math:`b^2-4ac = -95` which is negative
and thus has no real square roots.

Finally the upside-down parabola :math:`y = -x^2 + x + 2` has
:math:`a=-1, b=1, c=2` and the solutions are:

.. math::
   x = \frac{-1 \pm \sqrt{1^2 - 4 \times (-1) \times 2}}{2 \times (-1)} =
   \frac{-1 \pm \sqrt{5}}{-2} = ((1-\sqrt{5})/2, (1+\sqrt{5})/2)

All of these solutions should match the places in the plots where the
graph crosses the axes.


.. _sec-numerical-approximation:

The chapter: Numerical approximation
======================================

[Pedagogical goal: it's important to have a bit of programming for
each math/science topic we demonstrate.]

We have two goals in this section: one is to demonstrate how you can
write programs to give a *numerical* approximation to the correct
answer.

The other goal is to show a method which will also help with more
complicated root-finding problems.  You see, quadratic equations are
one of the simplest kinds of equations.  Once you go to cubic (third
degree) polynomials the formula is much more complicated, for quartic
(fourth degree) polynomials it's crazy, and for fifth degree and
higher there are no formulae. [#galois]_

"Root finding" is one of the most studied topics in numerical analysis
and we will just scratch the surface here, but we will get some good
results.  Type in the program in :numref:`listing-numerical-zeros-py`
and try running it.

.. _listing-numerical-zeros-py:

.. literalinclude:: numerical-zeros.py
   :language: python
   :caption: numerical-zeros.py Look at where a second degree
             polynomial crosses zero.

It will tell you an approximate 


The chapter: Applications
=========================

Physics: falling bodies
-----------------------

One of the earliest successes of physics, hundreds of years ago, was
being able to calculate what happens with falling bodies, both those
that fall straight to the ground and those that are constrained (like
a pendulum).

The force pulling a body to the ground on the surface of the earth is
given by:

.. math:: F = -mg

where m is the object's mass (typically measured in kilograms, `kg`), and
:math:`g` is the *acceleration of gravity* on the earth's surface
(typically measured in meters per seconds squared, :math:`m/s^2`).

You can then couple this with Newton's second law for how any body
feels a force:

.. math:: F = m a

where a is the acceleration produced by that force.  With some
elementary calculus you can find what the height of the body is at any
moment in time :math:`h(t)`.  If you have not yet studied calculus
then bear with me and you will soon see the result which you can just
trust.

The acceleration is the second derivative of the position: :math:`a =
\frac{d^2h(t)}{dt^2}`.  The  solution to the physics equation is:

.. math:: h(t) = h_0 + v_0 t - \frac{1}{2} g t^2

where:

* :math:`h_0` is the height from which we drop it.  Let's say we hold
  it up to 2 meters.

* :math:`v_0` is the initial velocity you give it.  In our case
  :math:`v_0 = 0` because we are just letting it drop.

So now our physics question is: after how much time does the body hit
the ground?

To answer this we note that we hit the ground when the height is 0:
:math:`h(t) = 0`.  If we substitute the physics equation for
:math:`h(t)` we get:

.. math:: 0 = h_0 + v_0 t - \frac{1}{2} g t^2

Since :math:`v_0` is 0, we have:

.. math::
   :label: eq-fall-from-h0

   h_0 - \frac{1}{2} g t^2 = 0

If we put in the values we discussed for initial height and
acceleration of gravity (:math:`h_0 = 2` and :math:`g = 9.81`) we get:

.. math:: -4.905 \times t^2 + 2 = 0

Now we can use our quadratic formula with :math:`a=-4.905, b=0, c=2`
and we get:

.. math::
   :label: eq-fall-from-2m

   t = \frac{-0 \pm \sqrt{0^2 - 4 \times (-4.905) \times 2}}{2
   \times -4.905} = \frac{\pm \sqrt{39.24}}{-9.81} = \pm 0.64

The two solutions are then :math:`\pm 0.64` which is measured in
seconds.

In a physics problem, when you have two possible solutions due to a
"plus or minus" in the square root you look at which of those times
makes sense physically.  Clearly the negative solution (it hits the
ground in the past) does not make sense, so we conclude that the time
we hit the ground is:

.. math:: t = 0.64 \; {\rm seconds}

This value of t solves the equation for when the object hits the
ground.

Exercises for this section
~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Try dropping a somewhat dense object from a height of 2 meters and
   see if you can get a good measurement of how long it takes to hit
   the ground.

#. Solve the same equation for t using a height of 3 meters and 4
   meters, and see if you can *safely* time how long it takes a body
   to fall from those heights.

#. Write out the general solution to Equation :eq:`eq-fall-from-h0`.
   You can use the quadratic formula with :math:`a=-g, b=v_0, c=h_0`.

#. Discuss how to pose a more interesting problem in which you give
   the ball a slight upwards toss.  This basically boils down to using
   a small initial velocity :math:`v_0`.  Then take the more general
   form of the equation that you just wrote out in the previous
   exercise and plug in your value for :math:`v_0` as well as those
   for :math:`h_0, g`, and calculate the time at which the object will
   hit the ground.

#. I mentioned that the solution for t has two values, due to the
   :math:`\pm` that comes from solving a second degree polynomial.  I
   told you to ignore the negative time solution, but try discussing
   with your classmates what the meaning of that negative time
   solution might be if you started tracking the trajectory of your
   body *before* the moment at which you drop it.



Geometry: areas and the Dido problem
------------------------------------

[Pedagogical note: I like to introduce associated historical facts to
make the subject more fun, to connect it to other subjects, and to
remind students that all areas of knowledge shine light on each
other.]

Now we solve a geometrical problem, inspired by the legend of Dido,
queen of Carthage.

The backstory is that some 8 or more centuries BCE, Dido led a party
of Phoenicians to settle an area in what is today Tunisia.  She
negotiated permission with a local Berber king to found her colony in
an area big enough to be encircled by an ox hide.

She cut the hide into very narrow strips, and thus had a fixed length
of hide and needed to draw the shape with the biggest area that could
be surrounded by that hide.

So "Dido's problem" is: what shape gives you the most area with a
fixed perimeter?

The general mathematical solution of this problem is hard to prove,
but we will compare two candidate shapes with a fixed perimeter: a
square and a circle.  We will then calculate the areas of each and see
which would be a better choice.

If we call the perimeter p, then we have:

.. math::

   A_c = \pi r^2   \\
   p = 2 \pi r

which gives

.. math:: A_c = \frac{\pi p^2} {(2\pi)^2} = \frac{p^2} {4\pi}

For the square:

.. math::

   A_{sq} = r^2   \\
   p = 2 \pi r

which gives:

.. math:: A_{sq} = \frac{p^2} {(2\pi)^2} = \frac{p^2} {4 \pi^2}


FIXME: must still find the killer app for a quadratic formula on this.

Exercises for the chapter
=========================

[Pedagogical goal: apart from the usefulness of doing exercises, these
are formulated to draw students beyond the material they should be
prepared for -- sometimes even beyond what is reached in high school,
like 3rd degree polynomials.]

Third degree polynomial equations like :math:`a x^3 + b x^2 + c x + d
= 0` have messy-looking solutions (and higher degree polynomals even
more so), so we will not look in to them here, but some of these
exercises will show some of the cleaner cases that can come up.

.. exercise::

   On paper expand the expression :math:`(x-3) (x-2) (x-1) (2x-1)
   (x+1)` into a polynomial.  It should be a fifth degree polynomial.
   With your classmates find a way to plot it so that you see the
   ranges well (hint: I came up with setting an x range of `[-2:4]`
   and a y range of `[-6:14]`).  Can you guess what the roots are?
   Does the plot sem to confirm your guess?  Discuss with your
   classmates why the plot diverges so much at the left and right
   ends.

.. exercise::

   Repeat the steps in the previous exercise, but this time craft your
   own *sixth* degree polynomial.  Discuss how the plot is different
   from the previous one.

.. exercise::

   Make up a seventh degree polynomial, plot it, then put that
   polynomial in the program in :numref:`listing-numerical-zeros-py`
   to find its roots.  Do the roots found in your program seem to
   match what you see in your plot?

.. exercise::

   Look for the roots of non-polynomial equations.  For example: the
   :math:`\sin(x)` function has roots at :math:`(0, \pi, 2\pi, 3\pi,
   ...)` and in fact at any integer multiple of :math:`\pi`.  Can you
   find approximations to that?

.. exercise::

   The program in :numref:`listing-numerical-zeros-py` has a rather
   coarse step size of 0.01.  Discuss how this limits the accuracy of
   your solution (i.e. by how much have your solutions been "off" of
   the correct solution?), and try making it smaller to see if the
   accuracy gets better.  Try making it *really* small (how about
   1.0e-6, which is scientific notation for one millionth) and see if
   the program starts taking too long to execute.

Further study
=============

https://en.wikipedia.org/wiki/Quadratic_formula


.. rubric:: Footnotes

.. [#galois] The proof that there is no general formula for fifth
   degree and higher polynomials is one of the deepest and most
   beautiful theorems in mathematics; it is the subject of *Galois
   Theory*.  Sadly it is too complex to describe in this footnote.
