#! /usr/bin/env python

"""This program shows an example of looking for roots of a polynomial.
We start with a guess that the roots will be between x = -10 and x= 10
and sample points until we find that y has changed sign.
"""

import math

## FIXME: must write it; look for zero crossings by looking for a
## change in sign

lowest = -10
highest = 10
step = 0.01

def main():
    x = lowest
    y = poly(x)
    previous_sign = -1 if y < 0 else 0
    while x <= highest:
        y = poly(x)
        sign = -1 if y < 0 else 1
        x += step               # move along the x axis
        if previous_sign != sign:
            print('we found a root at approximately x = %g' % x)
        previous_sign = sign

def poly(x):
    y = -x**2 + x + 2
    # y = x**2 - 5*x + 6
    # y = 7*x**2 + 2*x - 40
    ## now try a higher order polynomial
    # y = 2*x**5 - 3*x**4 - x**3 + 2*x**2 + x - 1
    return y

main()
