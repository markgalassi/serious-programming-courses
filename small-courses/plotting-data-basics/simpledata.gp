##REQUIRED_FILE: simpledata.dat
set xlabel 'x axis'
set ylabel 'y axis'
set multiplot layout 3,1 title 'Three ways of using "plot"'
plot 'simpledata.dat' using 1:2 title 'points'
plot 'simpledata.dat' using 1:2 with lines title 'lines'
plot 'simpledata.dat' using 1:2 with linespoints title 'linespoints'
