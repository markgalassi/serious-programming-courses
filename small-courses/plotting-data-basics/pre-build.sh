#! /bin/sh

echo "running: $0"
echo "args: $*"

## generate the simple sin wave
echo "simplewave.txt: simplewave.py ;	python3 simplewave.py > simplewave.txt" | make -f-
## and the one that we plot
echo "simplewave-write.dat: simplewave-write.py ;	python3 simplewave-write.py" | make -f-
