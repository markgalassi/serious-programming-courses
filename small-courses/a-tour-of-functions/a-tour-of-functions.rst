.. _chap-a-tour-of-functions:

=====================
 A tour of functions
=====================

.. sectionauthor:: Leina Gries <leinagries@gmail.com>

[status: content-mostly-written]


Motivation, Prerequisites, Plan
===============================

A function is a relationship between two (or more) variables.  For now
we will call one of them :math:`x` and the other :math:`y = f(x)`
(*the value of the function at x*).  For this relationship to be a
true function, each value of x must give a unique value of y.

We use other terminology for functions.  We can say:

* y is a function of x

* f is a mapping from x to y

There are other terms people use as well.  As usual, don't worry about
differing terminology: they mean more of less the same thing: you take
a value x, put it into a machine, crank the machine, and out pops the
value y.

The goal of this chapter is to gain a deeper understanding of
mathematical functions and how to go about plotting them.  Initally,
we will experiment with basic linear functions, using them as a tool
to understand plotting software and seeing how visualization can be
useful.  We will experiment with plotting multiple lines at a time,
and look at how the geometry matches up with the algebra.

Then we will look at polynomials and explore how they solve some
physical problems and how the solutions to these problems can be
visualized with plotting software.

Afterwards, we will move into the realm of more advanced functions-
touring exponential growth and decay, the curves made by sine and
cosine waves, bell curves (Gaussian Distribution), factorials and the
fibonacci sequence. Then, we will cover some functions that create
interesting and visually pleasing graphs.

Finally, we will apply our findings to the real world and plot
functions that describe real life events, such as climate change and
nuclear fission.

.. _sec-linear-functions:

Linear Functions
================

To start, let's stay linear. Graph the equation :math:`y=5x-8` by
running gnuplot and typing the following expressions:

.. code::

   gnuplot> set grid
   gnuplot> plot 5*x - 8

This should bring up a graph displaying the equation in a seperate
window. Try plotting this with and without grid lines, using the
command (code). Explore the window. What can you do to make the line
displayed change?

Can you make the slope negative?

Can you graph several lines at once? This brings us to plotting two
linear functions.  Try plotting the original line with the addition of
:math:`y=4x-6`.

.. code::

   gnuplot> set grid
   gnuplot> plot 5*x - 8
   gnuplot> replot 4*x - 6

.. _fig-linear:

.. figure:: linear.*
   :width: 70%

   Two lines, one with slope 5 and the other with slope 4.  Lines with
   different slopes will always intersect.

We see *visually* in :numref:`fig-linear` that the lines intersect.
How do we figure out at which point they touch?  Visually we can see
that x is near 2 and y is near 2, but can we get the exact values?

This is where the beauty of *analytic geometry* comes in: there is a
relationship between the gometry (which we see visually) and the
analysis that you can do using algebra.  The key here is: ::

   The intersection is the point at which the x and y values of both
   lines are the same.

How do we express that algebraically?  We can start by saying that if
:math:`y = 5 x - 8` and :math:`y = 4 x - 6`, then:

.. math::

   5 x - 8 = 4 x - 6

There are many ways of solving this, and depending on where you are in
your math studies you might have seen one or more.  I will do one of
the simplest: if :math:`5 x - 8` and :math:`4 x - 6` are equal, then I
can add and subtract *the same* quantities from both, and the result
will still be equal.  That gives

.. math::
   :nowrap:

   \begin{align}
   5 x - 8 & = 4 x - 6 \\
   5 x - 8 - 4 x & = 4 x - 6 - 4 x \\
   x - 8 & = - 6 \\
   x - 8 + 8 & = - 6 + 8 \\
   x & = 2 \\
   \end{align}

Once we have x we can plug that value into either of the equations for
y and we get :math:`y = 2`, so the intersection point is :math:`(2, 2)`

This technique can be used in many other circumstances to find where
two *curves* intersect.  The algebra will be more complicated than
finding  the intersection of lines, but the idea will be the same.

Can you think of a way to find out multiple points of intersection
between three or more lines?

Another bit of terminology: these functions in which x appears only in
the first power (there is no :math:`x^2` or :math:`x^3`) always give
straight lines when you plot them.  Because of this we call them
*linear* functions.

FIXME: discuss Direct proportionality.


Polynomials
===========

Polynomials are the next step in complexity. Let us start with a
binomial (second degree polynomial).  Plot the function
:math:`y=3x^2+6x-4`

.. _fig-quadratic:

.. figure:: quadratic.*
   :width: 70%

   A simple second degree polynomial :math:`y=3x^2+6x-4`

From the graph, what can you observe about this function?  Where are
the x intercepts?  The y intercept?  While these are clear in some
cases, they are difficult to find in others.

The figure that comes from plotting a second degree polynomial is
called a *parabola*.  This one has two arms that grow up from its
minimum.

We will spend a bit of time discussing this plot because it teaches us
some interesting things to look out for.

* This polynomial has three *terms:* :math:`3 x^2`, :math:`6 x` and
  :math:`-4`.  Each of these three terms has more or less importance
  according to the value of :math:`x`.  When x is 0, then the constant
  term 4 dominates.  When x is between 0 and 1 or 0 and -1 then the term
  :math:`6 x` grows to be more important.  When x gets much bigger
  than 1 (or much smaller than -1) then the :math:`3 x^2` term (second
  order term) dominates.

* Terminology: the number that multiplies :math:`x^2` (in our case 3)
  is called the *coefficient* of the second order term (or the
  *quadratic* term).  The number that multiplies :math:`x` (in our
  case 6) is called the coefficient of the *first order term* (or
  *linear* term).  The term that doesn't have x is called the
  *constant term* or the *offset*.

* The arms of the parabola eventually point up.  This is because the
  term with :math:`x^2` gets much bigger than the other ones, and when
  x is negative, :math:`x^2` is still positive.

* Zero crossings: it is interesting to note where the parabola crosses
  the x axis and the y axis.  In class we discuss how to find this by
  zooming in on the picture (both with the mouse and with the range
  parameters in gnuplot).  Terminology: the values of x at which a
  function crosses the x axis (i.e. where :math:`y = 0`) are called
  the *roots* of the function.

* It is also interesting to note the value of x for which the parabola
  has its *minimum* or *maximum* values.  Sometimes there is no
  *global* minimum or maximum, so in class I describe what a *local*
  minimum or maximum is.  There is a technique for finding maximum and
  minimum values for a function.  This involves calculating the
  *derivative* of the function.  We will say a bit more about
  derivatives below.

Derivatives
-----------

Without introducing the entire subject of differential calculus (for
that see :numref:`chap-pushing-toward-calculus`), let's
look at the parabola we have been considering: :math:`y=3x^2+6x-4`.
Looking at the plot, study what the slope of that plot is for big
negative x, for x between -3 and 0, and for big positive x.

We expect the slope to be big and negative for very big negative x,
close to zero for that value of x between -3 and 0, and then to get
positive and big.

When we learn calculus we will learn that you can figure out exactly
what this slope is at every point in the curve.  Without showing how
it's done, I show the result here.  We call the function that is the
slope of :math:`f(x)` the *derivative* of f, and we write
:math:`\frac{df(x)}{dx}`.  The derivative of this second degree
polynomial is:

.. math::

   \frac{df(x)}{dx} = 2 \times 3 x + 6 = 6 x + 6

The properties of derivatives that we used here were: (a) the
derivative of a sum of terms is the sum of the derivatives of each
term, and (b) the derivative of :math:`x^n` is :math:`dx^n/dx =
n \times x^{n-1}`.

The place at which the derivative is zero is :math:`x = -1`, so we can
say that the our parabola has its minimum at :math:`x = -1`.  If you
zoom in to the plot enough you will see that this is true
geometrically.

You should explore this polynomial further.  You should change the
sign or some of the values and see how different the plot looks.


Higher order polynomials
========================

Having successfully plotted a binomial, let's move onto higher order
polynomials.  Plot :math:`x^4 + x^3 - 7 x^2 - x + 6`.  The result will
initially look somewhat boring.

.. _fig-quartic-boring:

.. figure:: quartic-boring.*
   :width: 70%

   A fourth degree (quartic) polynomial :math:`y = x^4 + x^3 - 7 x^2 -
   x + 6`.  This does not look too interesting at this time.


.. _fig-quartic:

.. figure:: quartic.*
   :width: 70%

   The same fourth degree (quartic) polynomial :math:`y = x^4 + x^3 -
   7 x^2 - x + 6`.  By zooming in to the x range [-3.2:3.2] we see
   interesting features.

Now, finding the root (or solutions, found at the x intercepts of the
equation) are much more dificult to find manually.

By zooming in further, find the roots of this 4th order polynomial.
How many are there?  Does this relate to the degree of the polynomial?

And now let me tell you how I picked this polynomial: I wanted to find
a polynomial that crosses the x axis at -3, -1, 1 and 2.  How do you
do that?  First notice that the first order polynomial :math:`(x-1)`
is zero when :math:`x = 1`.  So we can craft a set of first 1\
:sup:`st`\ order polynomials (linear functions) which cross the x axis
at -3, :math:`(x + 3)`, -1 :math:`(x + 1)`, 1 :math:`(x -1)` and 2
:math:`(x - 2)`.  Since zero multiplied by any number gives zero, we
can try to *multiply these together*.  We get:

.. math::

   y = (x+3) \times (x + 1) \times (x - 1) \times (x - 2)

If you multiply out all the expressions in parentheses you will find
that:

.. math::

   y = x^4 + x^3 - 7 x^2 - x + 6

Would you have been able to *go the other way*: go from :math:`x^4 +
x^3 - 7 x^2 - x + 6` to :math:`(x + 3) \times (x + 1) \times (x - 1) \times (x - 2)`?
It's much harder to do, and it's called *factoring a polynomial*.

The factored expression is useful because it tells you clearly where
all the roots of the polynomial are.  The normal expression is useful
because you can see clearly the coefficients of the terms of each
order.  In particular, the fact that the :math:`x^4` has a positive
coefficient (in this case 1) and an even power allows you to say that
the curve will have arms that point up on both sides.

Another bit of terminology: the zoomed plot in :numref:`fig-quartic`
shows a lot of *structure* near :math:`x = 0`, but the unzoomed plot
in :numref:`fig-quartic-boring` shows that for large negative and
positive values of x you just go up.  The behavior of a function when
you go to large values of x (or large negative values) is called
*asymptotic* behavior, and understanding the asymptotic behavior of a
function is a useful and deep area of mathematics.

Note that with simple polynomials, it is relativly easy to graph
manually and to find the y for a given x. However with higher order
polynomials a plotting program helps us gain insight.


.. _sec-inverse-functions:

Inverse functions
=================

FIXME: must write; discuss 1/x and x^-n and x^-(a/b)

Mention *inverse proportionality*.

When one 



Elementary transcendental functions
===================================

A function which cannot be written as a polynomial is called a
*transcendental function*.


Exponentials
------------

Exponential growth and decay are commonly seen phenomena in daily
life.  The exponential function is be used to model many real life
situations with growth and decay.

Growth
~~~~~~

Let us start with a simple problem and a small program to simulated
it.  Let's say we have a pair of rabbits, and every month they produce
two baby rabbits.  (NOTE: rabbits do have a one month gestation time,
but their litters are actually much bigger -- 4 to 12 kits, but we're
keeping it simple).

After one generation you have 2 new rabbits (a total of 4).  Then
those pair up and reproduce and you have 4 new rabbits (a total of 8).

A simple program to demonstrate this is:

.. _listing-simple-reproduction:

.. literalinclude:: simple-reproduction.py
   :language: python
   :caption: Program which models simple reproduction.

Run this program and notice that the 

The general formula for exponential growth or decay is written
:math:`A(t) = P_0e^{r*t}`, where A is the amount at any given t, time,
r is the *rate* of growth or decay, and :math:`P_0` is the initial
amount of whatever is being changed. It is important to remember here
that e is simply a number -- it is not a variable, and should remain
constant in each situation. In this way, it should be treated the same
as pi.

Let us explore this function:

.. math::

   f(x) = e^x

Wait, wait wait: what does this even mean?  We have seen what it means
to take a number, like 4, and raise it to a power, like 3: :math:`4^3
= 4 \times 4 \times 4 = 64`.  But what does it mean to take numbers
that are *not* integers and raise them to powers?  That's
straightforward if the power is an integer.  For example: :math:`1.7^3
= 1.7 \times 1.7 \times 1.7 = 4.913`.  That's all fine.

But what does it mean to take a number to a power that is not an
integer?  The topic is too long to go in to in detail, but remember
this property you probably learned a long time ago:

.. math::

   x^{2 + 1} & = x^2 \times x^1 \\
   x^{m + n} & = x^m \times x^n

This allows us to define fractional exponents:

.. math::

   x^{1} = x^{1/2 + 1/2} = x^{1/2} * x^{1/2}

which means that :math:`x^{1/2}` is that number which, when squared,
gives x, which is the very definition of square root!  Thus:

.. math::

   x^{1/2} = \sqrt{x} \\
   x^{1/3} = \sqrt[3]{x} \\
   \dots \\
   x^{1/n} = \sqrt[n]{x} \\

Using the properties of exponents that we know, this allows us to get
pretty close to almost all expressions :math:`a^b` where a and b are
real numbers, not just integers or fractions.

The number :math:`e` is a special number, called *Euler's number*.
It's numeric value is about 2.71828, and it comes up in so many places
in math that it is as important as :math:\pi`.

Let us look at our rabbit reproduction problem and write out an extra
number.  Change the line that prints the current population to say:

.. code-block:: python

           print(generation, n, math.pow(2.0, generation+1))

Note that you will also need to :code:`import math` at the top of your
program.

If you run the program again you will see that the exponential
function :math:`2^(generation+1)` matches what our simulation of
rabbit growth gives.

At some point you will learn that you can convert an exponential in
base e to an exponential in base 2, or 10, or any other number.  The
three important ones are e (occurs naturally in math), 2 (comes up in
computer science) and 10 (we have 10 fingers, and our digits are 0 to
9).  This allows us to write:

FIXME

.. exercise::

Introduce rabbit death.  Then think about introducing foxes.  Refer to
the proposed chapter on predator-prey problems.


Decay
~~~~~

Uranium 235 has a half-life of 703 million years.  Let's write a
program to simulate how some :math:`5 \times 10^22` atoms of U235
would decay.

.. _listing-simple-decay:

.. literalinclude:: simple-decay.py
   :language: python
   :caption: simple-decay.py - a program which models simple decay.

Run the program and discuss what the meaning of half-life is and how
it relates to exponential decay.


Trigonometric function
----------------------

.. _sec-gaussian-distribution:

Gaussian distribution
=====================

Gaussian distribution, more simply known as the bell curve or normal
distribution, is found in many places in the world around us. When the
heights of children of a similar ages are graphed in relation to one
another, the result is a bell curve. Named for its distinctive, bell
like shape, the bell curve can be easily graphed with knowledge of a
relatively simple equation. It is :(equation-formatting?)  where () is
the mean, and () is the standard deviation.

Now, of course we could calculate these values manaually, but why
would we?Use (code) to generate a random set of numbers. Start out
with about 20.

Now, use () to find the sum of these numbers. Check your answer
logically- does this seem accurate? Now, divide this by the number of
values you used.

This works, but it would be easy to cut out the middle step- use
(code) to determine the mean with no possibility for user error.

Next, you can find the standard deviation- use (code).

Now, you're all set up to plug these numbers into the equation listed
above. But this seems time consuming, and programming is all about
finding ways to minimize repetitive tasks. Use (code) to find the
standard deviation of your data set.

Try it with data for the heights of your school, or for some other
large data set that you find interesting.

Next, plot this data using (code).

Try manipulating the graph using the above techniques.
