#! /usr/bin/env python3

import math

def main():
    n_generations = 10
    n0 = 2                      # initial population
    print('## generation n_rabbits')
    # print('## generation n_rabbits exponential')
    n = n0
    for generation in range(1, 11): # let's do 10 months
        n_new = (n / 2.0) * 2
        n = n + n_new
        print(generation, n)
        # print(generation, n, math.pow(2.0, generation+1))

main()
