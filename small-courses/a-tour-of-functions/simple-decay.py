#! /usr/bin/env python3

def main():
    n_steps = 10
    ## U235 density is 19.1 g/cm^3
    n0 = 5e22                      # initial population
    print('## time_yr N_U235')
    n = n0
    time = 0
    ## U235 half life 703.8 million years
    while time < 7.03e10:       # 100 half-lifes
        n = (n / 2.0)
        print('%10.4g    %10.4g' % (time, n))
        time += 7.03e8

main()
