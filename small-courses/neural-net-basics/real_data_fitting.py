from helper_functions import *

def main():
    num_children_per_net = 20
    num_survive = 5
    variance = 0.5
    generations = 30
    batch_size = 75
    net_dimensions = [1,5,5,5,1]
    filepath = 'car_data.csv'
    
    first_parent = generate_blank_net(net_dimensions)
    xs, ys = read_data(filepath)
    next_gen = make_next_gen([first_parent], xs, ys, num_children_per_net, num_survive, variance) # create first generation
    
    inputs = []
    expected_outputs = []
    for i in range(len(xs)):
        inputs.append(xs[i][0])
        expected_outputs.append(ys[i][0])  # reformat xs and ys for matplotlib
    
    for i in range(generations):
        next_gen = make_next_gen(next_gen, xs, ys, num_children_per_net, num_survive, variance, batch_size)
        variance *= 0.97
        print(f'The RSS is {eval_total_cost(next_gen[0], xs, ys) * 1000} after generation {i+1}.') # the times 1000 makes the RSSs a reasonable value
        
        outputs = []
        for i in range(len(xs)):
            outputs.append(calc_all_layers(xs[i], next_gen[0], generate_layer_dict(next_gen[0])))
        plt.clf()
        plt.scatter(inputs, expected_outputs, s=0.1)
        plt.plot(inputs, outputs)
        plt.savefig('real-data-fit.png')
        print('Figure saved to real-data-fit.png')
    plt.show() # show the final fit

def eval_RSS(net_outputs, expected_outputs):
    """gets RSS for a given set of outputs"""
    RSS = 0
    for i in range(len(net_outputs)):
        for j in range(len(net_outputs[i])):
            RSS += ((net_outputs[i][j] - expected_outputs[i][j]) ** 2) / len(net_outputs[i]) # make error independant of output layer size
    RSS /= len(net_outputs) # make error independant of data set size
    return RSS

def eval_total_cost(net, input_layers, expected_output_layers):
    """properply formats inputs to eval_RSS"""
    net_outputs = []
    layer_dict = generate_layer_dict(net)
    for i in range(len(input_layers)):
        net_outputs.append(calc_all_layers(input_layers[i], net, layer_dict))
    cost = eval_RSS(net_outputs, expected_output_layers)
    return cost / len(input_layers)
                       
def make_children(net, variance, num_children):
    """creates a specificied number of tweaked copies of a given net"""
    children = []
    for i in range(num_children):
        child = nx.DiGraph(net)
        for edge in child.edges:
            child.edges[edge]['weight'] += random.random() * variance - variance / 2
        children.append(child)
    return children

def make_next_gen(parents, input_layers, expected_output_layers, num_children_per_net=20, num_survive=1, variance=0.1, batch_size=50):
    """creates many of copies of a net, tweaks them slightly, then picks out the best ones to create the next generation."""
    children = parents.copy() # make sure RSS doesn't go up
    best_children = []
    costs = []
    batch_inputs, batch_expected_outputs = generate_batch(input_layers, expected_output_layers, batch_size)
    for parent in parents:
        loop_children = make_children(parent, variance, num_children_per_net)
        for child in loop_children:
            children.append(child)
    for child in children:
        costs.append(eval_total_cost(child, batch_inputs, batch_expected_outputs))
        
    for i in range(num_survive):
        best_child_index = costs.index(np.min(costs))
        best_children.append(children[best_child_index])
        children.pop(best_child_index)
        costs.pop(best_child_index)

    return best_children

def read_data(filepath):
    """reads and extracts data from the given file"""
    xs = [] # car listing
    ys = [] # car selling prices
    with open(filepath) as car_data:
        for line in car_data:
            values = line.split(',')
            if values[1].isdigit():
                xs.append([float(values[3])])
                ys.append([float(values[2])])
    # rescale data by median data point
    xs = (np.array(xs) / np.median(xs)).tolist()
    ys = (np.array(ys) / np.median(ys)).tolist()
    xy_zip = list(zip(xs, ys))
    xy_zip.sort(key=lambda x: x[0][0])
    outliers = []
    for point in xy_zip:
        if point[0][0] > 7.5: # arbitrary cutoff for outliers
            print(f'Outlier found: {point}')
            outliers.append(point)
            continue
        if point[1][0] > 7.5:
            print(f'Outlier found: {point}')
            outliers.append(point)
    for outlier in outliers:
        xy_zip.remove(outlier)
    xs, ys = list(zip(*xy_zip))
                
    return xs, ys

def generate_batch(inputs, expected_outputs, batch_size):
    """generates a random batch based on the batch size"""
    shuffle_zip = list(zip(inputs, expected_outputs))
    random.shuffle(shuffle_zip)
    inputs, expected_outputs = list(zip(*shuffle_zip))
    batch_inputs = []
    batch_expected_outputs = []
    for i in range(batch_size):
        batch_inputs.append(inputs[i])
        batch_expected_outputs.append(expected_outputs[i])
    batch_zip = list(zip(batch_inputs, batch_expected_outputs))
    batch_zip.sort(key=lambda x: x[0][0])
    batch_inputs, batch_expected_outputs = list(zip(*batch_zip))
    return batch_inputs, batch_expected_outputs

main()
