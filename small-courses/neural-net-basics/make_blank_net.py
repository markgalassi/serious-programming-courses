import networkx as nx
import matplotlib.pyplot as plt
import random # for weights

def main():
    simple_net = generate_blank_net([1,3,1]) # create our simple example net
    layer_dict = generate_layer_dict(simple_net)
    pos = nx.multipartite_layout(simple_net, layer_dict)
    nx.draw(simple_net, pos=pos)
    plt.savefig('simple-net.png')
    print('New net saved to \'simple-net.png\'.')

def generate_blank_net(layer_sizes):
    """creates a network with a specified number of neurons in each layer and random weights."""
    blank_net = nx.DiGraph()
    for i in range(len(layer_sizes)):
        for j in range(layer_sizes[i]):
            neuron = f'l{i}_{j}' # properly formats the neuron names
            blank_net.add_node(neuron)
        blank_net.add_node(f'l{i}_b', activation=1)
    blank_net.remove_node(f'l{len(layer_sizes) - 1}_b') # remove bias neuron in output layer

    for n_1 in blank_net.nodes:
        for n_2 in blank_net.nodes:
            # we need to check if the neurons are in adjacent layers, and make sure that the second neurom isn't a bias.
            if int(n_1[1]) + 1 == int(n_2[1]) and n_2[3] != 'b':
                blank_net.add_edge(n_1, n_2, weight=random.random()*2 - 1)

    return blank_net

def generate_layer_dict(net):
    """make a dictionary of a given neural net's nodes keyed by layer."""
    layer_dict = {}
    for neuron in net.nodes:
        if int(neuron[1]) not in layer_dict.keys():
            layer_dict[int(neuron[1])] = []
        layer_dict[int(neuron[1])].append(neuron)
    return layer_dict


main()
