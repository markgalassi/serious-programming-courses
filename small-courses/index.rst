.. Serious Programming - small courses master file, created by
   sphinx-quickstart on Sat Apr 29 12:54:15 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Serious Programming - small courses
===================================

   :Date: |today|
   :Author:  **Mark Galassi** <mark@galassi.org>
   :Author:  **Leina Gries** <leinagries@gmail.com>
   :Author:  **Sophia Mulholland** <smulholland505@gmail.com>
   :Author:  **Almond Heil** <almondheil@gmail.com>
   :Author:  **Malcolm Smith** <msmith.malcolmsmith@gmail.com>

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   :numbered:

   intro.rst
   plotting-data-basics/plotting-data-basics.rst
   plotting-intermediate/plotting-intermediate.rst
   a-tour-of-functions/a-tour-of-functions.rst
   growth-checked-and-not/growth-checked-and-not.rst
   plotting-advanced/plotting-advanced.rst
   fitting-functions-to-data/fitting-functions-to-data.rst
   case-studies-in-data/case-studies-in-data.rst
   special-numbers-pi/special-numbers-pi.rst
   programming-by-yourself/programming-by-yourself.rst
   random-number-basics/random-number-basics.rst
   randomness-disorder/randomness-disorder.rst
   random-processes/random-processes.rst
   power-laws-zipf-benford/power-laws-zipf-benford.rst
   pushing-toward-calculus/pushing-toward-calculus.rst
   numerical-integration/numerical-integration.rst
   differential-equations/differential-equations.rst
   sim-diff-equations/sim-diff-equations.rst
   lotka-volterra/lotka-volterra.rst
   ecology/ecology.rst
   bio-phylogeny/bio-phylogeny.rst
   recursion/recursion.rst
   programming-sorting/programming-sorting.rst
   NetworkX-intro/NetworkX-intro.rst
   maze-generation/maze-generation.rst
   birthday-paradox/birthday-paradox.rst
   optimal-stopping/optimal-stopping.rst
   GUI/GUI.rst
   canvas/canvas.rst
   TSP/TSP.rst
   agent-based-modeling-basics/agent-based-modeling-basics.rst
   emergent-behavior/emergent-behavior.rst
   web-scraping/web-scraping.rst
   getting-to-philosophy/getting-to-philosophy.rst
   music-basics/music-basics.rst
   collecting-mp3s/collecting-mp3s.rst
   computer-art/computer-art.rst
   image-filtering/image-filtering.rst
   crypto/crypto.rst
   other-languages-go/other-languages-go.rst
   app-guest-lecture-itinerary/app-guest-lecture-itinerary.rst
   app-how-to-build-the-book/app-how-to-build-the-book.rst
   app-how-to-add-a-chapter/app-how-to-add-a-chapter.rst
   app-project-proposals/app-project-proposals.rst
   app-proposed-chapters/app-proposed-chapters.rst
   app-license/app-license.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. bibliography::
   :all:

::

    Copyright (C)  2017-2022  Mark Galassi, Leina Gries, Sophia
    Mulholland, Almond Heil.

    This work is licensed under the Creative Commons
    Attribution-ShareAlike 4.0 International License. To view a copy
    of this license, visit
    http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to
    Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
