.. _sec-programming-sorting:

=============================
 Programming topics: sorting
=============================

[status: content-mostly-written]


Motivation, prerequisites, plan
===============================

There are several reasons to spend some time on sorting.  Apart from
the usefulness of sorting a list of data, we will study it because:

* It illustrates programming techniques.

* We will go from an intuitive understanding of sorting to how that
  can be expressed as an *algorithm*.

* It will give us a simple and yet rich example of how to study the
  *computational complexity* of an algorithm: how the execution time
  grows with the size of the data we work with.

Prerequisites:

* The 10-hour "serious programming" course.

Our plan is to start with an intuitive discussion of what sorting is,
and to distill an algorithm out of that.  We will look in to *sorted
insertion*, and then into the *bubblesort* algorithm.

Experiment: a game of cards
===========================

Start by dealing out a game of cards.  We can start with Poker, or
bridge if you and the other students have already seen it.

When you receive your hand you should:

#. Describe their algorithm to your neighbor in class.

#. Compare what happens when you sort 4 cards to when you sort 8 or 13
   cards.

#. Take notes on paper on how you sorted your hand so as to see all
   the features in it.

Intuition to algorithm on card sorting
======================================

Take your description of the algorithm and try to write a python
function to implement that sorting.  We will do so in the framework of
a small program that is ready to go as soon as you drop in your
sorting function.

Writing up the algorithm in Python
==================================

I show here a simple algorithm for sorting, which might look a lot
like what you have come up with.  The Python function is called
``sort_myveryown()``

.. code:: python

   def sort_myveryown(l):
       """This function implements my very own sorting algorithm, which
       is basically an insertion sort.  It takes the list l, sorts
       it, and returns the sorted result."""
       ## iterate through the whole list, skipping element 0
       for i in range(1, len(l)):
           ## having fixated on element i, look at all the ones
           ## *before* i and see if they are bigger and should 
           ## be placed *after* i
           for j in range(i):
               if l[i] < l[j]:
                   ## this is the case where l[j] is bigger.  since
                   ## all the j come *before* the i this means that
                   ## the sort is incorrect and we must swap i and j
                   # print('SWAP: %d/%d  <--> %d/%d' % (i, l[i], j, l[j]))
                   l[j], l[i] = l[i], l[j]
       return l

Look at the program in :numref:`listing-sort_frame-py` and put your
own code in for the function ``sort_myveryown()``.  You can look up
"insertion sort" in wikipedia for a very simple description of the
algorighm, and their "pseudocode" (a description of the algorithm that
is not in any specific programming language) can be translated to
Python quite easily.  If you feel that your procedure was different
from insertion sort then adapt the code to match what you did.


.. _listing-sort_frame-py:

.. literalinclude:: sort_frame.py
   :language: python
   :caption: sort_frame.py -- Framework for trying out sorting
             algorithms.  You can drop your own into the function
             sort_myveryown()

Profiling the algorithm
=======================

Modify the program to print information
---------------------------------------

The term "profiling" is used in software to mean "figuring out how
much time a computer program spends in each of its functions.  This is
an important tool to figure out how to improve performance.

As you have seen, the two operations which are called repeatedly in a
sorting algorithm are *comparison* and *swap*.  This means that the
most important part of our profiling work will be to count how many
times we compare two values in the list and how many times we swap two
values in the list.  Each algorithm will do differently on those two
issues.

The program in :numref:`listing-sort_frame_profiling-py` gives a
"framework" in which you can drop your own function and it will count
how many times it does a comparison or a swap.  It then prints out the
results in a form that can be easily plotted.

The way the program does this is to have two functions:
``increment_comparisons()`` and ``increment_swaps()``.  We will call
these functions every time we do a comparison or a swap.

The program ``sort_frame_profiling.py`` then prints out the number of
comparisons and number of swaps, as well as the size of the list, for
many different list sizes and for all the algorithms we use.


.. _listing-sort_frame_profiling-py:

.. literalinclude:: sort_frame_profiling.py
   :language: python
   :caption: sort_frame_profiling.py -- Framework for profiling
             sorting algorithms.  You can drop your own into the
             function sort_myveryown() and add the increment_swaps()
             and increment_comparisons() calls to profile the
             complexity of each algorithm. Download it by clicking
             here: :download:`sort_frame_profiling.py`


Run the program and make plots
------------------------------

When you run

.. code:: bash

   mkdir sort-stats            ## let's be tidy abogut where are files are
   cd sort-stats
   python3 sort_frame_profiling.py

you will notice that it takes a while to run (depending on the value
of ``N_MAX`` in ``main()``), and when it is done you will have a few
files called ``sort_ALGO.out`` for each of the algorithms.

You don't have to wait for it to finish!  You can open another
terminal and type:

.. code:: bash

   cd sort-stats
   ls -lsat | head             ## look at recently modified files
   wc sort_*.out               ## see how many lines each file has
   head sort_*.out             ## look at the start of each file
   tail sort_*.out             ## look at the end of each file

You can repeat that ``tail`` command several times as the run goes on
and you will get a feeling for the progress being made.

You can also make plots of the performance of the algorithms.  Run gnuplot:

.. code:: bash

   $ gnuplot

and in gnuplot run the following instructions:

.. _listing-plot-sort-bubble-gp:

.. literalinclude:: plot-bubble-quick.gp
   :language: gnuplot
   :caption: Plot bubblesort and quicksort timing.

.. _fig-plot-sort-bubble:

.. figure:: plot-bubble-quick.*
   :width: 95%

   The performance of bubblesort and quicksort on a random initial
   list.  Note that bubblesort operations grow as :math:`n^2` while
   quicksort operations grow as :math:`n \times log(n)`, which is a
   much slower growth.  This kind of plot shows how the number of
   operations in an algorithm depends on the size of the problem.
   This dependence is called the "computational complexity" of an
   algorithm.


How do we understand these plots?
---------------------------------

[TODO] Make some plots in gnuplot of ``x**2`` and ``x log(x)``, then
introduce two bits of terminology: "computational complexity" and
"asymptotic behavior".



Exercises
---------

.. exercise::

   Modify ``sort_frame_profiling.py`` to count the number of
   *comparisons* made by Python's built-in sort function.  One way to
   do this is to use an argument to ``l.sort(my_compare)`` which
   replaces Python's built-in comparison function.  You could then
   write your own ``my_compare()`` function which would call
   ``increment_comparison(l_type)``.

.. exercise::

   Research whether it is possible to count the number of *swaps* made
   by Python's built-in sort function.


Computational complexity
========================

Make plots versus ``N`` and compare to :math:`N^2` and :math:`N log(N)`

Further reading
===============

The wikipedia articles on various sorting methods are worth a read:

* https://en.wikipedia.org/wiki/Sorting_algorithm

* https://en.wikipedia.org/wiki/Bubble_sort

* https://en.wikipedia.org/wiki/Quicksort

* https://en.wikipedia.org/wiki/Insertion_sort

There is also a web site with animations of several sorting
algorithms:

* https://www.toptal.com/developers/sorting-algorithms

A youtube video that gives video and audio animation of a sort:

* https://www.youtube.com/watch?v=kPRA0W1kECg
