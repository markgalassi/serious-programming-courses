set grid
set xlabel 'N (size of list)'
set ylabel 'number of operations'
plot 'sort_bubble.out' using 1:4 with lines title 'bubble/random/comparisons', \
 'sort_bubble.out' using 1:5 with lines title 'bubble/random/swaps', \
 'sort_quicksort.out' using 1:4 with lines title 'quicksort/random/comparisons', \
 'sort_quicksort.out' using 1:5 with lines title 'quicksort/random/swaps'
