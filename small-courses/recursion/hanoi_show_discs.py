#! /usr/bin/env python3

"""
A demonstration of how to solve the Towers of Hanoi game using
recursion
"""

import pprint

pole_discs = {}

def main():
    global pole_discs
    n_discs = int(input('how many discs? '))
    populate_poles(n_discs, pole_discs)
    pprint.pprint(pole_discs)
    draw_poles(pole_discs)
    move_tower(n_discs, 'A', 'B', 'C')

def move_tower(height, from_pole, to_pole, interim_pole):
    if height > 0:
        move_tower(height-1, from_pole, interim_pole, to_pole)
        move_disk(from_pole, to_pole)
        move_tower(height-1, interim_pole, to_pole, from_pole)

def move_disk(from_pole, to_pole):
    # print('moving disk from', from_pole, 'to', to_pole)
    # updated the graphical data structure based on
    # this move
    moved_size = pole_discs[from_pole][-1]
    del(pole_discs[from_pole][-1])
    pole_discs[to_pole].append(moved_size)
    draw_poles(pole_discs)      # now I can draw it!

def populate_poles(n_discs, pole_discs):
    pole_discs['A'] = []
    pole_discs['B'] = []
    pole_discs['C'] = []
    for i in range(n_discs):
        pole_discs['A'].append(n_discs - i)

def draw_poles(pole_discs):
    positions = [10, 20, 30]
    most = max(pole_discs['A'] + [0])
    most = max(pole_discs['B'] + [most])
    most = max(pole_discs['C'] + [most])
    print('most:', most)
    row_list = ['_']*40
    for i in range(most):
        vpos = most - i
        # handle 'A'
        if len(pole_discs['A']) >= vpos:
            insert_disc(row_list, 10, pole_discs['A'][i])
            row_list[10] = '*'
        if len(pole_discs['B']) >= vpos:
            row_list[20] = '*'
        if len(pole_discs['C']) >= vpos:
            row_list[30] = '*'
        print(''.join(row_list))

def insert_disc(row_list, center, size):
    # print(f'insert {size} in {row_list}')
    for i in range(size):
        pos = center - size // 2 + i
        row_list[pos] = '*'


main()
