#! /usr/bin/env python3

"""Takes a file with a single column of integers and makes a histogram
of how frequently those integers occur in the file."""

import sys

def main():
    fname = sys.argv[1]
    histogram = []
    with open(fname, 'r') as f:
        lines = f.readlines()
        for line in lines:
            delta_t = int(line)
            ## in case this delta_t is bigger than any seen so far
            while delta_t >= len(histogram):
                histogram.append(0)
            histogram[delta_t] += 1

    hist_out_fname = sys.argv[1] + '.hist'
    with open(hist_out_fname, 'w') as f:
        for value in range(len(histogram)):
            f.write('%d    %d\n' % (value, histogram[value]))
    print('wrote histogram to %s' % hist_out_fname)

if __name__ == '__main__':
    main()
