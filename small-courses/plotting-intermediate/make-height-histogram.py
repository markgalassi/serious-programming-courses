#! /usr/bin/env python3

"""Takes file where the first column has heights and makes a histogram
out of it, putting them into 2cm bins between 130cm and 180cm.
"""

import sys
import matplotlib.pyplot as plt
import numpy as np

def main():
    fname = sys.argv[1]
    heights = []
    ## define the "bin edges"
    bin_edges = [i for i in range(135, 182, 2)]
    n_bins = len(bin_edges) - 1
    ## initially all bins are empty
    histogram = [0] * n_bins
    ## now open the file and read in all the values in the first
    ## column; put them in bins as we read them
    with open(fname, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if line[0] == '"':
                continue # skip the first line (it starts with a quote)
            ## break the line up into words, splitting on semicolons
            words = line.split(';')
            ## the first word is the height; convert it to float
            height = float(words[0])
            ## now put it in the right bin
            for bin, low_edge in enumerate(bin_edges[:-1]):
                if height >= low_edge and height < bin_edges[bin+1]:
                    ## found it!
                    histogram[bin] += 1

    ## now write out a file with the midpoints of the bins on the x
    ## axis and the number of heights in that bin on the y axis
    hist_out_fname = sys.argv[1] + '.hist'
    with open(hist_out_fname, 'w') as f:
        for bin in range(n_bins - 1):
            midpoint = (bin_edges[bin] + bin_edges[bin+1]) / 2.0
            f.write('%g    %d\n' % (midpoint, histogram[bin]))
            print('%g    %d' % (midpoint, histogram[bin]))
    print('wrote histogram to %s' % hist_out_fname)
    print('you could plot in gnuplot with the command')
    print('plot %s using 1:2 with boxes' % hist_out_fname)

if __name__ == '__main__':
    main()
