#! /bin/sh

echo "running: $0"
echo "args: $*"

## prepare the data
#wget --continue https://raw.githubusercontent.com/rmcelreath/rethinking/master/data/Howell1.csv
echo "Howell1.csv: ;	wget --continue https://raw.githubusercontent.com/rmcelreath/rethinking/master/data/Howell1.csv" | make -f-
echo "%.hist: % ;	./make-height-histogram.py $<" | make -f- Howell1.csv.hist
#./make-height-histogram.py Howell1.csv
grep ';0$' Howell1.csv > females.csv
grep ';1$' Howell1.csv > males.csv
./make-height-histogram.py females.csv
./make-height-histogram.py males.csv

# ## make all plots from .gp files
# for gp_fname in *.gp
# do
#     svg_fname=`echo $gp_fname | sed 's/gp/svg/'`
#     echo $gp_fname ' -> ' $svg_fname
#     gnuplot -e "set terminal svg; set output '$svg_fname'" $gp_fname
# done
