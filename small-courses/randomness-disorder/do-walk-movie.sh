#! /bin/sh

cd `dirname $0`

n_walk_pts=1000000
walk_output_fname=walk-${n_walk_pts}.dat
n_frames=500

if [ ! -f $walk_output_fname ];
then
    /bin/rm -f walk_frame-??????.png
    ./walk.py $n_walk_pts > $walk_output_fname
fi

## generate all the frames
./walk-movie.py $walk_output_fname $n_frames
## make the movie from the frames
mkdir -p ../_static
echo "../_static/walk-movie.mp4: walk-movie.py do-walk-movie.sh ;	ffmpeg -y -framerate 24 -i walk_frame-%06d.png ../_static/walk-movie.mp4" | make -f-
echo "../_static/walk-movie.mpg: walk-movie.py do-walk-movie.sh ;	ffmpeg -y -framerate 24 -i walk_frame-%06d.png ../_static/walk-movie.mpg" | make -f-
echo "../_static/walk-movie.ogg: walk-movie.py do-walk-movie.sh ;	ffmpeg -y -framerate 24 -i walk_frame-%06d.png ../_static/walk-movie.ogg" | make -f-
# ffmpeg -y -framerate 24 -i walk_frame-%06d.png ../_static/walk-movie.mpg
# ffmpeg -y -framerate 24 -i walk_frame-%06d.png ../_static/walk-movie.mp4
# ffmpeg -y -framerate 24 -i walk_frame-%06d.png ../_static/walk-movie.ogg
