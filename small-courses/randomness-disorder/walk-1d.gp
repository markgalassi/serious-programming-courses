unset xlabel
unset ylabel
set terminal pdf
set output 'walk-path-1d.pdf'
set multiplot layout 2,2
set grid
set size square
plot 'walk-1d-100.dat' using 2 with lines title '100 steps'
plot 'walk-1d-1000.dat' using 2 with lines title '1000 steps'
plot 'walk-1d-10000.dat' using 2 with lines title '10000 steps'
plot 'walk-1d-100000.dat' using 2 with lines title '100000 steps'
reset
unset multiplot
unset xlabel
unset ylabel
set terminal png
set output 'walk-path-1d.png'
set multiplot layout 2,2
set grid
set size square
plot 'walk-1d-100.dat' using 2 with lines title '100 steps'
plot 'walk-1d-1000.dat' using 2 with lines title '1000 steps'
plot 'walk-1d-10000.dat' using 2 with lines title '10000 steps'
plot 'walk-1d-100000.dat' using 2 with lines title '100000 steps'

reset
unset multiplot
unset xlabel
unset ylabel
set terminal svg
set output 'walk-path-1d.svg
set multiplot layout 2,2
set grid
set size square
plot 'walk-1d-100.dat' using 2 with lines title '100 steps'
plot 'walk-1d-1000.dat' using 2 with lines title '1000 steps'
plot 'walk-1d-10000.dat' using 2 with lines title '10000 steps'
plot 'walk-1d-100000.dat' using 2 with lines title '100000 steps'

quit
