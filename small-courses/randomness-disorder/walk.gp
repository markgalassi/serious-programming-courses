unset xlabel
unset ylabel
set terminal pdf
set output 'walk-path.pdf'
set multiplot layout 2,2
set grid
set size square
plot 'walk-100.dat' using 2:3 with lines title '100 steps'
plot 'walk-1000.dat' using 2:3 with lines title '1000 steps'
plot 'walk-10000.dat' using 2:3 with lines title '10000 steps'
plot 'walk-100000.dat' using 2:3 with lines title '100000 steps'
reset
unset multiplot
unset xlabel
unset ylabel
set terminal png
set output 'walk-path.png'
set multiplot layout 2,2
set grid
set size square
plot 'walk-100.dat' using 2:3 with lines title '100 steps'
plot 'walk-1000.dat' using 2:3 with lines title '1000 steps'
plot 'walk-10000.dat' using 2:3 with lines title '10000 steps'
plot 'walk-100000.dat' using 2:3 with lines title '100000 steps'
quit
