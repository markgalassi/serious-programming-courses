#! /bin/sh

cd `dirname $0`

for N in 100 1000 10000 100000
do
    ./walk.py $N > walk-${N}.dat
done

gnuplot walk.gp
