#! /bin/sh

cd `dirname $0`

for N in 100 1000 10000 100000
do
    ./walk-1d.py $N > walk-1d-${N}.dat
done

gnuplot walk-1d.gp
