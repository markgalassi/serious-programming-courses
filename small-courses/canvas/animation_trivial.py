#! /usr/bin/env python3

"""simple animation of a growing/contracting disc
"""

import time
import math

## we use the tkinter widget set; this seems to come automatically
## with python3 on ubuntu 16.04, but on some systems one might need to
## install a package with a name like python3-tk
from tkinter import *

canvas_width = 640
canvas_height = 480

def main():
    ## prepare a basic canvas
    root = Tk()
    w = Canvas(root, 
               width=canvas_width,
               height=canvas_height)
    w.pack()       # boiler-plate: we always call pack() on tk windows
    for i in range(24*180):     # 3 minutes if it's 24 frames/sec
        radius = 140 + 50*math.sin(i/24.0) # oscillating radius
        center = (canvas_width/2, canvas_height/2)
        color = 'blue'
        ## clear the canvas and then draw a new disc
        w.delete('all')
        w.create_oval(center[0]-radius, center[1]-radius,
                      center[0]+radius, center[1]+radius,
                      fill=color)
        ## update the canvas
        w.update()
        time.sleep(1.0/24.0)    # 24 frames per second
    mainloop()

main()
