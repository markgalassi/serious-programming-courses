set grid
set title 'Damped harmonic oscillator: displacement versus time and damping.'
set xlabel 't (seconds)'
set ylabel 'damping c'
set zlabel 'x(t) (cm)'
set pm3d
set hidden3d
set view 20, 60     ## set the viewpoint
splot 'damped-oscillator-surface.dat' using 1:2:3 with lines title ''
