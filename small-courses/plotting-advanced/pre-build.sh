#! /bin/sh

echo "running: $0"
echo "args: $*"

## generate the damped oscillator columns
echo "damped-oscillator-columns.dat: damped-oscillator-columns.py ;	python3 \$< > \$@" | make -f-

## generate the damped oscillator surface data
echo "damped-oscillator-surface.dat: damped-oscillator-surface.py ;	python3 \$< > \$@" | make -f-

echo "violin-F.mp3: ;	wget --continue http://freesound.org/data/previews/153/153595_2626346-lq.mp3 -O \$@" | make -f-
# wget -q --continue http://www.vibrationdata.com/white1.mp3 -O white-noise.mp3
# wget -q --continue http://freesound.org/data/previews/153/153587_2626346-lq.mp3 -O violin-A-440.mp3
echo "tuningfork440.mp3: ;	wget -q --continue http://www.vibrationdata.com/tuningfork440.mp3" | make -f-
# wget -q --continue http://www.galassi.org/course-samples/scientific/guitar-D.dat
# wget -q --continue http://www.galassi.org/course-samples/scientific/guitar-A.dat
# wget -q --continue https://upload.wikimedia.org/wikipedia/en/e/ea/Angels_We_Have_Heard_on_High%2C_sung_by_the_Mormon_Tabernacle_Choir.ogg -O gloria.ogg
# wget -q --continue http://he3.magnatune.com/music/Voices%20of%20Music/Concerto%20Barocco/21-Pachelbel%20Canon%20In%20D%20Major%20-%20Johann%20Pachelbel%20-%20Canon%20And%20Gigue%20For%20Three%20Violins%20And%20Basso%20Continuo%20In%20D%20Major-Voices%20of%20Music_spoken.mp3 -O Canon.mp3
# wget -q --continue http://www.vibrationdata.com/Kawai_Major_Third_A_C_sharp.mp3
# wget -q --continue http://www.vibrationdata.com/chime5.mp3

# echo "%.aif: %.ogg ;	ffmpeg -i \$< \$@" | make -f- gloria.aif
# echo "%.aif: %.mp3 ;	ffmpeg -i \$< \$@" | make -f- violin-F.aif violin-A-440.aif tuningfork440.aif Canon.aif white-noise.aif chime5.aif Kawai_Major_Third_A_C_sharp.aif
echo "%.aif: %.mp3 ;	ffmpeg -i \$< \$@" | make -f- violin-F.aif tuningfork440.aif

OFFSET=0
MAXLINES=100000
HEAD_NUM=`expr $OFFSET + $MAXLINES`
echo $OFFSET
echo $MAXLINES
echo $HEAD_NUM
# echo "%.dat: %.aif ;	sox \$< -t dat - | head -${HEAD_NUM} | tail -${MAXLINES} > \$@" | make -f- violin-F.dat violin-A-440.dat tuningfork440.dat Canon.dat white-noise.dat gloria.dat chime5.dat Kawai_Major_Third_A_C_sharp.dat
echo "%.dat: %.aif ;	sox \$< -t dat - | head -${HEAD_NUM} | tail -${MAXLINES} > \$@" | make -f- violin-F.dat tuningfork440.dat

# echo "%_spec.svg: music2spectrogram.py %.dat ;	python3 \$^ \$@" | make -f- violin-F_spec.svg violin-A-440_spec.svg tuningfork440_spec.svg Canon_spec.svg white-noise_spec.svg gloria_spec.svg guitar-A_spec.svg guitar-D_spec.svg chime5_spec.svg Kawai_Major_Third_A_C_sharp_spec.svg
echo "%_spec.svg: music2spectrogram.py %.dat ;	python3 \$^ \$@" | make -f- violin-F_spec.svg tuningfork440_spec.svg
echo "%_spec.pdf: music2spectrogram.py %.dat ;	python3 \$^ \$@" | make -f- violin-F_spec.pdf tuningfork440_spec.pdf
# echo "%_spec.png: music2spectrogram.py %.dat ;	python3 \$^ \$@" | make -f- violin-F_spec.png tuningfork440_spec.png
