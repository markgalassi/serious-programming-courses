
.. _chap-machine-learn:

======================================
 Getting started with Machine Learning
======================================


.. sectionauthor:: Christopher Mendoza <mendozachristopher0828@gmail.com>


..
   Motivation, prerequisites, plan
   ===============================

   
In this chapter, we will learn the basics when it comes to machine learning. To start off we will look at *PyTorch*, a machine learning library. Pytorch is mainly used for a variety of different applications but is primarily used for computer vision and natural language processing. Pytorch is also a framework that should be explored when looking towards deep learning. For this guide we will learn about how it works and set up a simple model.

In order to first do this make sure that you fulfill the following prerequisites.

* The 10-hour "serious programming" course
* Installing the required packages using pip.


Since for this guide we'll be using pip to install everything, the first thing to do is first we need to have pip installed, you can use the following line:

  .. code:: console

 	$ sudo apt install python3-pip
	 

To actually install Pytorch you will have to run a command that is based off of the device you are using. For my device as well as the program it will work on Linux, through the Pip package, and Python as well as run on the CPU. When working with Pytroch I could only work with the one that worked on my CPU so that is when I have in this command line. The Pytorch Build which I chose was Stable (2.3.1). Here is my command.

.. code-block:: console
   	 
   $ pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cpu

If you do not have the same device type as the one I am using then head over to the pytorch website and select the prerequisites in order to download the correct version for your device by clicking on the following link:

`Pytorch Website <https://pytorch.org/get-started/locally/>`_

Once you are done with that you are all set with Pytorch, for this program you will also visualize some data so you also need to install matplot which with pip can be done using the following command:

.. code:: console
 	 
   $ sudo apt-get install python3-matplotlib

For our demo lets install one more thing which is NumPy, in the command line run the following:

.. code:: console
 	 
   $ pip install NumPy

Now you are ready to begin this course! The learning objectives will be:

#. The data structure behind Pytorch
#. Working with already made data
#. Creating a small neural network

With all the packages downloaded on our machine, let's learn a little about its structures.


What is behind Pytorch?
=======================
In your favorite text editor, create a new Python file named "testPytorch.py". Inside that file import the necessary libraries, for this demo you will need to import the torch library so at the start of the file insert the following:

.. code:: console
   	 
   $ import torch

Now we can look at a crucial data structure for Pytorch, which are tensors.

Working with tensors
--------------------

Tensors are extremely similar to the arrays that we have used in Python to hold values, they are also similar to matrices, the use of tensors is to perform mathematical operations on large sets of data. We can view a tensor with the following code. Remember that for this code you want to paste it into that `testPytorch.py` file:

.. code-block:: python

   x = torch.empty(3, 4)
   print(type(x))
   print(x)


You can then run your file with the following command line:

.. code-block:: bash
   	 
   python3 testPytorch.py


For your output you should see a 2-dimensional tensor with 3 rows and 4 columns. For the most part tensors work with different values such as 0, and 1 and everything in between so lets create those as well with the following code:

.. code-block:: python
   	 
   zeros = torch.zeros(2, 3)
   print(zeros)

   ones = torch.ones(2, 3)
   print(ones)

   torch.manual_seed(1729)
   random = torch.rand(2, 3)
   print(random)

 

Now we have an additional 3 more tensors, one populated with zeros, another with ones, and the last one with random integers between zero and one. We can also very easily use NumPy to create these tensors, NumPy is a Python library which focuses on creating large arrays and matrices as well as helping with mathematical operations between those data structures. At the top of the file import the following:

.. code-block:: python
   	 
   import numpy as np


You can comment out everything we have so far using the *#* symbol but leave everything that we have imported. Create the tensor using NumPy with the following:

.. code-block:: python
   	 
   ndarray = np.array([0, 1, 2])
   t = torch.from_numpy(ndarray)
   print(t)




Now that you have made a tensor with Pytorch as well as with NumPy, we can look at the different attributes of tensors which include the shape, data type, as well as the device. Since we initialized *t* with the tensor, you can check the attributes with the following lines of code:

.. code-block:: python
   	 
   print(t.shape)
   print(t.dtype)
   print(t.device)
   

Lastly we can do some operations with tensors such as transposing tensors, multiplying them, and indexing the tensors. We can see these operation in action by first creating a tensor filled with zeroes with the following line:

.. code-block:: python
   	 
   zeros_tensor = torch.zeros((2, 3))
   print(zeros_tensor)

 
When printing it out we can see the tensor is populated with zeros, lets transpose it with the following line!:


.. code-block:: python
 	 
   transposed = zeros_tensor.T
   print(transposed)

After printing this out we can see that now our tensor is transposed, this moves the data around in the tensor. Now we can go ahead and pull out a certain index that we want from this transposed tensor, for example with the second index with the following line:


.. code-block:: python
 	 
   print(transposed[2])
   #Lets get another index
   print(transposed[:,0])



Now with the *zeros_tensor* variable that we already created, lets create a tensors filled with 1's that is three by three. Try to do this by yourself first! If you can't use the following line to create the ones_tensor and then lets create a initialize a product variable that multiplies these two tensors:

.. code-block:: python
   	 
   ones_tensor = torch.ones(3, 3)
   product = torch.matmul(zeros_tensor, ones_tensor)
   print(product)

The main purpose of tensors are to hold a lot of data, which when it comes to machine learning we need a lot of. You can visualize it with the following picture as well:



.. image:: visual-tensor.png
   :width: 500
   :alt: Visualizing tensors from https://medium.com/@anoorasfatima/10-most-common-maths-operation-with-pytorchs-tensor-70a491d8cafd
   :align: center



Now that we have had some context to machine learning, we can start building a small nueral network.



Creating a small neural network
===============================
.. rubric:: Working with data

To start off our program we want to use all the different libraries that we will be putting to use. These include all the different torch libraries, as well as data sets and matplotlib in order to visualize it.

.. code-block:: python
   	 
   import torch
   from torchvision import datasets
   from torchvision.transforms import ToTensor
   import matplotlib.pyplot as plt



In the next chunk of code, this is a simple way of loading in already created data from the Pytorch library. When first running the program you will be running these two lines and downloading data.

.. code-block:: python

   training_data = datasets.MNIST(root=".", train=True, download=True, transform=ToTensor())

   test_data = datasets.MNIST(root=".", train=False, download=True, transform=ToTensor())

   
These are datasets inside the Pytorch library that you can see will be downloaded on your device. After that is done with the code, create some new lines and add the following line:

.. code-block:: python
		
   print(training_data[0])

From the output you will see that it prints out a tensor, like we have talked about earlier in this guide. This is a huge multidimensional tensor with different data, this data meaning images. This is now data that has been downloaded from the torch library which we can use to visualize that data. For the next chunk of code, that will be used to visualize the data:

.. code-block:: python
   	 
   figure = plt.figure(figsize=(8, 8))
   cols, rows = 5, 5

   for i in range(1, cols * rows + 1):
   	sample_idx = torch.randint(len(training_data), size=(1,)).item()
   	img, label = training_data[sample_idx]
   	figure.add_subplot(rows, cols, i)
   	plt.axis("off")
   	plt.imshow(img.squeeze(), cmap="gray")
   plt.show()

   



First we create the figure and then we iterate throughout its columns and rows. We also add the subplot in order to be able to visualize the data. Now after running the code you can visualize a plot of data that are different numbers. This is a simple introduction to using data that Pytorch already has available.


.. image:: data-number-output.png
   :width: 500
   :alt: Visualizing tensors from https://medium.com/@anoorasfatima/10-most-comon-maths-operation-with-pytorchs-tensor-70a491d8cafd
   :align: center


Exercise: Playing with the data!
--------------------------------

     	 
As we saw from the previous code, we can already visualize these tensors and
   we see numbers! Now let's play around with this. Not only can you change the
   color of the background and letters but you can also change the data to visual
   ize! Go back to the lines we mentioned before where we download the data.Bef
   re "MNST" type fashion so that you changed those lines to be the following:


.. code-block:: python
      	 
   training_data = datasets.MNIST(root=".", train=True, download=True, trans
   orm=ToTensor())

   test_data = datasets.MNIST(root=".", train=False, download=True, transfor
   =ToTensor())

   #Run the program again and see what you get! As a result

Look for other resources online with pytorch that you can mess around and visualize with, make sure to leave it the way it was to continue with our code!


Small neural networks continue...
=================================

Moving on we want our focus now to be on a neural network, in order to do that we need to be able to chunk up the data into batch sizes. We do the following with:

.. code-block:: python
   	 
   from torch.utils.data import DataLoader
   
   #We can see here that the data is being bunched up into different batches to
   train
   loaded_train = DataLoader(training_data, batch_size=64, shuffle=True)
   loaded_test = DataLoader(test_data, batch_size=64, shuffle=True)

What we have been doing so far is passing the data through parameters just as tensors. In order to actually train our model it is best to package our data into batches. Now we can work on our simple neural network class:

.. code-block:: python
		
   from torch import nn

   class NeuralNetwork(nn.Module):
   	def __init__(self):
       	super(NeuralNetwork, self).__init__()
       	self.flatten = nn.Flatten()
       	self.linear_relu_stack = nn.Sequential(
           	nn.Linear(28*28, 512),
           	nn.ReLU(),
           	nn.Linear(512, 512),
           	nn.ReLU(),
           	nn.Linear(512, 10),
       	)
  	 
There are several things to note here:

* We use ``nn.Flatten()`` in order to transform the data that we have been working with from being multidimensional (remember tensors) into a one dimensional tensor
* After we see that we use ``nn.Sequential`` to create different layers of order within the network. We are created specific layers inside this network (but you can create different layers as well)

Inside our class we should also define the following function:

.. code-block:: python
   	 
   def forward(self, x):
    	x = self.flatten(x)
	#Relu refers to rectified linear unit which is an activation function
    	logits = self.linear_relu_stack(x)
    	return logits
    
In summary the forward function defines the way that the data flows throughout the network. It first:

#. Flattens the data
#. Passes the linear layers through the use of relu
#. Returns the output

Training our model
------------------
When it comes to neural networks, we need two fundamental components which work together in order to help our model. This is a `loss function` and `optimizer`. The purpose of the loss function is to measure how well our model is performing. For our optimizer we use that in order to optimize and make the training of our model more efficient. For this model we will be using the following lines to fill those conditions:

.. code-block:: python
		
   loss_function = nn.CrossEntropyLoss()
   optimizer = torch.optim.SGD(model.parameters(), lr=0.001)

Loss function:

* We are using CrossEntropyLoss, a loss function which measures the difference
  that there is between the discovered probability distribution and the predict
  ed values


Optimizer:

* For this code we are using the SGD (stochastic gradient descent) which is used to train models efficiently.
 
* NOTE: later we will be running an exercise in which we can use different optimizers and see what is best to use.

Now it's time to finally train the model in order to work on its prediction. We can implement it using the following training function

.. code-block:: python
		
   def train(dataloader, model, loss_fn, optimizer):
	size = len(dataloader.dataset)
	for batch, (X, y) in enumerate(dataloader):
    	pred = model(X)
    	loss = loss_fn(pred, y)

    	optimizer.zero_grad()
    	loss.backward()
    	optimizer.step()

    	if batch % 100 == 0:
        	loss, current = loss.item(), batch * len(X)
        	print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")


In this block of code we are simply iterating through the data one batch at a time. We are then passing it through our prediction model as well as our optimizers. At the end we are converting the `loss.item()` to convert the loss tensor to a float value and then print that out to see what are loss is when training


Now finally we can move on to our test function:

.. code-block:: python
		
   def test(dataloader, model, loss_fn):
	size = len(dataloader.dataset)
	num_batches = len(dataloader)
	test_loss, correct = 0, 0

	#This torch.no_grad is used in order to wrap up the code, it also ensur
	es that proper gradient calculations occur
	with torch.no_grad():
    	for X, y in dataloader:
        	pred = model(X)
        	test_loss += loss_fn(pred, y).item()
        	correct += (pred.argmax(1) == y).type(torch.float).sum().item()

	test_loss /= num_batches
	correct /= size
	print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {tes
	t_loss:>8f} \n")

This function first iterates through the test data and computes the prediction as well as accumulates the loss and checks which predictions are correct. We can then print out how accurate our model is.

Lastly all we need to do is set up how many times we are training the model. When it comes to machine learning and training models we use epochs, an epoch is when all the training data is used at one. In this case we are using five. Lets also set up how exactly our data will be printed out:


.. code-block:: python
		
   epochs = 5
   for t in range(epochs):
   	print(f"Training Epoch {t+1}\n===================")
   	train(loaded_train, model, loss_function, optimizer)
   	test(loaded_test, model, loss_function)
   print("Model has been trained")

   
Fantastic, now you have created your own neural network!


Exercise: Further things to try
-------------------------------
Now that you have finished your small neural network by using data as well as Pytorch, try some of the following things to learn more!

#. Try changing the amount of epochs that you use to see if the model is able to have better predictions!
#. When it comes to optimizers, we can also use different ones to see if it is possible for our model to do better. Use the following `optimize Pytorch Website <https://pytorch.org/docs/stable/optim.html/>`_ which focuses on different optimizers that are able to be used. 
#. Check out our other chapters to learn more such as with :numref:`chap-web-scraping` and see if you can go about applying web scraping to train a model!

