#! /bin/sh

echo "running: $0"
echo "args: $*"

## convert the native .svg files to .pdf for latex output
echo "%.pdf: %.svg ;	rsvg-convert -f pdf -o \$@ \$<" | make -f- Integral_approximations.pdf
echo "%.pdf: %.svg ;	rsvg-convert -f pdf -o \$@ \$<" | make -f- Integral_example.pdf
