#! /usr/bin/env python3

import math
import matplotlib.pyplot as plt
import numpy as np

def main():
    a = -1.3
    b = 3.1
    func = curvy_cubic
    # a = -1
    # b = 1
    # func = upper_semicircle

    fig = plt.figure()
    for n_rectangles in range(5, 200, 3):
        do_single_integration(fig, func, a, b, n_rectangles)
    plt.waitforbuttonpress()

def do_single_integration(fig, func, a, b, n_rectangles):
    x = np.arange(a, b, 1.0/n_rectangles)
    fig.clear()
    plt.grid(True)
    plt.plot(x, func(x), color='g')
    A = integrate_function(func, a, b, n_rectangles)
    info_para = """func: %s
n_rectangles: %d
area: %g
""" % (func.__name__, n_rectangles, A)
    plt.annotate(
        info_para,
        xy=(0, -1), xytext=(0, -2))
    print(info_para)
    fig.canvas.draw_idle()
    plt.pause(0.8)

def integrate_function(f, a, b, n_rectangles):
    width = (b - a) / n_rectangles
    total_area = 0
    for i in range(n_rectangles):
        left_point = a + i*width
        right_point = a + (i+1)*width
        midpoint = a + i * width + width/2
        height = f(midpoint)
        area = width * height
        total_area += area
        # now plot that rectangle; we must renormalize all to be in
        # the [0,1] range for both x and y
        if f(midpoint) < 0:
            color = 'r'
        else:
            color = 'b'
        norm_left = left_point
        plt.bar([midpoint], height, width=width, edgecolor=color, fill=False)
    return total_area

def curvy_cubic(x):
    return (x-1)**3 - 3*(x-1) + np.sin(x)

# Note that this function, when integrated from -1 to 1, should give
# an area of pi/2
def upper_semicircle(x):
    return np.sqrt(1 - x*x)

main()
