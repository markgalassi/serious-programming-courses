.. _chap-numerical-integration:

***********************
 Numerical integration
***********************

[status: enough written to run a course, but incomplete]

The integral
============

The *integral* of a function is the "area under the curve" of that
function between two given points a and b.  We write it in this way:

.. math::
   :label: integral-notation

   \int_a^b f(x) dx

A picture that illustrates this is in the `Wikipedia article on
integrals <https://en.wikipedia.org/wiki/Integral>`_ which I reprodue
here in :numref:`fig-integral-example`.

.. _fig-integral-example:

.. figure:: Integral_example.*
   :width: 60%

   The integral is the area under the curve of a function, between two
   points a and b.

Other written tutorials explain integrals quite well, but the figure
above allows an instructor to explain it at a blackboard or
whiteboard.

For now remember a couple of things: the integral is an area, and
notice the beautiful notation in :eq:`integral-notation`.

Integrals come up a lot in science.  In Physics, to example, *work* is
defined as the integral of force over a distance.  This way of
describing of work then leads to defining a body's *energy*.
*Electric flux* is the integral of the electric field over a surface
(a 2-dimensional integral, but the same idea).  A simple application
in pure math is to calculate the areas of certain figures and the
volumes of some solids.

Integrals are related to derivatives by the *fundamental theorem of
calculus*, developed in the 17th century and based on work by
Archimedes in ancient Greece.  I will not touch on this too much at
this time: it's possible to explain derivatives and integrals to kids,
but explaining the "antiderivative" might be out of our reach.  We
will just occasionally mention little bits of information about the
link.


Calculating the integral numerically
====================================

It rare that we can calculate an integral exactly, but we can write
computer programs that approximate the integral of any function.  In
:numref:`fig-integral-example` we see one way of doing it by breaking
the area down into many small rectangles.  The area is then close to
the sum of the areas of those small rectangles.

.. _fig-integral-approximations:

.. figure:: Integral_approximations.*
   :width: 60%

   Approximating the area under the curve by making many small
   rectangles that try to fit the curve.

:numref:`fig-integral-example` shows how we can break that region of
space into a bunch of rectangles.  The area of all those rectangles
will approximate the integral of our function. If you insert more
rectangles, they will snuggle up to the curve even better.

The drawing in :numref:`fig-integral-approximations` lets us explain
the notation in Equation :eq:`integral-notation` :math:`\int_a^b f(x)
dx`.  Each little rectangle in the figure has a base of :math:`dx`,
and a height of :math:`f(x)`.  The area of that little rectangle is
height :math:`\times` base, which is :math:`f(x) dx`.  Now think of
:math:`\int` as a fancily drawn letter S and you see that the notation
in Equation :eq:`integral-notation` reads as "the sum of :math:`f(x)
dx` for x between a and b", which means "the sum of the areas of all
the baby rectangles between a and b".

The key insight is then to notice that in the *limit* (yes! it's
limits again!) of an increasing number of smaller and smaller
rectangles we get the the integral.

Let us write a program which calculates the area under a curve using
this approximation.  Students are encouraged to think for a while
before looking at :numref:`listing-integrate-with-rectangles-py`.  I
spend some time devising an algorithm with the students.  I typically
go to the whiteboard and, interactively with the students, I point out
that we need:

* A loop over the number of rectangles.

* A variable with the area we have accumulated so far.  (This is a
  standard paradigm in many programs: a variable that starts at zero,
  and you add stuff to it as you compute it.)

* Identifying the lower left and right corners of each baby rectangle.

* Finding the upper left and right corners of each baby rectangle.

* Finding the height of the rectangle using :math:`f(x)`.

* Taking that individual rectangle's area.

* Adding that rectangle's area to the sum variable.

Now let us write the program in
:numref:`listing-integrate-with-rectangles-py` program and run it.  It
will report the area it found.  If you put few rectangles you will get
a worse approximation, and if you put in more rectangles you get a
better approximation.

.. _listing-integrate-with-rectangles-py:

.. literalinclude:: integrate-with-rectangles.py
   :caption: integrate-with-rectangles.py - approximate the area under
             a curve with a collection of small rectangles that fit
             under that curve.

You can modify ``integrate-with-rectangles.py`` to use a curvy
function ``curvy_cubic()`` or to use the ``upper_semicircle()``
function.  The latter is interesting because the integral under the
upper semicircle should be half the area of a circle with that radius:
:math:`\pi r^2`.  In our case the radius is 1, so we expect that
:math:`\int_{-1}^{1} {\rm upper\_semicircle}(x) dx = \pi/2`.  This
means that we can use our integration program to calculate
:math:`\pi`!

But ``integrate-with-rectangles.py`` was a rather dry program: it is
simple to write and understand, it gives the answer, but it does not
carry us on a wave of enthusiasm.

So let us see if we can make an animation of the process of this
approximation.  Enter the program in
:numref:`listing-integrate-with-rectangles-and-plot-py`.  This version
shows the plot of the curve as well as the subdivision of the area
into rectangles, and it gives us an update on the estimate of the
area.

.. _listing-integrate-with-rectangles-and-plot-py:

.. literalinclude:: integrate-with-rectangles-and-plot.py
   :caption: integrate-with-rectangles-and-plot.py - calculate area
             with the same approximation in
             :numref:`listing-integrate-with-rectangles-py` but also
             draw an animation of the rectangles as we do it.

Run the program and pay close attention to the ongoing calculation of
the area and see if it converges as you get more and more rectangles.


Improving the numerical approximation
=====================================

If you imagine using *trapezoids* instead of rectangles in
:numref:`fig-integral-approximations` you will notice that they hug
the curve more closely.  The area of a trapezoid is straightforward to
calculate (I usually get the students to work it out while I write
their ideas on the whiteboard).  This method is called the
*trapezoidal rule*.

.. exercise:: trapezoidal rule

   Modify the program in
   :numref:`listing-integrate-with-rectangles-and-plot-py` to use
   *trapezoids* instead of rectangles.  Compare the results to those
   from the rectangular method.

Even better is a method called *Simpson's rule*.  We take three points
along the line and approximate the curve between them with a second
degree polynomial.  The curve then becomes a stitched together
sequence of baby parabolas.  Parabolas hug our function curve even
more closely, so we should get a better approximation.

The procedure for Simpson's rule has three main steps:

#. Break the interval from *a* to *b* into segments which have a left,
   middle and right point.

#. Find the parabolas using techniques similar to those in
   :numref:`sec-polynomial-fits`.

#. We know how to integrate polynomials exactly, so each tiny parabola
   has a known area, for example :math:`\int_a^b x^2 dx =
   (b^3-a^3)/3`.

.. exercise:: Simpson's rule

   Modify the program in
   :numref:`listing-integrate-with-rectangles-and-plot-py` to use
   Simpson's rule.


Stepping back from numerical integration back to analytical work
================================================================

If anyone in the course has studied a bit of calculus we can discuss
how to integrate the curvy cubic and the semicircle analytically.
