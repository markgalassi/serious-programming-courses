:tocdepth: 2

.. _app-guest-lecture-itinerary:

===========================================
 Appendix: An itinerary for guest lectures
===========================================

This appendix shows some examples of possible guest lectures which
would show some of the material in this book.  It is mostly meant to
provide links


Motivation for linking computing and scholarship
================================================

Personal story
--------------

* 1980 and the first personal computres
* 1983 and the Reed College physics department requiring that we learn
  to program
* 1983 and David Basin trading: he teaches me C and UNIX if I teach
  him to play guitar
* 1985 and Nerdix
* 1985 and watching Keith Packard hack
* Realizaing that others did not get this level of computing
  instruction
* 1992 and an impossible physics thesis thanks to software
  sophistication

Software freedom
----------------

The only reason I have enjoyed my career as a physicist is because of
the existence of software freedom.

What can be done in various disciplines
---------------------------------------

Although I have not seen it represented graphically, there is a
timeline 

How a physicist-hacker looks at things
--------------------------------------

The glow worms in New Zeland.

Constellations in the random number calculation of pi.


A tour of topics
================

The history and calculation of pi
---------------------------------

:numref:`chap-special-numbers-pi`


Generating music
----------------

To give this presentation it is useful to first have the students
install the sox utilities on their own computers.

:numref:`chap-music-basics`

* https://www.dataquest.io/blog/free-datasets-for-projects/
