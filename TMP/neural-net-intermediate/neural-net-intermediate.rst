.. _chap-neural-net-intermediate:

=============================================
[Advanced Topic] Intermediate Neural Networks
=============================================

.. sectionauthor:: Malcolm Smith <msmith.malcolmsmith@gmail.com>

[status: first draft]


Motivation, Prerequisites, Plan
===============================

.. rubric:: Motivation

In the last neural networks chapter, a genetic algorithm was used to
fit a neural net to polynomial data. This is a great way to glean a
basic understanding of neural networks, but it's really not that
efficient. Instead, we will learn the method that every A.I. you have
ever used was trained on: backpropagation. The goal for this chapter
is to give you an understanding of how backpropagation works, why it's
used for what it's used for, and why it's more effective than the
genetic algorithm. In addition, we learn how to use python classes,
which are a vital tool for any python programmer.

.. rubric:: Prerequisites

* The 10-hour "serious programming" course.
* The "Neural Network Basics" chapter in :numref:`chap-neural-net-basics`.
* Having the required libraries installed. You can install them with:
		
.. code-block:: console

		$ sudo apt install python3-tk pip
		$ pip install --user --upgrade networkx[default] matplotlib numpy

.. rubric:: Plan

The plan for this mini-course is to briefly go over what
backpropagation is, as well as some of the simpler required math. We
won't go into too much detail here, as this algorithm involves fairly
complex calculus. Next, we'll go over how to implement it for
polynomial data, as a trivial example, then move on to building a
model that can recognize hand-drawn digits. Then, we will implement a
system to input our own digits, utilizing the Tkinter library.

Neural Nets Refresher
=====================

In the previous chapter, we introduced the concept of neural
networks, which are more or less a function. They have inputs and
outputs, and for each input there is exactly one output. However, they
are much more complex than your average algebraic function. Their
structure is in layers, with the first and last being the input and
output, respectively. Before explaining any more, let us look at a
picture:

.. _fig-simple-neural-net:

.. figure:: simple-net.svg

	    Basic structure of a neural network.

As you can see, every neuron in one layer (those dots are the neurons)
are connect to every neuron in the next with lines. We call those
lines edges, and they all have something called weight, which is how
much the neuron on one end affects the other end. What is it affecting
in the other neuron? It's the activation, which is a measure of how
"on" the neuron is. The exception to this rule is the neurons along
the top, known as bias neurons. They always have an activation of one,
and allows the net to add a fixed value to any neuron regardless of
the input. We will explore them more in depth later on.

Intro to Backpropagation
========================

So far we have been using a genetic algorithm to update the net. This
works and is easy to understand, but it is not the most efficient
method. What large-scale neural nets use is known as backpropagation,
which is somewhat tricky to understand. Basically, it will go through
the network backward after a given iteration, and figure out how each
individual activation affected the final RSS. Then, it'll use that
information to figure out which way and how much each weight should be
changed to change all those activations in the proper
directions. Understanding all the theory and math behind this requires
knowledge equivalent to at least calculus III, so we will skip that
and go from a high-level description to the method of
implementation. If you want a slightly more in-depth explanation,
3Blue1Brown (the same channel we used last time) has a great video on
the topic `here <https://www.youtube.com/watch?v=Ilg3gGewQ5U>`_.

The basic structure of the backpropagation algorithm is in two parts:
calculating neuron errors and adjusting the weights. The first part
can be accomplished by doing some manipulation of various values, such
as the weights, activations, and errors of other neurons. The general
formula can be found on line 46 of :numref:`lis_backprop_genetic`. The
second part involves the errors, as well as something called the
learning rate. The learning rate is a number that controls how fast
the net's weights can change. For our purposes, it will likely be
between 0.1 and 0.001. The way it calculates the amount to change the
weight is fairly simple; it multiplies the error of the neuron at the
end of the edge by the activation of the neuron at the start, then
multiplies all of that by the learning rate. This can be understood as
"what is wrong with the node we affect" * "the amount we can affect
that node" * "some constant". This algorithm is much faster than the
genetic algorithm, though it has some trouble fitting close to the
endpoints.

.. _lis_backprop_genetic:

.. literalinclude:: simple_backpropagation.py
   :language: python
   :caption: first_backpropagation.py - the fastest and best method for training neural nets

There's a lot going on here, but the first line is worth looking at:
if you are doing this in a different directory from the last neural
nets chapter, or deleted the helper functions file, you'll need a new
one. You can find the code in :numref:`lis-NN-helper-functions` in the
block of code titled "helper_functions.py". Some of the new program is
directly copied from the genetic algorithm code from last time, but
most of it is new. The function ``backpropagate()`` has all the
horrible math; when you look through it be aware of that. With that
being said, here's what this function can produce:

.. _fig_backpropagation_fit:

.. figure:: first_backprop_render.*

	    An OK fit.

Something that should strike you right away is that the fit seems to
be less good than in the case of the genetic algorithm. There are a
couple reasons for this, the main one being that neural nets, and
especially backpropagation, aren't designed to fit simple
data. Fitting simple data is a great way to learn them, as a
one-to-one input/output structure can help give an intuition for what
they are and can do. Using a more sophisticated version of this code,
we can actually train a neural net to identify hand drawn digits,
which we'll be doing in the next section.


Extracting the MNIST Data Set
=============================

In the following sections, there will be a lot of extremely
complicated math involving matrices and derivatives. It is ok if you
don't understand it, but you should come away with at least a high
level understanding of how this fits data. Backpropagation is what
almost every A.I. feature you've ever used was trained on. It comes in
many forms, but it can always be traced back to the simplest form,
which is what we will implement here. While the backpropagation
algorithm was the basis of the previous section, it used a somewhat
inefficient method. Manually calculating every multiplication that the
backprop steps needed was resource intensive, and in this section we
will use the numpy library, which is optimized to be able to do large
numbers of multiplications very quickly.

We will be fitting the MNIST data set, which can be found `here
<https://github.com/mrgloom/MNIST-dataset-in-different-formats/tree/master/data/Original%20dataset>`_
or on the wayback machine `here
<http://web.archive.org/web/20201013103734/github.com/mrgloom/MNIST-dataset-in-different-formats/>`_. You
can then download the data with

.. code-block:: console

		$ wget https://github.com/mrgloom/MNIST-dataset-in-different-formats/raw/refs/heads/master/data/Original%20dataset/t10k-images.idx3-ubyte
		$ wget https://github.com/mrgloom/MNIST-dataset-in-different-formats/raw/refs/heads/master/data/Original%20dataset/t10k-labels.idx1-ubyte
		$ wget https://github.com/mrgloom/MNIST-dataset-in-different-formats/raw/refs/heads/master/data/Original%20dataset/train-images.idx3-ubyte
		$ wget https://github.com/mrgloom/MNIST-dataset-in-different-formats/raw/refs/heads/master/data/Original%20dataset/train-labels.idx1-ubyte

This will give you four files with somewhat cryptic names. The ones
with 'labels' in the name are the correct answers, the ones with
'images' are the images (in a weird data format we'll get to in a
second). The ones with 't10k' are the testing data (because there are
10,000 testing examples), and the ones with 'train' are the training
data.

To get the data in a recognizable form, we have to do some
formatting. Conveniently, the site that we downloaded the data from
has a program on it that can unpack the data for
us. :numref:`lis_data_reader` has that program with a few tweaks made
to better suit our needs.

.. _lis_data_reader:

.. literalinclude:: data_reader.py
   :language: python
   :caption: data_reader.py - a class that can extract MNIST data

This is a very dense program, but before we get to what it does, let
us look at the new python feature here. You may notice, towards the
top, that this entire program is actually the definition of something
called a class. A class is a type of object, such as an array, float,
or string. It has attributes and methods, which we will define in a
second. In this case, the data extraction could easily be delegated to
a function, but seeing as classes are used to define basically
everything else we do in this chapter, this is a great first
example. To explain what class is and can do, we will look at the two
basic features of them: attributes and methods. An attribute is just
some sort of value associated with a class object. For example, in the
first function, there are four attributes being defined. Note that, in
this case, all four of them will be strings (because filepaths are
stored as strings in python). Also, inside the definition of a class,
all of its attributes are referred to as ``self.attribute_name``. When
we want to reference it outside the definition, it's called
``variable_name.attribute_name``, where variable_name is whatever we
called that particular object. To understand this, think about talking
about the concept of an array versus an actual array. The former is
when you'd use ``self.`` and the latter, which has a name and elements
and so forth, is when you'd used ``variable_name.``.

Methods are functions specific to a class. They are called with either
``self.`` or ``variable_name.`` and then
``method_name(arguments)``. The distinction between ``self.`` and
``variable_name.`` is covered in the previous paragraph. Like a
function, they have arguments, can execute code, and can return
values. There are a couple special methods, but the only one we will
see is the ``__init__()`` method. Its inputs (excluding ``self`` - we
will get to that) are what is passed in when creating a new class
object. For example, when a new ``MnistDataloader`` object is created,
it requires four values: the filepaths to the four files we downloaded
earlier. This method is called implicitly and will almost never be
called explicitly.

Now that you understand the basics of classes, you should try to write a simple one yourself:

.. exercise::

   Write a simple class that has the ability to print out (in any
   future code) the input given when creating an object of that
   class. (Hint: remember that inputs to a class are inputs to the
   ``__init__()`` method.)

With syntax and a simple example out of the way, let us look at what
the data reading class actually does. Everything here is happening is
one function: ``read_images_labels()`` (this is technically a method,
but calling it a function is also valid). There are two chunks here,
so we will look at each separately. The first three lines of both
(starting with ``open(...``) are simply opening and extracting files,
and getting important data about them (such as the size of the
images). The first block gets the labels, and the next line in that
block defines labels as an array of arrays of length 10, the
importance of which will become clear in a second. It then iterates
through each set of 10 zeros in labels, and sets one of them to a
one. The one it sets is at the index of the actual label. This means
that if the image shows a zero, the first entry is changed, if it
shows a one, the second, etc. This is because the way our neural net
will output is in a ten-node layer where each node represents the
"chance" that the image shows that digit. Thus, the correct answer
should be zero for incorrect choices and 1 for the correct one.

As for the images, the files contain an array that just lists the
pixel values in order. No breaks, we have to put those in
ourselves. To do this, we iterate through the data in chunks equal to
the number of pixels in an image, and then reformat it to be a nested
array where the outer layer contains images, the images contain rows,
and finally the rows contain the actual pixel values. We also rescale
the pixel values to be between zero and one, because it makes the net
run a little smoother later on.

Now that we understand how to get the data into python, let us begin
imagining a method we could use to represent our neural net. We used
NetworkX to gain an understanding of what one is, but that library is
not optimized at all for running the amount of calculations we'll need
to. Instead, we'll use numpy and its matrix multiplication features to
do the backpropagation. To understand how this works, let's look at
what forward propagation entails, which gives us insights into how
backprop might work.

Numpy Neural Nets
=================

An alternative (and more efficient) way to represent weights in a
given layer is as a matrix. If you don't know what a matrix is, it's
simply a grid of numbers, and can be represented in code as a nested
array (for example an array of four arrays, each of which have three
elements). For our purposes, this is how we will sort the weights: for
every neuron at the beginning of the weight layer (meaning the "layer"
between two columns of nodes, which we call the beginning nodes and
end nodes respectively), construct an array of the weights that neuron
connects to. In a fully connected layer, which is what we have, the
length of this array will be the number of neurons at the end of the
layer. If m is the number of input neurons ("input" neuron in this
case referring to layer input, not overall net input), and n is the
number of output neurons (again, layer output, not overall output),
then our weights matrix will be an array of m arrays of length n. From
now on, this will be referred to as an m by n matrix, or m x n matrix.

.. _fig-example-matrix:

.. figure:: MxN_matrix.png

	    A matrix with m rows and n columns.

Now that we have our new weight matrix, how can we use it to evaluate
net output? For this, we need to redefine our activations as
well. Thinking of them as an array works well enough, but for the next
step, it's more helpful to imagine them as a 1 x m matrix. Ok, now we
have new definitions of our activations ``a`` and the weights
``W``. How do we use these to get the next set of activations? To
start, let us consider a given neuron in the current layer's
output. If we don't consider the activation function for now (we will
get to that), the activation should be the sum of all the activations
in the previous layer times the respective weights. The trick now is
finding those weights in our matrix. But if you think about it, there
are m activations, and m arrays in ``W``. Each activation corresponds
to a row in the matrix. And the index in each of those rows will be
the same, namely the position of the neuron we're considering in the
output layer. This works because of the order we put the weights and
activations in; if they'd been done any differently, it would have
been much, much harder to pair activations with weights. After
multiplying all the relevant weights and activations, we simply sum
them up to get the activation of the neuron in the layer output. And
as it happens, numpy has a method that allows us to do the
multiplication and summing for every layer output neuron in one
line. It's the ``np.dot()`` method, which will actually calculate the
entire next activation array in one line. In our case, the specific
phrasing will be ``a2 = np.dot(a1, W1)`` where ``a1`` and ``W1`` are
the same activations and weights we described earlier.

If this is all sounding very complex, don't worry. It is complex. We
will do a couple examples next to hopefully shed some light on parts
you may not understand. Let us imagine a layer with two input neurons
and three output neurons. In such a layer, there would be six weights
(because each input neuron has three edges going out, or,
equivalently, each output neuron has two edges going in). These would
be arranged into a 2x3 matrix, like this:

[[1, 2, 3],

[4, 5, 6]]

Of course, in reality, these would all be long, possibly negative,
decimals. But for now, we will use simple numbers to illustrate the
concept. Next, we will run the input activations ``[1, 2]`` through
this layer. In order to do this, let's look at what those weights
represent. The 1 - 3 are outgoing from the first input neuron, the 4 -
6 from the second. On the other hand, the 1 and 4 are going into the
first output neuron, the 2 and 5 into the second, and the 3 and 6 into
the third. This means, to calculate the activation of the first output
neuron, we'd multiply the weight 1 by the activation 1, then add that
to the weight 4 multiplied by the activation 2. We get 9. You can do
the same for the other two outputs neurons, and you get 12 and 15,
respectively. So the output is ``[9, 12, 15]``. The nice thing is we
can obtain this same result with a little use of the numpy library, as
so:

.. code-block:: pycon

		>>> import numpy as np
		>>> W = np.array([[1, 2, 3], [4, 5, 6]])
		>>> a = np.array([1, 2])
		>>> np.dot(a, W)

Now, let us extend this example to make a complete net (minus
activation functions) and run through it:

.. code-block:: python

		import numpy as np
		net_shape = [2,3,2]
		inputs = [1, 2]
		weights = []
		for i in range(len(net_shape) - 1):
		    weights.append(np.random.randn(net_shape[i], net_shape[i + 1])
		    # np.random.randn is a function that will output an array with
		    # dimensions equal to the inputs populated with random values between -0.5 and 0.5

		def prop_forward(weights, inputs):
		    current_activations = inputs.copy()
		    for weight_matrix in weights:
		        current_activations = np.dot(current_activation, weight_matrix)
		    return current_activations

		print(prop_forward(weights, inputs))

Your output will be random, and different every time, but it should
have an array with 2 numbers in it. However, this is still only half
the puzzle. We need to add an activation function to avoid the output
being a linear combination of the input. If we use the same function
we've been using (hyperbolic tangent), all you need to do is add this
line after ``current_activations = ...``: ``current_activations =
np.tanh(current_activations)``. This reveals another incredible numpy
feature: you can perform algebraic operations on an array, and it will
apply that operation to each element in the array. This is a feature
that we will take full advantage of later on, and you should be aware
of it.

Of course, this simple model does not have any biases. We will show
later how this method of representation keeps track of biases, and why
it is simpler to do in this new fashion than with bias neurons.

Backpropagation with MNIST
==========================

Now that we understand forward propagation, let's get to back
propagation. It works by calculating some error (in this case just the
expected output minus the actual output) and then trying to minimize
it. It does this by propagating the error backward through the net,
calculating the precise amount each weight contributed to the final
error. It then systematically adjusts the weights to better fit the
data, and then repeats, usually hundreds or thousands of
times. Because of the complexity and calculus involved, this is a very
simplified model, and a great in-depth explanation can be found in the
3Blue1Brown video linked above.

The method we will be using to implement this may seem a little odd at
first, but the strengths will quickly become apparent. Each layer will
be its own class object, which will all be compiled into one neural
net class object. With this model, we can give the layer objects
methods to forward propagate and backward propagate, meaning, in the
future, you could add new types of layers without modifying the
existing code. Having said that, let us take a look at the final
program, as shown in :numref:`lis-MNIST-classes`. We will explore what
each part of this code does in greater detail next.

.. _lis-MNIST-classes:

.. literalinclude:: MNIST_classes.py
   :language: python
   :caption: MNIST_classes.py - A set of classes built to help fit the MNIST data set.

One difference with the previous models that may strike you is that we
are representing biases as a separate list, rather than nodes with an
activation that's constantly one. This is because it makes the matrix
dimension calculations easier, although it also means we have to
update them separately. To do the forward propagation with these, you
simply add them into the weighted sums, having the exact same effect
the bias neurons.  One more minor change is that the activation
function was changed to the sigmoid, which is just a function that
maps inputs to [0,1], rather than [-1,1] (as the hyperbolic tangent
did). the reason for this change is that because the outputs of the
net will be a sort of "probability", this is more convenient for our
net. A bigger change, though, is that the activation function isn't
part of the ``layer`` class. It's actually its own class, and treated
by the ``NN`` class as though it were a layer.  That can lead to some
funny-looking variables, such as the weights and biases of an
activation function. We set these to ``None`` to have to backprop code
skip over them, and use an entirely different method of forward
propagation specific to the type of activation function. For example,
in the sigmoid class, we apply the sigmoid function to each activation
individually. The formula is displayed below:

.. math::
   :nowrap:

   \begin{align}
   σ(x) & = \frac{1}{1 + e^{-x}}\\
   \end{align}

The biggest difference is that we use numpy's ability to run
calculations on arrays in parallel to do the entire batch in one
run. This means that every activation and activation gradient are
actually arrays of activations and activation gradients, one for each
input. There's a comment at the top explaining this as well, but it's
so important it should be repeated. The way the ``prop_backward``
methods work is beyond the scope of this chapter, but feel free to
look into it if you have the requisite mathematical background.

The core of this class is the ``update_params()`` method. It takes the
gradients, layer parameters, and velocities (which will be explained
in a second) and combines them, along with the learning rate, to
update all the weights and biases of all the layers. It does this by
taking the gradient (which is effectively the "error" of each weight
and bias) and changing the relevant parameter slightly in the correct
direction. The part that's probably still confusing right now is the
velocity. The best analogy for this is rolling a ball down a bumpy
hill; if the hill is very shallow or high-friction, the ball won't
pick up speed, and could end up getting stuck in a small rut in the
hill. However, if you do the same with a steep, low-friction slope,
the ball will just roll right through the same rut, and arrive at the
bottom of the hill. This is what velocity is doing to the neural net,
allowing it to roll through small "ruts" it may encounter on its way
to fitting the data. Over time, the velocity will depreciate, meaning
velocities from a long time ago will have less effect on the current
direction than more recent ones.

The ``train()`` method is basically a wrapper around
``update_params()``, cleaning it up with some nice printouts and
accuracy visualization. It's the method we actually call to train the
net, hence the name. It isn't super complicated once you understand
(more or less) what ``update_params`` is doing. Finally, we have the
``test()`` method which simply shows how the model does on data it's
never seen before, which helps catch overfitting errors but is also
really gratifying when it works properly.

We can now import both the neural net class and the data loader class
into our final function, which will extract data, train the model, and
test it. This can be implemented as shown in
:numref:`lis-MNIST-backprop`

.. _lis-MNIST-backprop:

.. literalinclude:: MNIST_backprop.py
   :language: python
   :caption: MNIST_backprop.py - A function that can fit a neural net to the MNIST data set.

As you can see, this program is very simple compared with the other
programs we have been looking at. It has the variables to control how
the net will fit the data (which are the top 6 variables) and the
filepaths as inputs, and after that, runs about ten lines of code. It
adds the layers to fit with the shape of the net, then trains and
tests the net with the methods we just defined. When run, it should
(after a couple minutes of waiting) produce an image similar to
:numref:`fig-accuracy-plot`

.. _fig-accuracy-plot:

.. figure:: accuracy_plot.*

	    A graph of the accuracy over time.

Your graph, if you've copied the function above, will only go up to
150 epochs (which is an equivalent term to iterations). This is to
prevent the training from taking prohibitively long (that graph took
around 5 minutes to generate), and gains in accuracy are modest
beyond that point. Feel free to increase the number of epochs or
change any other variables and see what happens, though do note that
some changes will cause the program to take a very long time to
run. Also, don't change the first or last number in shape, as those
are used by other parts of the program. You can, however, add more
hidden layers by adding more numbers to the middle of the array. One
particularly useful one is that if it's taking too long to run, you
can decrease the epochs further and increase the learning rate a
bit. Don't do it too much, as it can cause massive oscillation in the
weights, but increasing it by a little could help it run slightly
faster.

With the instructions for running the program out of the way, we will
look at what this accuracy graph can tell us. The first and most
obvious tend is that it goes up over time, and shoots up towards the
start and then rises only slowly. The reason for this is the fact that
we used a training model with velocity, which means that once it found
the correct "direction" to go in, it snowballed and reached high
accuracy very quickly. Another trend is that the accuracy can actually
go down from one epoch to the next. There are a large number of
reasons for this, the first being that backpropagation does not
actually optimize accuracy. It optimizes all ten of the outputs,
meaning that if the net got it right but was somewhat unsure, with all
the outputs being relatively high, the algorithm would give it a low
score. Conversely, if the net got it wrong, but was very confident
(meaning gave the wrong answer a high score and everything else a low
score), the algorithm would rate it more favorably. In short, the net
isn't just optimizing for accuracy, it's also optimizing for
confidence, both of which are required for the next step we are going
to take. The other reason for this trend is that the batches are
randomly selected; it is totally possible that the net gets a batch
that is just more difficult from one epoch to the next.

With the results analyzed, let us look at how we could input digits
into the net ourselves.

Hand-drawing digits
===================

The Tkinter library is made for GUI (graphical user interface)
applications. It's fairly straightforward to use once you understand
the basic commands. The ones we will be using are:

* ``tk.Tk()`` creates the object that holds all the information about
  the GUI. From now on, we will call it root (because that is what we
  call it in the program).
* ``root.title('title')`` names the window that Tkinter opens.
* ``root.minsize(x, y)`` makes the window at least x pixels by y
  pixels.
* ``tk.Canvas(root, width=width, height=height, bg=background_color)``
  creates a canvas object which can be interacted with by the user.
* ``tkinter_object.pack()`` adds the object to its respective ``Tk``
  object.
* ``tk.Canvas.bind(motion_type, function_call)`` makes a certain type
  of mouse motion over the canvas call a particular function.
* ``tk.Frame(root)`` adds a blank region to the root.
* ``tk.Button(Frame, text='text', font='name size bold?'
  command=function_call)`` creates a button with the specified text
  and font that calls a function when pressed by the user.

Note that for the canvas to call a function, the last function
parameter needs to be an object that holds the x and y coordinates of
the mouse. Those can be accessed with ``mouse.x`` and ``mouse.y``
respectively.

This is a very simple overview, and if there is anything that is still
unclear, the answer can probably be found in
:numref:`sec-graphical-user-interfaces` or
:numref:`sec-drawing-on-a-canvas`. However, those sections are not
required for this chapter, and it is unlikely you will need them.

Attempting to input to a net like this can be tricky, because all the
data was in a particular style, and ours might be in a slightly
different style. This means that the net is going to have a difficult
time classifying these other-style inputs, but it should still be
instructive to see when it gets the inputs right and when it does not.

With that said, let us look at the code in :numref:`lis-MNIST-user-input`:

.. _lis-MNIST-user-input:

.. literalinclude:: MNIST_backprop_user_input.py
   :language: python
   :caption: MNIST_backprop.py - A program that trains a net and get user input

You may notice a new import at the top from something called
``drawpad``. We will get to what this is, but first let us look at the
new functions. ``show_example()`` is a function that will display a
given training image, and is a good way to understand how Tkinter
works. Every time it is run, the function creates a new ``Tk`` object,
adds a 560x560 canvas to it (so we have a 28x28 grid of 20x20 pixels),
and then draws each value in the input as a rectangle on the
canvas. The ``rgb_hex()`` function takes an rgb color as input, and a
hex color as output. This is useful because tkinter takes hex, while
the pixel value is a grayscale value between 0 and 1. It's easy to
convert from grayscale to rgb, as all you have to do are multiply by
255 and repeat the result three times. However, it takes a bit more
work to convert from rgb to hex, which is why we need a separate
function. The format of the display images can be seen in
:numref:`fig-MNIST-image`:

.. _fig-MNIST-image:

.. figure:: MNIST_image.png

	    An example image from the MNIST data set.

We call these functions in the ``main()`` function to first draw 10
sample images, to give a sense of what the images should look like,
then gives you a new screen to draw the digits on. To understand how
to do that, we need to look at the drawpad function in
:numref:`lis-MNIST-drawpad`:

.. _lis-MNIST-drawpad:

.. literalinclude:: drawpad.py
   :language: python
   :caption: drawpad.py - A class that allows user input of digits

There's more going on here than in the ``show_example()`` function,
but the idea is the same. A 28x28 grid, with pixels being represented
with a number between 0 and 1. The difference is that now we can write
to these pixels rather than just reading them. To do this, the main
function we make use of is the ``self.draw_pixels()`` function, which
takes a mouse input as was described earlier. It selects a diamond
around the cursor (2 in every direction, and 1 in the diagonals) and
shades them according to the distance to the cursor. This is
cumulative, so leaving the cursor in one spot for longer makes the
pixels brighter. A sample image drawn on this can be seen in :numref:`fig-drawpad-digit`:

.. _fig-drawpad-digit:

.. figure:: drawpad_digit.png

	    An example digit on the drawing pad.

When the new `MNIST_backprop.py` is run, it should go through the same
steps of printing out the epochs, but then open an example image. When
you close this, the next one will open, and then the next. After
clicking through those, the drawing pad should open. Be careful not to
close this, as you will have to rerun the program to open it
again. The submit button will print out the net's prediction in the
console, and the clear button will clear the drawing pad. You can exit
the program by closing the drawing pad.

After entering a couple numbers, two things should become clear: the
number must be centered for the program to even have a shot at
identifying it correctly, and the accuracy is not as high as it was
even on the test data. The reason for the first is fairly obvious: all
the training and testing data was centered. The reason for the second
is a little more subtle: the MNIST data was created by scanning digits
written on paper into pixel values, while our program is entirely
digital. This means it's very hard for ours to capture different parts
of a stroke being heavier, or any other real-world phenomena that our
simple drawing pad can't take into account. Either way, the ways it
does mess up are fascinating, classifying 8's as 3's and vice versa,
or confusing 7's and 1's. No matter the accuracy, this is a great way
to get a glimpse of how the neural net works.

Conclusion
==========

In this mini-course, we continued to develop knowledge of neural
networks by using backpropagation. To start, we tried implementing it
on the polynomial data we generated last time, but that proved to be a
simple task prone to overfitting. We then tried a significantly more
complex example, namely the hand drawn digits in the MNIST data
set. This worked extremely well, with the net being able to achieve
around 85% accuracy.

After that, we implemented a way to input our own hand-drawn digits
into the net. This gave us a deeper insight into the way it works, and
scratched the surface of the incredibly versatile Tkinter library.

We did tread lightly around the math involved in backpropagation
because of how complex it is. But, as I have mentioned in various
places in the chapter, if you are interested in this subject, I would
highly recommend doing research into the gritty details of this
process. It may take a while to find a good source, but the
understanding is well worth it.
