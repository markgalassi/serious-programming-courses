from MNIST_classes import*
from data_reader import *
from drawpad import *

def main():
    shape = [784, 32, 10]
    activation_func = sigmoid
    learning_rate = 1
    velocity_depreciation = 0.9
    epochs = 1500
    batch_size = 250
    
    filepaths = ['train-images.idx3-ubyte','train-labels.idx1-ubyte','t10k-images.idx3-ubyte','t10k-labels.idx1-ubyte']
    
    inputs, expected_outputs, test_inputs, expected_test_outputs = MnistDataloader(filepaths[0], filepaths[1], filepaths[2], filepaths[3]).load_data()
    
    net = NN()
    for i in range(len(shape) - 1):
        net.add_layer(layer(shape[i], shape[i+1]))
        net.add_layer(activation_func())
        
    accuracies = net.train(epochs, inputs, expected_outputs, batch_size, learning_rate, velocity_depreciation, real_time=False)
    test_accuracy = net.test(test_inputs, expected_test_outputs)

    for _ in range(10):
        i = random.randint(0, len(inputs) - 1)
        image = inputs[i]
        label = expected_outputs[i]
        net.run_forward([image])
        output = net.outputs[0]
        show_example(image, list(output).index(np.max(output)), list(label).index(1))

    pad = DrawPad(net.run_forward) ## creates a drawing pad that will run the output through the trained net
    while pad.root.state() == 'normal': # checks if the window tkinter opened is still open 
        pad.get_input() ## automatically runs output through net
        print(f'Net output: {list(net.outputs[0]).index(np.max(net.outputs[0]))}')

def show_example(image, net_output, expected_output):
    """shows an image, the net output, and the image's label"""
    root = tk.Tk()
    root.title('Digit')
    canvas = tk.Canvas(root, width=560, height=560, bg='black')
    canvas.pack()
    image = image.reshape(28, 28).T
    for x in range(image.shape[0]):
        for y in range(image.shape[1]):
            pix_val = image[x][y] * 255
            fill = rgb_hex((pix_val, pix_val, pix_val))
            canvas.create_rectangle(x * 20 - 10, y * 20 - 10, x * 20 + 10, y * 20 + 10, fill=fill, width=0)
    print(f'Net output: {net_output}')
    print(f'Label: {expected_output}')
    root.mainloop()

def rgb_hex(rgb):
    """turns an rgb color into a hexcode color"""
    r, g, b = rgb
    return f'#{int(r):02x}{int(g):02x}{int(b):02x}'
    
main()
