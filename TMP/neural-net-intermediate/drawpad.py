import tkinter as tk
import numpy as np

class DrawPad():
    def __init__(self, output_command):
        """sets up various necessary variables"""
        self.output_command = output_command
        self.root = tk.Tk()
        self.pixels = np.zeros((28, 28))
        self.root.title('Input Digit:')
        self.root.minsize(560, 600)
        self.canvas = tk.Canvas(self.root, width=560, height=560, bg='black')
        self.canvas.pack()
        self.canvas.bind('<B1-Motion>', self.draw_pixels) # B1-Motion is when the left-click is held and dragged
        self.frame = tk.Frame(self.root)
        self.frame.pack(side=tk.BOTTOM)
        tk.Button(self.frame, text='Submit', font='comicsans 12 bold', command=self.output).pack(side=tk.BOTTOM, padx=6, pady=6)
        tk.Button(self.frame, text='Clear', font='comicsans 12 bold', command=self.clear).pack(side=tk.BOTTOM, padx=6)

    def draw_pixels(self, mouse):
        """draws a 3x3 diamond of pixels around the cursor"""
        # snaps xs and ys to a 28x28 grid
        x = np.floor(mouse.x / 20) * 20 + 10
        y = np.floor(mouse.y / 20) * 20 + 10
        for dx in range(-40, 60, 20):
            for dy in range(-40, 60, 20):
                if not ((abs(dx) == 40 and abs(dy) == 40) or (abs(dx) == 20 and abs(dy) == 40) or (abs(dx) == 40 and abs(dy) == 20)):
                    # selects a diamond-shaped region around the mouse
                    if abs(dx) == 20 or abs(dy) == 20:
                        # selects four pixels adjacent to center
                        dalpha = 10 / 255
                    elif abs(dx) == 40 or abs(dy) == 40 or (abs(dx) == 20 and abs(dy) == 20):
                        # selects 8 pixels not adjacent to center
                        dalpha = 3 / 255
                    else:
                        # selects center pixel
                        dalpha = 80 / 255
                    self.pixels[int((x + dx - 10) / 20)][int((y + dy - 10) / 20)] += dalpha # increases the value of a pixel
                    if self.pixels[int((x + dx - 10) / 20)][int((y + dy - 10) / 20)] > 1:
                        self.pixels[int((x + dx - 10) / 20)][int((y + dy - 10) / 20)] = 1
                    self.draw_rec((x - 10 + dx, y - 10 + dy), (x + 10 + dx, y + 10 + dy), alpha=self.pixels[int((x + dx - 10) / 20)][int((y + dy - 10) / 20)] * 255)

    def rgb_hex(self, rgb):
        """turns an rgb color into a hexcode color"""
        r, g, b = rgb
        return f'#{int(r):02x}{int(g):02x}{int(b):02x}'
                    
    def draw_rec(self, pt_1, pt_2, alpha=255):
        """draws a given pixel at a given alpha"""
        fill = self.rgb_hex((alpha, alpha, alpha))
        self.canvas.create_rectangle(pt_1[0], pt_1[1], pt_2[0], pt_2[1], fill=fill, width=0)

    def clear(self):
        """clears the canvas"""
        self.canvas.delete('all')
        self.pixels = np.zeros((28, 28))

    def output(self):
        """outputs the current state of the canvas"""
        self.output_command(self.pixels.T.reshape(1, 784))
        self.root.quit()
        
    def get_input(self):
        """displays the canvas and buttons"""
        self.root.mainloop()
