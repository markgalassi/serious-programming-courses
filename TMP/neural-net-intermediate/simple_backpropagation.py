from helper_functions import *

def main():
    poly_coeffs = [0.4, -1, 1, 1]
    domain = [-2, 1.5]
    num_points = 100
    noise = 0.75
    iterations = 1000
    learning_rate = 0.05

    
    nnet = generate_blank_net([1,5,5,5,1])
    inputs, expected_outputs = generate_polynomial_data(poly_coeffs, domain, num_points, noise)
    for iteration in range(iterations):
        outputs = []
        for i in range(len(inputs)):
            output = calc_all_layers(inputs[i], nnet, generate_layer_dict(nnet))
            outputs.append(output)
            backpropogate(nnet, inputs[i], output, expected_outputs[i])
            update_weights(nnet, learning_rate)
            
        if (iteration + 1) % 20 == 0:
            inputs_without_bias = []
            for input in inputs:
                inputs_without_bias.append(input[0])
            plot_current_fit(inputs_without_bias, outputs, expected_outputs)
        
def backpropogate(net, input, output, expected_output):
    """calculates errors for all neuron activations according to the error from a single input and output"""
    layer_dict = generate_layer_dict(net)
    for layer in reversed(sorted(layer_dict.keys())):
        if layer == len(layer_dict.keys()) - 1:
            for i in range(len(layer_dict[layer])):
                # tanh is the activation function here, and could theoretically be replaced by
                # any other non-linear function
                net.nodes[layer_dict[layer][i]]['error'] = (output[i] - expected_output[i]) * (1 - np.tanh(output[i]) ** 2)
        else:
             for current_neuron in layer_dict[layer]:
                 net.nodes[current_neuron]['error'] = 0
                 for previous_neuron in layer_dict[layer + 1]:
                     if previous_neuron[3] == 'b':
                         continue
                     # the formula for activation error is based on both the weights and the
                     # activations of the layer in front of it. This explains why we propagate
                     # backward through the net.
                     current_error = net.edges[(current_neuron, previous_neuron)]['weight'] * net.nodes[previous_neuron]['error'] * (1 - np.tanh(net.nodes[current_neuron]['activation']) ** 2)
                     net.nodes[current_neuron]['error'] += current_error

def update_weights(net, l_rate):
    """update weights based on error of concluding neuron, activation of beginning neuron, and the error in the ending neuron"""
    for edge in net.edges:
        net.edges[edge]['weight'] -= l_rate * net.nodes[edge[0]]['activation'] * net.nodes[edge[1]]['error'] 
                     
def generate_polynomial_data(coefficients, domain, num_points, noise=0):
    """returns two formatted lists: one for inputs, the other for expected outputs"""
    xs = np.linspace(domain[0], domain[1], num_points)
    ys = []
    xs_lists = [] # because of numpy syntax, we have to make two xs arrays
    for i in range(len(xs)):
        # add variation to the data to give fitting a thourough test
        xs[i] += 0.1
        xs_lists.append([xs[i]])
        # input_layers is a list of lists, so this formats the inputs properly
        y = 0
        for j in range(len(coefficients)):
            y += (coefficients[j] * (xs[i] ** j)) + (random.random() * noise) - (noise / 2)
        ys.append([y]) # same as above but for outputs
    return xs_lists, ys

def plot_current_fit(inputs, outputs, expected_outputs):
    """simple function to plot the net output vs expected output"""
    plt.clf()
    flat_outputs = sum(outputs, [])
    flat_expected_outputs = sum(expected_outputs, [])
    plt.plot(inputs, flat_expected_outputs)
    plt.plot(inputs, flat_outputs)
    plt.savefig('fitted-curve.png')
    print('Figure saved to fitted-curve.png')


main()
