Introducing serious programming -- some tic-tac-toe
===================================================

Motivation
----------

My goal is to go in to a 5th or 6th grade class as a guest with a 45
minute period, and in that time I want to motivate the students to
come take the "serious programming" course.

Physical setup
--------------

The classroom should have a projector.  Nowadays they all do.

I bring three laptops.  One is my teaching laptop, the other two are loaners.


What we do
----------

I will demonstrate, live with my laptop being projected:

* Simple python operations at the interpreter.

* Writing a simple program in the editor.

* Writing a program that displays a tic-tac-toe board.

* Demonstrating the fully written tic-tac-toe program.

* Installing an operating system. [pick this if time is going well]

* Demonstrating a couple of the scientific topics we cover. [pick this
  if time is going well]

* 
