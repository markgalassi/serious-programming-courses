====================================================================
 Teaser session in preparation for the "serious programming" course
====================================================================

Purpose
=======

I have seen the need for a shorter course to introduce students to the
ideas and feeling of what a serious programming course might look
like.

This teaser would invite students who cannot yet commit to 10 hours in
a weekend to come for a 1.5 hour course which would expose them to:

* The architectural picture from hardware to operating system to
  software.

* The GNU/Linux operating system.

* The command line.

* Simple few-line programs.

* Editing a file with an editor to write the program into that file.

* An example of a brief but interesting program.

Pre-introduction
================

Students can show up an hour early if they have an old laptop that is
not in use and want to install a GNU/Linux operating system to bring
it back to life.


Introduction
============

In the introduction I will guide the students through the dissecting
of a computer to show what the parts are, and then we will write a
diagram of how the hardware parts fit together.  This is a highly
interactive portion, where students are invited to use their
understanding to fill out many of the details, while I give it
structure fill in what's missing.

Then I introduce the software picture, with the kernel at the center,
then the layers of drivers and libraries and application programs.

The block diagrams here are the ones used in the full Serious
Programming course.

Duration estimate: 20 minutes (to be corrected after a couple of
courses)


Using the GNU/Linux operating system
====================================

In this section I demonstrate typical daily use of the operating
system.  The important part here is to show how it is
indistinguishable from other operating systems in the contemporary
user paradign, while at the same time showing that there is a parallel
world of using the command line.

I introduce files, directories, and a text editor.  First I
demonstrate a desktop-integrated WYSIWYG editor (such as gedit), and
then I demonstrate emacs.


Writing your first Python programs
==================================

Simple few-line programs.  Hello world and fahrenheit converasion, as
well as tables of number operations.


Editing a more complex program
==============================

First use of lists, programs with 10 or 15 lines of code.  Possibly
the Gauss formula for the sum of integers up to N.  Introduce the
mantra that the purpose of computers is to automate repetitive tasks.


A serious application
=====================

Interesting math application which needs a computer because of
repetitive tasks: the calculation of pi using the simulation of
throwing darts into a circle.
