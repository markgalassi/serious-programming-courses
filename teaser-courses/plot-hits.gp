## prepare output file with
## ./calculate-pi.py  > calculate-pi.out
set size square
plot [-1:1] 'calculate-pi.out' using 2:3 with points
replot [-1:1] sqrt(1 - x*x) lw 4
replot [-1:1] -sqrt(1 - x*x) lw 4
pause -1
