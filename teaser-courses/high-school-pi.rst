Introducing serious programming -- calculating pi
=================================================

Motivation
----------

The top motivation, as usual, is to show how a professional programmer
might write software.

For this age group I also show a tie-in between the math they are
about to study (:math:`pi` and the area of a circle) and writing a
program that calculates those quantities.

Finding the level of 7th graders
--------------------------------

The level of 7th graders can vary greatly in New Mexico, where 7th
grade is usually the first year of middle school.
