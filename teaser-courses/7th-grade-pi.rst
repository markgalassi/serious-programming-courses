Introducing serious programming -- calculating pi
=================================================

Motivation
----------

The top motivation, as usual, is to show how a professional programmer
might write software.

For this age group I also show a tie-in between the math they are
about to study (:math:`pi` and the area of a circle) and writing a
program that calculates those quantities.

Finding the level of 7th graders
--------------------------------

The level of 7th graders can vary greatly in New Mexico, where 7th
grade is usually the first year of middle school.

UNFINISHED

facts about pi:

* bible calculation with fountain

* archimedes with hexagons

* measure with two plates and a tape-measure

* 22/7

* 355/113 -- start with 113355, slash in the middle, then flip

* Nilakantha series

.. math::

   \pi = 3 + \frac{4}{2\times 3\times 4} - \frac{4}{4\times 5\times 6}
   + \frac{4}{6\times 7\times 8} - \frac{4}{7\times 8\times 9} + \dots


