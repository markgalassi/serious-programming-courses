.. teaser-courses documentation master file, created by
   sphinx-quickstart on Fri Aug 24 14:05:38 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Teaser courses for the programming workshop
===========================================

   :Date: |today|
   :Author: **Mark Galassi** <mark@galassi.org>


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro.rst
   5th-grade-tic-tac-toe.rst
   7th-grade-pi.rst
   high-school-pi.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
