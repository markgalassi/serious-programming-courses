#! /usr/bin/env python3

import math
import random

n_points = 10000
n_in_circle = 0
n_total = 0

print('## n x y n_total n_in_circle pi_estimate x y')


for i in range(n_points):
    x = random.random() * 2 - 1
    y = random.random() * 2 - 1
    distance = math.sqrt(x*x + y*y)
    n_total = n_total + 1
    if distance < 1:
        n_in_circle = n_in_circle + 1
    pi_estimate = (4.0 * n_in_circle) / n_total
    print('%d    %g    %g    %d    %d    %g' 
          % (i, x, y, n_total, n_in_circle, pi_estimate))
print('## pi: %g' % pi_estimate)
