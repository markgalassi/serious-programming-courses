## to sync from my own work account to my web page

## advice from
## http://stackoverflow.com/questions/10737166/chrome-not-rendering-svg-referenced-via-img-tag
rsync --delete -avz /home/markgalassi/repo/serious-programming-courses/small-courses/_build/html/ /home/markgalassi/web-galassi.org/mark/serious-programming/small-courses
rsync --progress --partial --delete -avz ~/web-galassi.org/mark/ web3.rdrop.com:web-galassi.org/mark
