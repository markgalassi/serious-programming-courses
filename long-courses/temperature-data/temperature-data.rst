Getting and plotting temperature data
=====================================

Using Python to retrieve data
-----------------------------

The most important thing we will do in this lesson is to *write a Python
program which retrieves data from the web*. This is very useful in at
least these two cases:

-  You want to access data that is regularly updated by some other
   institution. Examples of this include atmospheric, earthquake,
   astronomical and financial data.

-  You have your own measurement device which offers up its own data
   using a web server.

Python has excellent libraries for accessing and parsing web data.

The example I will give here is to access the NOAA (National Oceanic and
Atmospheric Administration) US Climate Research Network data.
Specifically: they have weather stations in many locations around the US
and they give straightforward online access to the data.

The first thing to do is find the URLs and understand how they are
structured, so that we can then write our program. The base URL actually
looks like this:
ftp://ftp.ncdc.noaa.gov/pub/data/uscrn/products/subhourly01/

There are three things to notice here:

a. the `README.txt`,

b. `HEADERS.txt`, and

c. all the subdirectories for each year of data.

We start by exploring: if you follow the year (let us say 2014) you find
a list of states and cities which have weather stations, and we will
pick Las Cruces, NM (New Mexico). The URL for Las Cruces, 2014, is:

ftp://ftp.ncdc.noaa.gov/pub/data/uscrn/products/subhourly01/2014/CRNS0101-05-2014-NM_Las_Cruces_20_N.txt

and in general the format is:

ftp://ftp.ncdc.noaa.gov/pub/data/uscrn/products/subhourly01/YYYY/CRNS0101-05-YYYY-SS_THE_CITY_20_N.txt

where YY is the year, SS is the date, and THE\_CITY is the city.

The problem is: we cannot just write out the URL – we have to give the
students a procedure for finding this data, so we start with a search
string:

``NOAA subhourly data``

and the first match we find will then give us a link for the subhourly
data sets.

Now that we have explored the layout of the data directories, we take a
look at the and files. These tell us that column 9 has the air
temperature, while the date and time (in UTC) are in columns 4 and 5.
(Remember that in Python, as in most computer languages, arrays start at
0, so these will be positions 8, 3, 4.)

We are now ready to write a python program which downloads the 2014 data
file for Las Cruces:

This program creates an empty list, then reads data from the URL, one
line at a time. Each line is split into words, and words 1 and 2
(remember: 2nd and 3rd columns) are used to get the date and time, while
word 8 (9th column) has the temperature in degrees Celsius.

This introduces three new Python tricks:

urllib
    Lets us open files on web (or ftp) servers as if they were local
    files. We can use the ``readlines()`` method as if it were a local
    file.

*string splitting*
    Allows us to break the line up into words.

``datetime``
    This library lets us turn the date and time strings into a numeric
    quantity (in our case minutes) which we can plot on the :math:`x`
    axis.

Once we have our time and temperature values we add them to the list
``time_temp_data``, and once we have filled that up we write it out to a
file.

Plotting the temperature data
-----------------------------

Now let us run the program, and then plot the resulting data file:

::

    $ ./gettemp.py
    ## retrieving from ftp://ftp.ncdc.noaa.gov/pub/data/uscrn [....]
    ## writing to file temperatures_Las_Cruces.dat
    $ gnuplot
    gnuplot> set grid
    gnuplot> set xlabel 'time (minutes since start of data)'
    gnuplot> set ylabel 'temperature (Celsius)'
    gnuplot> set title 'Las Cruces temperature in 2014'
    gnuplot> plot 'temperatures_Las_Cruces.dat' using 1:2 with lines
    gnuplot> set terminal pdf
    gnuplot> set output 'temperature-first.pdf'
    gnuplot> plot 'temperatures_Las_Cruces.dat' using 1:2 with lines
    gnuplot> unset output

This plot, shown in , tells a clear story: the temperature starts low in
January 2014, gets higher in the spring and summer, then goes back down
in the next fall and winter.

Within this larger 1-year cycle we also see many fluctuations that look
like spikes. If we zoom in by only plotting ten thousand and eight
minutes (one week) like this:

::

    gnuplot> set title 'Las Cruces temperature, first week of 2014'
    gnuplot> plot [0:10080] 'temperatures_Las_Cruces.dat' using 1:2 with lines
    gnuplot> set terminal pdf
    gnuplot> set output 'temperature-first-one-week.pdf'
    gnuplot> plot [0:10080] 'temperatures_Las_Cruces.dat' using 1:2 with lines
    gnuplot> unset output

we get the second plot shown in .

sampleplot-gp-gettemp-A-tex

sampleplot-gp-gettemp-B-tex

This zoomed-in plot allows us to say that the spikes are not “noise”,
but rather daily fluctuations. One week has 10080 minutes, and we see
seven cycles in that period of time. Within that you do see some noise,
but it’s clear that the main features in these plots are the yearly and
daily temperature fluctuations.

Retrieving more than one year
-----------------------------

The program in only fetches one year of temperature, but it is
instructive to gather a few years, since it shows us that along with the
daily period (), we also have an annual period ().

shows a few years of plots. We will examine this superposition of two
different periodic signals in greater detail in , using the powerful
technique of fourier analaysis.

sampleplot-gp-gettemp-multi-tex
