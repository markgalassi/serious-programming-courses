.. Serious Programming - long projects documentation master file, created by
   sphinx-quickstart on Sat Apr 29 12:54:15 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Serious Programming - long projects
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro.rst
   geospatial/geospatial.rst
   temperature-data/temperature-data.rst
   exploring-statistics/exploring-statistics.rst
   fourier-analysis/fourier-analysis.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. bibliography:: ../hackingcamp.bib
   :all:
