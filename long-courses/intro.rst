The long courses
================

Please see the introductory section in the `"Small Courses" book
<../small-courses/index.html>` for an overall description on how I
recommend teaching individual scientific topics.

This book is for longer courses than those in the "Small Courses"
book.  They are recommended for older students and for students who
can commit to two or three 1.5 hour sessions.  In these longer courses
I will sometimes enter into more advanced mathematical territory.
