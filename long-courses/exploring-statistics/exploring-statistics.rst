Exploring statistics
====================

Sometimes labeled as boring, the study of statistics is quite
fascinating: an area in which our intuition is often incorrect, but at
the same time a discipline which is crucial for our understanding of
many real-world problems.

We will use random number generators to simulate some situations in
which it is very easy to produce and plot data which gives surprising
but solid insights into some aspects of nature.

Poisson statistics in a time series
-----------------------------------

You have a process which generates events intermittently in time. There
are at least two types of situations:

-  Each event time *is related to the value of the previous event time*.
   Example: your next meal – you get hungry after a certain amount of
   time since your last meal, which affects when you next choose to eat.
   (Note that in a very regimented family these times might not be
   random at all.)

-  Each event time *is **independent** of the previous event time*.
   Example: radioactive decay, where each particle decay time has
   nothing to do with the previous one.

The second type of process (event time is independent of previous event
time) is called a *Poisson process*.

It is interesting to understand when a series of events comes from a
poisson process and when it does not. We will now use the random number
generator to simulate a poisson process, but first let us give ourselves
a physical example of what might be happening.

Imagine the following. You have a house in a very unfortunate place:
every day there is a 30% chance of a lightning striking that
house. And every day’s 30% *does not change* based on what happened
yesterday or at any time in the past. To use Steven Pinker’s
expression :cite:`pinker2011better`, every day Zeus throws a 10-sided die,
and if the number is between 1 and 3 the house will be hit.

Let us study a bit about what happens to this house. We will study it by
simulating each day whether it gets hit or not, and we will collect
statistics on whether it was hit.

One might wonder why lightning strikes are interesting. At this stage I
point out that these sequences of random events are called a *Poisson
series:* the chance of each event is unrelated to when or how the
previous event happened. Other examples might include radioactive decay,
earthquakes, outbreak of war, some measures in financial markes, and
many others.

Let us start by reminding ourselves of how python allows us to produce
random numbers. In our hacking camp
:cite:`galassi2015hackingcampteacher` we saw this example of a few
functions:

::

    >>> import random
    >>> random.random()
    >>> random.random()
    >>> random.random()
    >>> random.randint(-3, 10)
    >>> random.randint(-3, 10)
    >>> random.randint(-3, 10)
    >>> random.randint(0, 2)
    >>> random.randint(0, 2)
    >>> random.randint(0, 2)
    >>> random.randint(0, 2)
    >>> random.randint(0, 2)

To generate an event which looks like the result of tossing dice with a
30% outcome, and to see if the probability was right, we can use this
little program:

When you run this program you should get an average lighting strikes per
day that is close to 0.3:

.. code:: bash

    $ ./lightning-first-stab.py
    average daily hits: 0.285 (1000 days)

every time the run gives a different result, but it’s always close to
0.3.

It’s good to get some agility with these simple programs. Let us start
by seeing how the average behaves when we simulate more or fewer days.
We start by writing a function which does the simple calculation of
average lightning strikes/day:

The output of looks like this:

.. code:: bash

    $ ./lightning-vary-n-days.py 
    average daily hits: 0.04 (100 days), off by 0.01
    average daily hits: 0.05 (100 days), off by 0.02
    average daily hits: 0.05 (100 days), off by 0.02
    average daily hits: 0.01 (100 days), off by 0.02

    average daily hits: 0.03 (1000 days), off by 0
    average daily hits: 0.026 (1000 days), off by 0.004
    average daily hits: 0.032 (1000 days), off by 0.002
    average daily hits: 0.024 (1000 days), off by 0.006

    average daily hits: 0.0303 (10000 days), off by 0.0003
    average daily hits: 0.033 (10000 days), off by 0.003
    average daily hits: 0.0288 (10000 days), off by 0.0012
    average daily hits: 0.0281 (10000 days), off by 0.0019

    average daily hits: 0.0299 (100000 days), off by 0.0001
    average daily hits: 0.03062 (100000 days), off by 0.00062
    average daily hits: 0.02974 (100000 days), off by 0.00026
    average daily hits: 0.03062 (100000 days), off by 0.00062

    average daily hits: 0.030283 (1000000 days), off by 0.000283
    average daily hits: 0.030008 (1000000 days), off by 8e-06
    average daily hits: 0.030252 (1000000 days), off by 0.000252
    average daily hits: 0.030035 (1000000 days), off by 3.5e-05

The interesting thing about this output is that it shows how more runs
give you an average number of lightning strikes/day that gets closer and
closer to the 0.3 number.

This confirms that the the snippet of Python code which counts hits and
misses:

::

            # ...
            r = random.random()
            if r <= 0.3:             ## 30% chance
                n_hits += 1
            else:
                n_misses += 1
            # ...
        hit_fraction = n_hits / n_days

is a valid way of simulating a random occurrence which is *uniformly
distributed*. It also reminds us again that if you want good statistics
you need many events: the runs with 100000 events gave an average much
closer to 0.3 than the runs with 10 or 100 runs…

Let us now collect some other properties than the average number of
lightning strikes. One question we might ask about these events is:

Q: what is the typical time that elapses between successive strikes?

| To collect this information we modify the program and call it

This program outputs a list of time intervals, :math:`\Delta t`. Let us
get an idea of what these look like by making a scatter plot of the
:math:`\Delta t` values:

::

    $ gnuplot
    gnuplot> plot 'time_diffs.dat' using 1 with points pt 4 ps 0.3

This does not look like much: just some 30 points on the screen,
somewhat randomly laid out. There are not enough points yet to notice a
clear pattern, so change the number ``n_days`` to be a very large
number, like 10000, and see what you get. We can re-run the program:

::

    $ ./lighting-time-distribution.py 10000
    $ gnuplot
    gnuplot> plot 'time_diffs.dat' using 1 with points pt 4 ps 0.3 title 'time between lightning strikes'

and see the result in . sampleplot-gp-lightning-scatter-tex

Here is how I would read the plot to a class of students:

I would then jump up and down, exclaiming “you see how a single plot
command can give you so much insight?”

To get even more insight let us show what a random plot would have
looked like. The program ``random-uniform.py`` puts out a list of
uniform random numbers:

The purely random numbers can be seen in , and now we can really jump up
and down yelling about insight: the scatter plot of random numbers had
no structure, whereas the scatter plot of time between lightning strikes
had a clear structure.

sampleplot-gp-random-scatter-tex

Then a sobering note: this “scatter plot” was very good for probing the
data for a quick bit of insight, but it does not tell us anything
quantitative about the time between strikes. To do this we need to plot
*histograms* of the data.

Histograms of quantities
------------------------

A lot of plots you are used to seeing in popular media are *histogram*
plots. These plots don’t show the measured quantities directly: they
show how many times certain values come up.

For example, when you look at a plot of the weight of a group of people
you see the typical bell curve, and on the x axis you have *weight
ranges*, while on the y axis you have *how many people are in that
weight range*.

The data is not naturally measured in this way, so we write a bit of
code to change it to that format.

Let us do this with the file which was written out by our . It contains
a single column of :math:`\Delta t` values (measured in days) that look
like this:

::

    1
    4
    12
    3
    2
    2
    5
    6
    1
    1
    8
    2
    ## ... many more lines ...

What we want to do is count how many times each duration appears in the
file, this will be the histogram. A python program to do so might be:

You can run it with:

.. code:: bash

    $ ./int-histogram-maker.py time_diffs.dat
    $ gnuplot
    gnuplot> set grid
    gnuplot> set xlabel '{/Symbol D} t (days)'
    gnuplot> set ylabel 'frequency of that interval'
    gnuplot> plot 'time_diffs.dat.hist' using 1:2 with linespoints

We can do better: plotting programs have special ways of plotting
histograms – try this one:

.. code:: bash

    gnuplot> plot 'time_diffs.dat.hist' using 1:2 with boxes

The result is shown in

sampleplot-gp-lightning-hist-tex

The story in this plot () is easy to tell: there are many more lightning
strikes that are closely spaced than that are far apart.

For completeness let us look at what happens when we take the random
values shown in the scatter plot and look at this histogram of those?
The result (a flat histogram) is shown in Figure[fig:random-hist].

sampleplot-gp-random-hist-tex

Random spatial distribution
---------------------------

We have talked about processes which give events distributed *randomly
in time:* events happen at random times. Let us now look at processes
that generate points distributed *randomly in space:* :math:`(x, y)`
coordinates are spewed out by our process. An example might be where the
grains of sand land when you drop a handful onto the ground.

We can write a program to generate random :math:`(x, y)` points between
0 and 100. The program ``random-spatial.py`` generates a series of such
points, each completely independent of the previous one.

The results of running this program are shown in [fig:random-spatial].
You can see *features* in the data, even though it was randomly
generated: filaments, clustering, voids… [6]_

.. [6]
   Note that the clustering is an artifact of the random generation of
   points; it is not due to a physical effect that clusters the points
   together.

sampleplot-gp-random-spatial-tex

A possible comment: people who spend a lot of time looking at randomly
generated data probably don’t easily believe in conspiracy theories.

We can then do something analogous to what we did for random events in
time: plot the distribution of distances between :math:`(x, y)` points.
The programs ``xy-to-distances.py`` and ``int-histogram-maker`` allow us
to do so, and the results are in . Note that you will not get as much
insight out of these spatial histograms as you did in , since a big
factor in the distribution of spacial distances is the small size of the
*x-y* plane we used.

sampleplot-gp-spatial-hist-tex

What have we learned
--------------------

In this section we have learned that:

-  A histogram shows you *how often* a value comes up (the frequency of
   certain values).

-  We can write simple Python programs which take data and make
   histograms of the frequency of those values.

-  Plotting programs can be used to see the histograms.
