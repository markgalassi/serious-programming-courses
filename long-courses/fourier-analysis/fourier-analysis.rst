
Looking deeply at a curve: the Fourier Transform
================================================

There is often more to a plot than immediately meets the eye, and in the
life of a scientist one of the great joys is to glean more information
than what is on the surface.

Here we will show one of my favorite tools for extracting further
information from data: the *Fourier transform*, also referred to as the
*Fourier spectrum*.

It is unlikely that you will be able to fully explain Fourier Transforms
to kids, but here is an approach:

Fourier analysis: the square wave
---------------------------------

Let us start gnuplot and plot a :math:`\sin` wave, then look at some
:math:`\sin` waves with higher frequencies. Then add them together and
see what you get:

::

    $ gnuplot
    gnuplot> set samples 1000
    gnuplot> plot [] [-1.2:1.2] sgn(sin(x))
    gnuplot> replot sin(x)
    gnuplot> replot (1.0/3)*sin(3*x)
    gnuplot> replot (1.0/5)*sin(5*x)
    gnuplot> replot sin(x) + (1.0/3)*sin(3*x) + (1.0/5)*sin(5*x)
    ## now look at the summed-up plot by itself:
    gnuplot> plot sin(x) + (1.0/3)*sin(3*x) + (1.0/5)*sin(5*x) t '5x'

You should start seeing that you go from a :math:`\sin` wave to
something that looks a bit more like a square wave. shows what the
individual :math:`\sin` waves look like and shows how you can add up to
13 of them and get something that starts to look quite square instead of
wavy.

sampleplot-gp-square-wave-tex

The mathematics behind the Fourier Transform are beautiful but more
advanced than this course, so we will just stick with having seen the
main idea:

The square wave is not particularly realistic, so let us look at some
real signals. We will start by looking at white noise, then a tuning
fork, then single notes on musical instruments, then at a more complex
music clip.

Fourier analysis: sound and music
---------------------------------

When we mentioned waves with different frequencies you might have
thought of sound, and you would have been right. Fourier analysis is a
good tool for understanding what makes up sound waves.

Let us take a tour through a series of signals, and we will look at
their fourier transforms.

Tuning fork
~~~~~~~~~~~

A tuning fork puts out a very pure single :math:`\sin` wave at a “middle
A” note, also known as :math:`A_4` or concert pitch. :math:`A_4` has a
frequency of 440 Hz.

We can download a stock mp3 file of the sound of a tuning fork, then we
can use standard command line utilities to convert it to a text file.
For example:

::

   wget https://freesound.org/data/previews/126/126352_2219070-lq.mp3 -O tuning-fork.mp3
   # play it to see that it's good
   vlc tuning-fork.mp3
   # convert through various steps to .aif and then to .dat
   ffmpeg -i tuning-fork.mp3 tuning-fork.aif
   sox tuning-fork.aif tuning-fork.dat
   ls -lsh tuning-fork.*

You can plot it with:

::

   gnuplot
   # then at the gnuplot> prompt:
   plot 'tuning-fork.dat' using 1:2 with lines

This shows you the plot of amplitude versus time, and you should
notice some correspondence between it and the sound you heard - the
person who recorded this file kept modulating how loud it was.

But that does not tell us what frequencies are in it!  Let us adapt
the ``filter_random.py`` program to work with this data file.

You can download 

Once we have it as a text file we can do the following:

-  Use our standard plotting techniques to see that it looks like a very
   clean :math:`\sin` wave (top plot in ).

-  Write a program which uses the powerful Python scientific libraries
   to calculate the Fourier transform of the tuning fork signal.

-  Look at the Fourier transform, hoping to see that a clean
   :math:`\sin` wave will appear as a single spike, indicating that
   there is only one :math:`\sin` wave in the signal.

sampleplot-gp-tuningfork-tex

Having looked at the plots in let us write the python program
``simple-fft.py`` which reads in the signal and writes out the Fourier
transform:

Having written this program we can then follow the steps in to generate
those plots ourselves. Note that the steps include a simple scripting
trick to pick out just a small part of the tuning fork signal. That’s
because

[*(a)]*

these signals have more than 40000 samples/second which is big and
unnecessary, and

data at the very start of the file can have artifacts.

What we need to do is skip a lot of lines at the start, and then pick
out about 1000 lines of data. A simple use of UNIX shell pipelines can
do this:

.. code:: bash

    $ head -50000 tuningfork440.dat | tail -1000 > tuningfork-small-sample.dat

We will use ``simple-fft.py`` in the next few examples as well, getting
a surprise amount of mileage from one program.

White noise
~~~~~~~~~~~

Now let us download an example of *white noise*. This is a random wave
with no discernible pattern.

sampleplot-gp-white-noise-tex

Go ahead and follow the instructions in . To the ear the signal sounds
like a hissing sound, as you can see if you run

.. code:: bash

    vlc white-noise.mp3

*Remember this simple way of playing audio clips.*

You could say that the Fourier spectrum for white noise is the opposite
of that for the pure tuning fork signal: instead of a single spike you
have random-looking spikes all over the spectrum.

Violin playing single “A” note
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Now let us look at some musical notes from real instruments. Each note
corresponds to a certain frequency
:cite:`NoteFrequencies1977`. shows a much more complex
signal than the tuning fork.

sampleplot-gp-violin-A-440-tex

The signal in has some structure to it. The Fourier transform has
several peaks: the strongest peak (for a :math:`A_4` note) will be at
the *fundamental frequency* of 440 Hz, but there are many other peaks.
The pattern of the peaks characterizes the sound of that specific violin
(or guitar or other instrument).

Violin playing single “F” note
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is a repeat of but with a different note on the violin: F instead
of A. is hard to distinguish from since the character of the instrument
is the same. At this point we have not written code to plot the actual
frequencies on the :math:`x` axis, so we cannot spot that the
:math:`A_4` note has its highest peak at 440Hz, while the :math:`F_5` is
at 698.45Hz :cite:`NoteFrequencies1977`.

sampleplot-gp-violin-F-tex

 the web site with these violin clips has something weird where it
always downloads the same file unless you sign in, so at this time is
incorrect and probably the same as . Plan: record guitar clips for the
same notes and save them somewhere online.

A more complex music clip
~~~~~~~~~~~~~~~~~~~~~~~~~

The Pachelbel Canon is a well-known piece of baroque classical music
which starts with a single note that is held for a while. shows the
signal and its Fourier transform, where you can identify a dominant
peak.

sampleplot-gp-canon-tex

But what happens if we have music that is not a single note? In we will
look at a clip of a choir singing *Gloria in Excelsis Deo*. The clip
(which you can play with ``vlc gloria.ogg`` after downloading it) starts
in a place where many voices are singing in harmony, so there is no
single note to be picked out.

We expect to see several different peaks in the Fourier spectrum, and
that is what you see in the second plot of .

sampleplot-gp-gloria-excelsis-deo-tex

Create your own audio clip and analyze it
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Record some sound and analyze it.

The program ``sox``, which we used to do audio format conversion, comes
with two other programs ``rec`` (to record from your computer’s
microphone) and ``play`` to play those files back.

To try it out run the following:

.. code:: bash

    $ rec myvoice.dat
    ## speak for a second or two into the microphone, then hit control-C
    $ play myvoice.dat
    $ ./simple-fft.py myvoice.dat 1 myvoice-fft.dat
    $ gnuplot
    gnuplot> set multi layout 2,1
    gnuplot> plot 'myvoice.dat' using 1:2 with lines
    gnuplot> plot 'myvoice-fft.dat' using 1:3 with boxes

Now try doing this again for different things you can record with your
microphone. If you have a tuning fork, tap it and then rest it on a
guitar’s soundboard and record that, see if you get something similar to
what we saw in . If you have a musical instrument, try recording an A
note or an F note and compare them to what we saw in .

Picking out frequencies from a toy signal
-----------------------------------------

In we generated a simple :math:`\sin` wave. The fourier analysis of that
should offer a clear single peak, as we can see with

.. code:: bash

    $ ./simplewave.py > simplewave.dat
    $ ./simple-fft.py simplewave.dat 1 simplewave-fft.dat
    $ gnuplot
    gnuplot> set multi layout 2,1
    gnuplot> plot 'simplewave.dat' using 1:2 with lines
    gnuplot> plot 'simplewave-fft.dat' using 1:3 with boxes

Now let us generate a slightly more complex signal, one that will add
two :math:`\sin` waves together. We will do it to mimic the temperature
over a couple of years, like what we retrieved in . Start by making a
copy of the program to , and then we will try adding two different
things to it:

[*(a)]*

a higher-frequency :math:`\sin` wave, analogous to the daily
fluctuations in temperature on top of the yearly fluctuations, and

a certain amount of white noise.

The Fourier transform of these two different additions should be
instructive. Both will *look the same:* a lot of spikes on top of the
annual period. They will look different when you zoom in (as in ), but
the Fourier transform will also allow us to pick out a strong
difference.

Modify to look like this:

The results are shown in : we see two peaks, one for the “annual” cycle,
the other for the higher frequency “daily” cycle. There are many points,
so the peaks are not that thick, but they are tall and well-defined.

sampleplot-gp-two-sin-fft-tex

Then we do the same thing with a *noisy* :math:`\sin` wave. The program
looks like this:

The noisy :math:`\sin` wave and its Fourier transform are shown in , and
you can see that although the signal is hard to tell apart from the
double sin wave, the spectrum is clear: there is only one significant
peak (from the “yearly” :math:`\sin` wave), and then there is a
scattering of noisy bits in the spectrum, which we expect when the
signal is noisy instead of being a pure :math:`\sin` wave.

sampleplot-gp-noisy-sin-fft-tex

EXTRA: Fourier analysis of the temperature data
-----------------------------------------------

Our final step is to look at the temperature data we downloaded in .
Unlike , this example will use *real* data from the weather station near
Las Cruces.

sampleplot-gp-temperature-fft-tex

The Fourier transform of the temperature is shown in . We see something
analogous to what we saw with the toy model which simulated temperature
by adding two :math:`\sin` waves.
